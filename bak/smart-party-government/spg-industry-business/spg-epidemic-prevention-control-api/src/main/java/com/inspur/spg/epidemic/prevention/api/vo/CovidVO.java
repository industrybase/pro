package com.inspur.spg.epidemic.prevention.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "疫情实体VO")
public class CovidVO {

    //现有确诊
    @ApiModelProperty(value = "现有确诊")
    private Integer nowConfirm;
    //累积确诊
    @ApiModelProperty(value = "累积确诊")
    private Integer confirm;
    //较昨日现有确诊
    @ApiModelProperty(value = "较昨日现有确诊")
    private Integer nowConfirmBefore;
    //新增确诊
    @ApiModelProperty(value = "新增确诊")
    private Integer confirmAdd;
    //较昨日新增确诊
    @ApiModelProperty(value = "较昨日新增确诊")
    private Integer confirmAddBefore;
    //累计死亡
    @ApiModelProperty(value = "累计死亡")
    private Integer dead;
    //新增死亡数
    @ApiModelProperty(value = "新增死亡数")
    private Integer deadAdd;
    //较昨日新增死亡数
    @ApiModelProperty(value = "较昨日新增死亡数")
    private Integer deadAddBefore;
    //较昨日新增死亡数
    @ApiModelProperty(value = "疑似病例")
    private Integer suspect;
    //累积治愈数
    @ApiModelProperty(value = "累积治愈数")
    private Integer heal;
    //新增治愈数
    @ApiModelProperty(value = "新增治愈数")
    private Integer healAdd;
    //较昨日新增治愈数
    @ApiModelProperty(value = "较昨日新增治愈数")
    private Integer healAddBefore;
    //无症状
    @ApiModelProperty(value = "无症状")
    private Integer asymptomatic;
    //新增无症状
    @ApiModelProperty(value = "新增无症状")
    private Integer asymptomaticAdd;
    //较昨日新增无症状
    @ApiModelProperty(value = "较昨日新增无症状")
    private Integer asymptomaticAddBefore;

}
