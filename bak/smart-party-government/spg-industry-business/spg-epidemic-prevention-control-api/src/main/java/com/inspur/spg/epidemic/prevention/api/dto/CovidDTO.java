package com.inspur.spg.epidemic.prevention.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Date;

/**
 * 疫情城市dto
 */
@Data
@ApiModel(description = "疫情城市dto")
public class CovidDTO implements Serializable {

    private static final long serialVersionUID = 1029692422931571945L;
    /**
     * 省份
     */
    @ApiModelProperty(value = "省份",required = true)
    @NotEmpty(message = "省份不能为空")
    private String provinceName;

    /**
     * 城市
     */
    @ApiModelProperty(value = "城市")
    private String cityName;

    /**
     * 统计开始日期
     */
    @ApiModelProperty(value = "查询开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 统计结束日期
     */
    @ApiModelProperty(value = "查询结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 查询日期
     */
    @ApiModelProperty(value = "查询日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date statDate;

    public String getProvinceName() {
        if(StringUtils.isNotBlank(this.provinceName)){
            if(this.provinceName.contains("省")){
                this.provinceName = this.provinceName.replace("省","");
            }else if(this.provinceName.contains("市")){
                this.provinceName = this.provinceName.replace("市","");
            }
        }
        return provinceName;
    }
}
