package com.inspur.spg.epidemic.prevention.api.feign.factory;

import com.inspur.spg.epidemic.prevention.api.feign.RemoteCovidService;
import com.inspur.spg.epidemic.prevention.api.feign.fallback.RemoteCovidFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/4 14:57
 **/
@Component
public class RemoteCovidFallbackFactory implements FallbackFactory<RemoteCovidService> {
    @Override
    public RemoteCovidService create(Throwable cause) {
        RemoteCovidFallbackImpl fallback = new RemoteCovidFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }
}
