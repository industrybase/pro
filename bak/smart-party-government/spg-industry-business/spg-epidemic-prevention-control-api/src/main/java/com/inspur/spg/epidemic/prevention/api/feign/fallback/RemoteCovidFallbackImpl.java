package com.inspur.spg.epidemic.prevention.api.feign.fallback;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.epidemic.prevention.api.feign.RemoteCovidService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/4 14:58
 **/
@Component
@Slf4j
public class RemoteCovidFallbackImpl implements RemoteCovidService {

    @Setter
    private Throwable cause;


    @Override
    public R saveBatch(Map<String,Object> dataMap, String from) {
        log.error("feign 疫情接口 saveBatch 保存失败", cause);
        return R.failed("feign 疫情接口 saveBatch 保存失败，" + cause.getMessage());
    }
}
