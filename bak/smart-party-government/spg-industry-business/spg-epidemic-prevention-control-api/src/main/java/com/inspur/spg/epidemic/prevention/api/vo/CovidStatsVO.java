package com.inspur.spg.epidemic.prevention.api.vo;

import lombok.Data;

import java.util.Date;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/15 15:40
 **/
@Data
public class CovidStatsVO {

    //统计日期
    private Date statDate;
    //累积新增
    private Integer totalConfirmAdd;
    //本土新增
    private Integer localConfirmAdd;
    //境外新增
    private Integer foreignConfirmAdd;

}
