package com.inspur.spg.epidemic.prevention.api.feign;


import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.constant.ServiceNameConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.epidemic.prevention.api.feign.factory.RemoteCovidFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;

@FeignClient(contextId = "remoteCovidService", value = ServiceNameConstants.EPIDEMIC_PREVENTION_CONTROL_SERVICE, fallbackFactory = RemoteCovidFallbackFactory.class)
public interface RemoteCovidService {

    /**
     * @Author CJ
     * @Description 批量保存
     * @Date 2022/1/4 15:01
     * @param map
     * @return com.inspur.spg.common.core.util.R
     **/
    @PostMapping("/covid/saveBatch")
    R saveBatch(@RequestBody Map<String,Object> dataMap, @RequestHeader(SecurityConstants.FROM) String from);

}
