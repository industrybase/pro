package com.inspur.spg.epidemic.prevention.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;


/**
 * 省份疫情信息(CovidProvince)表实体类
 *
 * @author CJ
 * @since 2022-01-03 14:12:36
 */
@Data
@TableName("covid_province")
@EqualsAndHashCode(callSuper = true)
public class CovidProvince extends Model<CovidProvince>{

    private Long id;
    //省份名称
    private String provinceName;
    //省份编码
    private String provinceCode;
    //现有确诊
    private Integer nowConfirm;
    //较昨日现有确诊
    private Integer nowConfirmBefore;
    //累积确诊
    private Integer confirm;
    //新增确诊
    private Integer confirmAdd;
    //较昨日新增确诊
    private Integer confirmAddBefore;
    //疑似病例
    private Integer suspect;
    //累计死亡
    private Integer dead;
    //新增死亡数
    private Integer deadAdd;
    //较昨日新增死亡数
    private Integer deadAddBefore;
    //死亡率
    private BigDecimal deadRate;
    //累积治愈
    private Integer heal;
    //新增治愈
    private Integer healAdd;
    //较昨日新增治愈
    private Integer healAddBefore;
    //治愈率
    private BigDecimal healRate;
    //无症状
    private Integer asymptomatic;
    //新增无症状
    private Integer asymptomaticAdd;
    //较昨日新增无症状
    private Integer asymptomaticAddBefore;
    //创建时间
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //最后更新时间
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastUpdateTime;
    //统计日期
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date statisticalDate;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}