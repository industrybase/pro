package com.inspur.spg.epidemic.prevention.api.dto;

import com.inspur.spg.epidemic.prevention.api.entity.CovidCity;
import com.inspur.spg.epidemic.prevention.api.entity.CovidProvince;
import lombok.Data;

import java.util.List;

@Data
public class CovidProvinceDTO extends CovidProvince {

    List<CovidCity> covidCityList;

}
