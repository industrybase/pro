package com.inspur.spg.epidemic.prevention.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.epidemic.prevention.api.dto.CovidDTO;
import com.inspur.spg.epidemic.prevention.api.dto.CovidProvinceDTO;
import com.inspur.spg.epidemic.prevention.api.entity.CovidCity;
import com.inspur.spg.epidemic.prevention.api.entity.CovidProvince;
import com.inspur.spg.epidemic.prevention.api.vo.CovidVO;
import com.inspur.spg.epidemic.prevention.mapper.CovidProvinceMapper;
import com.inspur.spg.epidemic.prevention.service.CovidCityService;
import com.inspur.spg.epidemic.prevention.service.CovidProvinceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 省份疫情信息(CovidProvince)表服务实现类
 *
 * @author CJ
 * @since 2022-01-03 14:12:36
 */
@Service
public class CovidProvinceServiceImpl extends ServiceImpl<CovidProvinceMapper, CovidProvince> implements CovidProvinceService {

    @Resource
    private CovidCityService covidCityService;

    @Override
    @Transactional
    public void saveCovidProvinceDTO(CovidProvinceDTO covidProvinceDTO) {
        baseMapper.insert(covidProvinceDTO);
        covidCityService.saveBatch(covidProvinceDTO.getCovidCityList());
    }

    @Override
    public void saveCovid(Map<String,Object> dataMap) {
        List<Map<String,Object>> areaTree = (List<Map<String, Object>>) dataMap.get("areaTree");
        Map<String,Object> chinaMap = areaTree.get(0);
        List<Map<String,Object>> childrenList = (List<Map<String, Object>>) chinaMap.get("children");
        for(Map<String,Object> map : childrenList){
            try{
                map.put("lastUpdateTime",dataMap.get("lastUpdateTime"));
                CovidProvinceDTO covidProvinceDTO = getCovidProvinceDTOByMap(map);
                saveCovidProvinceDTO(covidProvinceDTO);
            }catch (Exception e){

            }
        }
    }

    @Override
    public CovidProvince getByStatisticalDateAndProvinceName(Date statDate, String provinceName) {
        return baseMapper.selectOne(
                Wrappers.<CovidProvince>lambdaQuery().eq(CovidProvince::getStatisticalDate, statDate).eq(CovidProvince::getProvinceName,provinceName));
    }

    @Override
    public CovidVO getCovidVo(CovidDTO covidDto) {
        return baseMapper.getCovidVo(covidDto);
    }

    private CovidProvinceDTO getCovidProvinceDTOByMap(Map<String, Object> map) {
        String provinceName = (String) map.get("name");
        Date yesterday = DateUtil.formatDateToDate(DateUtil.addDays(new Date(),-2),"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd");
        Map<String,Object> todayMap = (Map<String, Object>) map.get("today");
        Map<String,Object> totalMap = (Map<String, Object>) map.get("total");
        CovidProvinceDTO covidProvinceDTO = new CovidProvinceDTO();
        covidProvinceDTO.setId(IdWorker.getId(covidProvinceDTO));
        covidProvinceDTO.setProvinceName(provinceName);
        //当日新增
        Integer confirmAdd = (Integer) todayMap.get("confirm");
        covidProvinceDTO.setConfirmAdd(confirmAdd);
        //现有确诊
        Integer nowConfirm = (Integer) totalMap.get("nowConfirm");
        covidProvinceDTO.setNowConfirm(nowConfirm);
        covidProvinceDTO.setConfirm((Integer) totalMap.get("confirm"));
        //无症状新增
        //无用字段
        Integer asymptomaticAdd =  (Integer) totalMap.get("wzz");
        covidProvinceDTO.setAsymptomaticAdd(asymptomaticAdd);
        covidProvinceDTO.setAsymptomatic(asymptomaticAdd);
        covidProvinceDTO.setSuspect(ObjectUtil.defaultIfNull((Integer) totalMap.get("suspect"),0));
        Integer dead = (Integer) totalMap.get("dead");
        Integer deadAdd = dead;
        String deadRate = (String) totalMap.get("deadRate");
        covidProvinceDTO.setDeadRate(new BigDecimal(ObjectUtil.defaultIfNull((deadRate),0).toString()));
        covidProvinceDTO.setDead(dead);
        Integer heal = (Integer) totalMap.get("heal");
        Integer healAdd = heal;
        String healRate = (String) totalMap.get("healRate");
        covidProvinceDTO.setHealRate(new BigDecimal(ObjectUtil.defaultIfNull((healRate),0).toString()));
        covidProvinceDTO.setHeal(heal);
        //昨日数据
        CovidProvince covidProvinceBefore = getByStatisticalDateAndProvinceName(yesterday,provinceName);
        if(covidProvinceBefore != null){
            //接口没有每天新增，今天总量-昨天的总量
            //新增死亡数
            deadAdd = dead - covidProvinceBefore.getDead();
            //新增治愈数
            healAdd = heal - covidProvinceBefore.getHeal();
            //较昨日新增确诊
            Integer confirmAddBefore = confirmAdd - covidProvinceBefore.getConfirmAdd();
            //较昨日新增无症状
            Integer asymptomaticAddBefore = asymptomaticAdd - covidProvinceBefore.getAsymptomaticAdd();
            //较昨日新增死亡数
            Integer deadAddBefore = deadAdd - covidProvinceBefore.getDeadAdd();
            //较昨日新增治愈
            Integer healAddBefore = healAdd - covidProvinceBefore.getHealAdd();
            //较昨日现有
            Integer nowConfirmBefore = nowConfirm - (covidProvinceBefore.getConfirmAdd() - covidProvinceBefore.getHealAdd() - covidProvinceBefore.getDeadAdd());

            covidProvinceDTO.setNowConfirmBefore(nowConfirmBefore);
            covidProvinceDTO.setConfirmAddBefore(confirmAddBefore);
            covidProvinceDTO.setAsymptomaticAddBefore(asymptomaticAddBefore);
            covidProvinceDTO.setDeadAddBefore(deadAddBefore);
            covidProvinceDTO.setHealAddBefore(healAddBefore);
        }
        covidProvinceDTO.setDeadAdd(deadAdd);
        covidProvinceDTO.setHealAdd(healAdd);
        covidProvinceDTO.setCreateTime(new Date());
        String lastUpdateTime = (String) map.get("lastUpdateTime");
        covidProvinceDTO.setLastUpdateTime(DateUtil.parseDateTime(lastUpdateTime));
        covidProvinceDTO.setStatisticalDate(DateUtil.addDays(new Date(),-1));
        List<CovidCity> covidCityList = covidCityService.getCovidCity((List<Map<String, Object>>) map.get("children"),covidProvinceDTO,yesterday);
        covidProvinceDTO.setCovidCityList(covidCityList);
        return covidProvinceDTO;
    }




}