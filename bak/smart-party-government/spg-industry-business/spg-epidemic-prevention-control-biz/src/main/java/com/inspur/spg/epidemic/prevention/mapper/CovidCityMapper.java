package com.inspur.spg.epidemic.prevention.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.epidemic.prevention.api.dto.CovidDTO;
import com.inspur.spg.epidemic.prevention.api.entity.CovidCity;
import com.inspur.spg.epidemic.prevention.api.vo.CovidStatsVO;
import com.inspur.spg.epidemic.prevention.api.vo.CovidVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 城市新冠疫情表(CovidCity)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-03 14:12:35
 */
@Mapper
public interface CovidCityMapper extends BaseMapper<CovidCity> {

    /**
     * @Author CJ
     * @Description 获取省份每日新增（因为省份不会记录境外参数，固在省份所在地市表查询）
     * @Date 2022/2/10 17:22
     * @param covidDto
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<CovidStatsVO> getProvinceConfirmAddList(CovidDTO covidDto);

    /**
     * @Author CJ
     * @Description 获取每日新增
     * @Date 2022/2/10 17:22
     * @param covidDto
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<CovidStatsVO> getConfirmAddList(CovidDTO covidDto);

    /**
     * 查询疫情信息
     * @param covidDto
     * @return
     */
    CovidVO getCovidVo(CovidDTO covidDto);


}