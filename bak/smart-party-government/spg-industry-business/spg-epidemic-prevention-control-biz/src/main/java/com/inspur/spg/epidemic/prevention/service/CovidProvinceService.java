package com.inspur.spg.epidemic.prevention.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.epidemic.prevention.api.dto.CovidDTO;
import com.inspur.spg.epidemic.prevention.api.dto.CovidProvinceDTO;
import com.inspur.spg.epidemic.prevention.api.entity.CovidProvince;
import com.inspur.spg.epidemic.prevention.api.vo.CovidVO;

import java.util.Date;
import java.util.Map;

/**
 * 省份疫情信息(CovidProvince)表服务接口
 *
 * @author CJ
 * @since 2022-01-03 14:12:36
 */
public interface CovidProvinceService extends IService<CovidProvince> {

    /**
     * @Author CJ
     * @Description dto保存
     * @Date 2022/1/5 11:34
     * @param covidProvinceDTO
     * @return void
     **/
    void saveCovidProvinceDTO(CovidProvinceDTO covidProvinceDTO);

    /**
     * @Author CJ
     * @Description 保存数据
     * @Date 2022/2/10 16:08
     * @param dataMap
     * @return void
     **/
    void saveCovid(Map<String,Object> dataMap);

    /**
     * @Author CJ
     * @Description 根据统计日期查询昨天数据
     * @Date 2022/1/5 14:55
     * @param statisticalDate
     * @param provinceName
     * @return com.inspur.spg.epidemic.prevention.api.entity.CovidProvince
     **/
    CovidProvince getByStatisticalDateAndProvinceName(Date statDate,String provinceName);

    /**
     * 根据统计日和省份查询
     * @param covidDto
     * @return
     */
    CovidVO getCovidVo(CovidDTO covidDto);

}