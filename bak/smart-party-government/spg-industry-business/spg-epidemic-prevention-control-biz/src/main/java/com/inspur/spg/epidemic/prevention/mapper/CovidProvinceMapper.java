package com.inspur.spg.epidemic.prevention.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.epidemic.prevention.api.dto.CovidDTO;
import com.inspur.spg.epidemic.prevention.api.entity.CovidProvince;
import com.inspur.spg.epidemic.prevention.api.vo.CovidVO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 省份疫情信息(CovidProvince)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-03 14:12:36
 */
@Mapper
public interface CovidProvinceMapper extends BaseMapper<CovidProvince> {

    /**
     * 根据日期和省份名称查询
     * @param statDate
     * @param provinceName
     * @return
     */
    CovidVO getCovidVo(CovidDTO covidDto);

}