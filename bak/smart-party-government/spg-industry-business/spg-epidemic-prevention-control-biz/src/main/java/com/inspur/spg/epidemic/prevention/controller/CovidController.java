package com.inspur.spg.epidemic.prevention.controller;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.epidemic.prevention.api.dto.CovidDTO;
import com.inspur.spg.epidemic.prevention.api.vo.CovidVO;
import com.inspur.spg.epidemic.prevention.service.CovidCityService;
import com.inspur.spg.epidemic.prevention.service.CovidProvinceService;
import io.swagger.annotations.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/4 15:03
 **/
@RestController
@RequestMapping("/covid")
@Api(value = "covid", tags = "疫情模块")
public class CovidController {

    @Resource
    private CovidProvinceService covidProvinceService;
    @Resource
    private CovidCityService covidCityService;

    @PostMapping("/saveBatch")
    @ApiIgnore
    @Inner
    public R saveBatch(@RequestBody Map<String,Object> dataMap){
        covidProvinceService.saveCovid(dataMap);
        return R.ok();
    }

    /**
     * @Author CJ
     * @Description 获取疫情统计图
     * @Date 2022/2/10 17:54
     * @param covidDto
     * @return com.inspur.spg.common.core.util.R<java.util.List<com.inspur.spg.common.core.entity.ChartsInfo<java.lang.Integer>>>
     **/
    @GetMapping("/getStats")
    @ApiOperation(value = "获取疫情统计数据", notes = "获取疫情统计数据")
    public R<List<ChartsInfo<Integer>>> getStats(@Valid CovidDTO covidDto){
        if(covidDto.getStartDate() == null || covidDto.getEndDate() == null){
            return R.failed("开始日期和结束日期不能为空");
        }
        return R.ok(covidCityService.getChartsInfo(covidDto));
    }

    /**
     * @Author CJ
     * @Description 根据省份获取疫情数据
     * @Date 2022/2/10 17:56
     * @param covidDto
     * @param
     * @return com.inspur.spg.common.core.util.R<com.inspur.spg.epidemic.prevention.api.entity.CovidProvince>
     **/
    @GetMapping("/getData")
    @ApiOperation(value = "根据省份获取疫情数据", notes = "根据省份获取疫情数据")
    public R<CovidVO> getData(@Valid CovidDTO covidDto){
        if(covidDto.getStatDate() == null){
            return R.failed("查询日期不能为空");
        }
        if(StringUtils.isBlank(covidDto.getCityName())){
            return R.ok(covidProvinceService.getCovidVo(covidDto));
        }
        return R.ok(covidCityService.getCovidVo(covidDto));
    }

}
