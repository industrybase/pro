package com.inspur.spg.epidemic.prevention.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.epidemic.prevention.api.dto.CovidDTO;
import com.inspur.spg.epidemic.prevention.api.dto.CovidProvinceDTO;
import com.inspur.spg.epidemic.prevention.api.entity.CovidCity;
import com.inspur.spg.epidemic.prevention.api.vo.CovidStatsVO;
import com.inspur.spg.epidemic.prevention.api.vo.CovidVO;
import com.inspur.spg.epidemic.prevention.mapper.CovidCityMapper;
import com.inspur.spg.epidemic.prevention.service.CovidCityService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 城市新冠疫情表(CovidCity)表服务实现类
 *
 * @author CJ
 * @since 2022-01-03 14:12:35
 */
@Service
public class CovidCityServiceImpl extends ServiceImpl<CovidCityMapper, CovidCity> implements CovidCityService {

    @Override
    public CovidCity getByStatisticalDateAndProvinceNameAndCityName(Date statisticalDate, String provinceName, String cityName) {
        return  baseMapper.selectOne(
                Wrappers.<CovidCity>lambdaQuery().eq(CovidCity::getStatisticalDate, statisticalDate).eq(CovidCity::getProvinceName,provinceName).eq(CovidCity::getCityName,cityName));
    }

    @Override
    public List<CovidCity> getCovidCity(List<Map<String, Object>> covidCityList, CovidProvinceDTO covidProvinceDto, Date yesterday) {
        List<CovidCity> saveList = covidCityList.stream().map(map ->{
            String cityName = (String) map.get("name");
            Map<String,Object> todayMap = (Map<String, Object>) map.get("today");
            Map<String,Object> totalMap = (Map<String, Object>) map.get("total");
            CovidCity covidCity = new CovidCity();
            covidCity.setProvinceId(covidProvinceDto.getId());
            covidCity.setId(IdWorker.getId(covidCity));
            covidCity.setProvinceName(covidProvinceDto.getProvinceName());
            covidCity.setCityName(cityName);
            //当日新增
            Integer confirmAdd = (Integer) todayMap.get("confirm");
            covidCity.setConfirmAdd(confirmAdd);
            //现有确诊
            Integer nowConfirm = (Integer) totalMap.get("nowConfirm");
            covidCity.setNowConfirm(nowConfirm);
            covidCity.setConfirm((Integer) totalMap.get("confirm"));
            Integer asymptomatic = (Integer) totalMap.get("wzz");
            Integer asymptomaticAdd = asymptomatic;
            covidCity.setAsymptomatic(asymptomatic);
            covidCity.setAsymptomaticAdd(asymptomaticAdd);
            covidCity.setSuspect(ObjectUtil.defaultIfNull((Integer) totalMap.get("suspect"),0));
            Integer dead = (Integer) totalMap.get("dead");
            Integer deadAdd = dead;
            String deadRate = (String) totalMap.get("deadRate");
            covidCity.setDeadRate(new BigDecimal(ObjectUtil.defaultIfNull((deadRate),0).toString()));
            covidCity.setDead(dead);
            Integer heal = (Integer) totalMap.get("heal");
            Integer healAdd = heal;
            String healRate = (String) totalMap.get("healRate");
            covidCity.setHealRate(new BigDecimal(ObjectUtil.defaultIfNull((healRate),0).toString()));
            covidCity.setHeal(heal);
            CovidCity covidCityBefore = getByStatisticalDateAndProvinceNameAndCityName(yesterday,covidProvinceDto.getProvinceName(),cityName);
            if(covidCityBefore != null){
                //接口没有每天新增，今天总量-昨天的总量
                //新增死亡数
                healAdd = heal - covidCityBefore.getHeal();
                //新增治愈数
                deadAdd = dead - covidCityBefore.getDead();
                //新增无症状
                //asymptomaticAdd = asymptomatic - covidCityBefore.getAsymptomatic();
                //较昨日新增确诊
                Integer confirmAddBefore = confirmAdd - covidCityBefore.getConfirmAdd();
                //较昨日新增无症状
                Integer asymptomaticAddBefore = asymptomaticAdd - covidCityBefore.getAsymptomaticAdd();
                //较昨日新增死亡数
                Integer deadAddBefore = deadAdd - covidCityBefore.getDeadAdd();
                //较昨日新增治愈
                Integer healAddBefore = healAdd - covidCityBefore.getHealAdd();
                //较昨日现有
                Integer nowConfirmBefore = nowConfirm - (covidCityBefore.getConfirmAdd() - covidCityBefore.getHealAdd() - covidCityBefore.getDeadAdd());

                covidCity.setNowConfirmBefore(nowConfirmBefore);
                covidCity.setConfirmAddBefore(confirmAddBefore);
                covidCity.setAsymptomaticAddBefore(asymptomaticAddBefore);
                covidCity.setDeadAddBefore(deadAddBefore);
                covidCity.setHealAddBefore(healAddBefore);
            }
            //covidCity.setAsymptomaticAdd(asymptomaticAdd);
            covidCity.setDeadAdd(deadAdd);
            covidCity.setHealAdd(healAdd);
            covidCity.setCreateTime(new Date());
            covidCity.setGrade((String) totalMap.get("grade"));
            covidCity.setLastUpdateTime(covidProvinceDto.getLastUpdateTime());
            covidCity.setStatisticalDate(covidProvinceDto.getStatisticalDate());
            return covidCity;
        }).collect(Collectors.toList());
        return saveList;
    }

    @Override
    public List<ChartsInfo<Integer>> getChartsInfo(CovidDTO covidDto) {
        List<CovidStatsVO> list;
        if(StringUtils.isNotBlank(covidDto.getProvinceName()) && StringUtils.isBlank(covidDto.getCityName())){
            list = baseMapper.getProvinceConfirmAddList(covidDto);
        }else{
            list = baseMapper.getConfirmAddList(covidDto);
        }
        List<ChartsInfo<Integer>> chartsInfos = list.stream().map(item -> {
            ChartsInfo<Integer> chartsInfo = new ChartsInfo<>();
            Map<String,Integer> valuesMap = new HashMap<>(3);
            //一共新增
            valuesMap.put("totalConfirmAdd",item.getTotalConfirmAdd());
            //新增本土
            valuesMap.put("localConfirmAdd",item.getLocalConfirmAdd());
            //新增境外
            valuesMap.put("foreignConfirmAdd",item.getForeignConfirmAdd());
            //日期
            chartsInfo.setTitle(DateUtil.formatDate(item.getStatDate()));
            chartsInfo.setValuesMap(valuesMap);
            return chartsInfo;
        }).collect(Collectors.toList());
        return chartsInfos;
    }

    @Override
    public CovidVO getCovidVo(CovidDTO covidDto) {
        return baseMapper.getCovidVo(covidDto);
    }

}