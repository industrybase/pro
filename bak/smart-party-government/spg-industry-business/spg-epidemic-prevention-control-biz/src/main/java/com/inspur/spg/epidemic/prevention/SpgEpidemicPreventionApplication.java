package com.inspur.spg.epidemic.prevention;

import com.inspur.spg.common.feign.annotation.EnableSpgFeignClients;
import com.inspur.spg.common.security.annotation.EnableSpgResourceServer;
import com.inspur.spg.common.swagger.annotation.EnableSpgSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Profile;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/4 15:17
 **/
@EnableSpgFeignClients
@EnableDiscoveryClient
@EnableSpgResourceServer
@SpringBootApplication
@EnableSpgSwagger2
public class SpgEpidemicPreventionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpgEpidemicPreventionApplication.class, args);
    }


}
