package com.inspur.spg.epidemic.prevention.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.epidemic.prevention.api.dto.CovidDTO;
import com.inspur.spg.epidemic.prevention.api.dto.CovidProvinceDTO;
import com.inspur.spg.epidemic.prevention.api.entity.CovidCity;
import com.inspur.spg.epidemic.prevention.api.vo.CovidVO;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 城市新冠疫情表(CovidCity)表服务接口
 *
 * @author CJ
 * @since 2022-01-03 14:12:35
 */
public interface CovidCityService extends IService<CovidCity> {

    /**
     * @Author CJ
     * @Description 根据省份城市日期
     * @Date 2022/1/5 15:23
     * @param statisticalDate
     * @param provinceName
     * @param cityName
     * @return com.inspur.spg.epidemic.prevention.api.entity.CovidCity
     **/
    CovidCity getByStatisticalDateAndProvinceNameAndCityName(Date statisticalDate, String provinceName, String cityName);

    /**
     * @Author CJ
     * @Description 根据接口获取的数据返回
     * @Date 2022/1/5 15:27
     * @param covidCityList
     * @return java.util.List<com.inspur.spg.epidemic.prevention.api.entity.CovidCity>
     **/
    List<CovidCity> getCovidCity(List<Map<String,Object>> covidCityList, CovidProvinceDTO covidProvinceDto, Date yesterday);

    /**
     * @Author CJ
     * @Description
     * @Date 2022/2/10 16:09
     * @param
     * @return java.util.List<com.inspur.spg.common.core.entity.ChartsInfo<java.math.BigDecimal>>
     **/
    List<ChartsInfo<Integer>> getChartsInfo(CovidDTO covidDto);

    /**
     * 查询疫情数据
     * @param covidDto
     * @return
     */
    CovidVO getCovidVo(CovidDTO covidDto);

}