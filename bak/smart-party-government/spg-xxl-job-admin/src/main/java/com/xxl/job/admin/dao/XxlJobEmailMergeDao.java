package com.xxl.job.admin.dao;

import com.xxl.job.admin.core.model.XxlJobEmailMerge;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: data-monitoring-job
 * @description:
 * @author: CJ
 * @create: 2021-01-15 15:02
 **/
@Mapper
public interface XxlJobEmailMergeDao {


    /**
     * 查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    List<XxlJobEmailMerge> listAll();

    /**
     * 根据邮箱查询
     * @param email
     * @return
     */
    List<XxlJobEmailMerge> listByEmail(String email);

    /**
     * 根据任务id查询所有记录
     *
     * @return 返回集合，没有返回空List
     */
    XxlJobEmailMerge getByJobId(@Param("jobId") Integer jobId);

    List<String> getEmailList();


    /**
     * 根据主键查询
     *
     * @param id 主键
     * @return 返回记录，没有返回null
     */
    XxlJobEmailMerge getById(Integer id);

    /**
     * 新增，插入所有字段
     *
     * @param xxlJobEmailMerge 新增的记录
     * @return 返回影响行数
     */
    int insert(XxlJobEmailMerge xxlJobEmailMerge);

    /**
     * 批量新增，忽略null字段
     *
     * @param list 新增的记录
     * @return 返回影响行数
     */
    int insertBatch(List<XxlJobEmailMerge> list);


    /**
     * 新增，忽略null字段
     *
     * @param xxlJobEmailMerge 新增的记录
     * @return 返回影响行数
     */
    int insertIgnoreNull(XxlJobEmailMerge xxlJobEmailMerge);



    /**
     * 修改，修改所有字段
     *
     * @param xxlJobEmailMerge 修改的记录
     * @return 返回影响行数
     */
    int update(XxlJobEmailMerge xxlJobEmailMerge);

    /**
     * 修改，忽略null字段
     *
     * @param xxlJobEmailMerge 修改的记录
     * @return 返回影响行数
     */
    int updateIgnoreNull(XxlJobEmailMerge xxlJobEmailMerge);

    /**
     * 删除记录
     *
     * @param xxlJobEmailMerge 待删除的记录
     * @return 返回影响行数
     */
    int delete(XxlJobEmailMerge xxlJobEmailMerge);

    /**
     * 删除记录
     *
     * @param email 待删除的邮箱
     * @return 返回影响行数
     */
    int deleteByEmail(String email);

    /**
     * 根据任务id删除
     * @param jobIds
     * @return
     */
    int deleteByJobIds(@Param("jobIds") List<Integer> jobIds);

}
