package com.xxl.job.admin.core.model;

import java.util.Date;

/**
 * 任务合并告警邮件
 */
public class XxlJobEmailMerge {
    private Integer id;
    /** 任务表id */
    private Integer jobId;
    /** 邮件，逗号隔开 */
    private String email;
    /** 创建时间 */
    private Date createTime;
    /** 创建人 */
    private Integer creator;

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getJobId() {
        return this.jobId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public Integer getCreator() {
        return this.creator;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) {return false;}
        XxlJobEmailMerge that = (XxlJobEmailMerge) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(id);
    }

    @Override
    public String toString() {
        return "XxlJobEmailMerge{" +
                "id=" + id +
                ",jobId='" + jobId + "'" +
                ",email='" + email + "'" +
                ",createTime='" + createTime + "'" +
                ",creator='" + creator + "'" +
                '}';
    }

}
