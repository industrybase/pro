package com.xxl.job.admin.core.alarm.event;

import org.springframework.context.ApplicationEvent;

/*
 * @Author CJ
 * @Description 告警短信发送事件
 * @Date 2021/5/20 11:54
 **/
public class AlarmSmsJobEvent extends ApplicationEvent {

	private String method;

	public String getMethod() {
		return method;
	}

	public AlarmSmsJobEvent(Object source, String method) {
		super(source);
		this.method = method;
	}

	public AlarmSmsJobEvent(Object source) {
		super(source);
	}

}
