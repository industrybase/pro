package com.xxl.job.admin.core.alarm.event;

import com.xxl.job.admin.core.model.AlarmSmsJob;
import com.xxl.job.admin.dao.AlarmSmsJobDao;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

import java.util.Date;

/*
 * @Author CJ
 * @Description
 * @Date 2021/5/20 11:55
 **/
public class AlarmSmsJobListener {

	private AlarmSmsJobDao alarmSmsJobDao;

	public AlarmSmsJobListener(AlarmSmsJobDao alarmSmsJobDao) {
		this.alarmSmsJobDao = alarmSmsJobDao;
	}

	@Async
	@Order
	@EventListener(AlarmSmsJobEvent.class)
	public void saveSmsJob(AlarmSmsJobEvent event) {
		Integer jobId = (Integer) event.getSource();
		if(event.getMethod().equals("delete")){
			alarmSmsJobDao.deleteByJobId(jobId);
		}else{
			AlarmSmsJob alarmSmsJob = new AlarmSmsJob();
			alarmSmsJob.setCreateTime(new Date());
			alarmSmsJob.setJobId(jobId);
			alarmSmsJobDao.insertIgnoreNull(alarmSmsJob);
		}
	}

}
