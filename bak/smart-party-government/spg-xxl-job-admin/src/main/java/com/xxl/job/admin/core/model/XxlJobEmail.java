package com.xxl.job.admin.core.model;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @program: data-monitoring-job
 * @description:
 * @author: CJ
 * @create: 2021-01-15 10:49
 **/
public class XxlJobEmail {

    public static LinkedBlockingQueue<XxlJobEmail> xxlJobEmailLinkedBlockingQueue = new LinkedBlockingQueue<>();

    private XxlJobInfo xxlJobInfo;

    private XxlJobLog xxlJobLog;

    public XxlJobInfo getXxlJobInfo() {
        return xxlJobInfo;
    }

    public void setXxlJobInfo(XxlJobInfo xxlJobInfo) {
        this.xxlJobInfo = xxlJobInfo;
    }

    public XxlJobLog getXxlJobLog() {
        return xxlJobLog;
    }

    public void setXxlJobLog(XxlJobLog xxlJobLog) {
        this.xxlJobLog = xxlJobLog;
    }
}
