package com.xxl.job.admin.dao;

import com.xxl.job.admin.core.model.AlarmSmsJob;
import com.xxl.job.admin.core.model.XxlJobEmailMerge;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AlarmSmsJobDao {

	/**
	 * 新增，插入所有字段
	 *
	 * @param alarmSmsJob 新增的记录
	 * @return 返回影响行数
	 */
	int insert(AlarmSmsJob alarmSmsJob);



	/**
	 * 新增，忽略null字段
	 *
	 * @param alarmSmsJob 新增的记录
	 * @return 返回影响行数
	 */
	int insertIgnoreNull(AlarmSmsJob alarmSmsJob);

	/**
	 * 删除记录
	 *
	 * @param JobId 待删除的任务Id
	 * @return 返回影响行数
	 */
	int deleteByJobId(Integer JobId);

}
