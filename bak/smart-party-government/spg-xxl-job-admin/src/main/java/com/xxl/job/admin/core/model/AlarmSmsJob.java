package com.xxl.job.admin.core.model;



import java.time.LocalDateTime;
import java.util.Date;

/*
 * @Author CJ
 * @Description
 * @Date 2021/5/21 21:20
 **/
public class AlarmSmsJob {



	/**
	 *
	 */
	private Integer id;

	/**
	 * 任务id
	 */
	private Integer jobId;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 联系人名称
	 */
	private String contactsName;
	/**
	 * 创建时间
	 */
	private Date createTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getContactsName() {
		return contactsName;
	}

	public void setContactsName(String contactsName) {
		this.contactsName = contactsName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
