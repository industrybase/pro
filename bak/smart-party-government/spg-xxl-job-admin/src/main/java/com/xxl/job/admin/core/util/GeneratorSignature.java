package com.xxl.job.admin.core.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/*
 * @Author CJ
 * @Description
 * @Date 2021/12/6 15:58
 **/
public class GeneratorSignature {

	public static void main(String[] args) {
		String stringToSign="GET"+"\n";
		String url="/FE7oHg5GNlPKdmcjMJ2GEHwDep8IhzLr/uf5vdnwrih4k/dyservice/ZYCZHCX/GDSKeYunBaoCheXianLu";
		stringToSign=stringToSign+url;
		String secret="01d4b34b7c45a205a2f57abfb448cec0";
		try{
			Mac hmacSha256 = Mac.getInstance("HmacSHA256");
			byte[] keyBytes = secret.getBytes("UTF-8");
			hmacSha256.init(new SecretKeySpec(keyBytes, 0, keyBytes.length, "HmacSHA256"));
			byte[]  hmacSha256string=hmacSha256.doFinal(stringToSign.getBytes("UTF-8"));
//          log.error("hmacSha256string:"+byteArrayToHexString(hmacSha256string));
			String sign= byteArrayToHexString(hmacSha256string);
			System.out.print(sign);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public static String generatorSignature(String url, String appSecret) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
		String stringToSign = "GET" + "\n";
		stringToSign = stringToSign + url;
		String secret = appSecret;
		Mac hmacSha256 = Mac.getInstance("HmacSHA256");
		byte[] keyBytes = secret.getBytes("UTF-8");
		hmacSha256.init(new SecretKeySpec(keyBytes, 0, keyBytes.length, "HmacSHA256"));
		byte[] hmacSha256string = hmacSha256.doFinal(stringToSign.getBytes("UTF-8"));
		String sign = byteArrayToHexString(hmacSha256string);
		return sign;
	}

	public static String byteArrayToHexString(byte[] b) {
		StringBuilder hs = new StringBuilder();
		String stmp;
		for (int n = 0; b != null && n < b.length; n++) {
			stmp = Integer.toHexString(b[n] & 0XFF);
			if (stmp.length() == 1)
				hs.append('0');
			hs.append(stmp);
		}
		return hs.toString().toLowerCase();
	}

}
