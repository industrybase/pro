package com.xxl.job.admin.core.thread;

import com.xxl.job.admin.core.conf.XxlJobAdminConfig;
import com.xxl.job.admin.core.model.XxlJobEmail;
import com.xxl.job.admin.core.model.XxlJobEmailMerge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @program: data-monitoring-job
 * @description:
 * @author: CJ
 * @create: 2021-01-17 14:23
 **/
public class JobFailMonitorBatchHelper {

    private static Logger logger = LoggerFactory.getLogger(JobFailMonitorHelper.class);

    private static JobFailMonitorBatchHelper instance = new JobFailMonitorBatchHelper();

    public static JobFailMonitorBatchHelper getInstance() {
        return instance;
    }

    private Thread monitorThread;
    private volatile boolean toStop = false;

    public void start() {
        monitorThread = new Thread(new Runnable() {

            @Override
            public void run() {
                // monitor
                while (!toStop) {
                    try {
						Calendar rightNow = Calendar.getInstance();
						int hour = rightNow.get(Calendar.HOUR_OF_DAY);
						if(!(hour >= 8 && hour <= 18)){
							XxlJobEmail.xxlJobEmailLinkedBlockingQueue = new LinkedBlockingQueue<>();
						}
                        int newAlarmStatus = 0;        // 告警状态：0-默认、-1=锁定状态、1-无需告警、2-告警成功、3-告警失败
                        if (XxlJobEmail.xxlJobEmailLinkedBlockingQueue.size() > 0) {
                            List<XxlJobEmail> xxlJobEmailList = new ArrayList<>();
                            XxlJobEmail.xxlJobEmailLinkedBlockingQueue.drainTo(xxlJobEmailList);
                            Map<String, List<XxlJobEmail>> emailMap = new HashMap<>();
                            //获取所有批次邮件信息
                            List<String> emailList = XxlJobAdminConfig.getAdminConfig().getXxlJobEmailMergeDao().getEmailList();
                            for(String email : emailList){
                                List<XxlJobEmail> jobEmails = new ArrayList<>();
                                for (XxlJobEmail xxlJobEmail : xxlJobEmailList) {
                                    XxlJobEmailMerge xxlJobEmailMerge = XxlJobAdminConfig.getAdminConfig().getXxlJobEmailMergeDao().getByJobId(xxlJobEmail.getXxlJobLog().getJobId());
                                    if(email.equals(xxlJobEmailMerge.getEmail())){
                                        jobEmails.add(xxlJobEmail);
                                    }
                                }
                                emailMap.put(email, jobEmails);
                            }

                            for (Map.Entry<String, List<XxlJobEmail>> entry : emailMap.entrySet()) {
                                boolean alarmResult = XxlJobAdminConfig.getAdminConfig().getJobAlarmer().alarm(entry.getValue(), entry.getKey());
                                newAlarmStatus = alarmResult ? 2 : 3;
                                List<Long> failLogIds = xxlJobEmailList.stream().map(item -> item.getXxlJobLog().getId()).collect(Collectors.toList());
                                XxlJobAdminConfig.getAdminConfig().getXxlJobLogDao().updateAlarmStatusBatch(failLogIds, -1, newAlarmStatus);
                            }
                        }
                    } catch (Exception e) {
                        if (!toStop) {
                            logger.error(">>>>>>>>>>> xxl-job, job fail monitor batch thread error:{}", e);
                        }
                    }
					try {
                        TimeUnit.HOURS.sleep(1);
                    } catch (Exception e) {
                        if (!toStop) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                }
                logger.info(">>>>>>>>>>> xxl-job, job fail monitor thread stop");

            }
        });
        monitorThread.setDaemon(true);
        monitorThread.setName("xxl-job, admin JobFailMonitorBatchHelper");
        monitorThread.start();
    }

    public void toStop() {
        toStop = true;
        // interrupt and wait
        monitorThread.interrupt();
        try {
            monitorThread.join();
        } catch (InterruptedException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
