package com.xxl.job.admin.core.alarm;

import com.xxl.job.admin.core.model.XxlJobEmail;
import com.xxl.job.admin.core.model.XxlJobInfo;
import com.xxl.job.admin.core.model.XxlJobLog;

import java.util.List;

/**
 * @author xuxueli 2020-01-19
 */
public interface JobAlarm {

    /**
     * job alarm
     *
     * @param info
     * @param jobLog
     * @return
     */
    public boolean doAlarm(XxlJobInfo info, XxlJobLog jobLog);

    /**
     * job alarm
     *
     * @param xxlJobEmails
     * @param alarmEmail
     * @return
     */
    public boolean doAlarm(List<XxlJobEmail> xxlJobEmails,String alarmEmail);


}
