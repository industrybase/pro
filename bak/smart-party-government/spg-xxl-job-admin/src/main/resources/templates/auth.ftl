<!DOCTYPE html>
<html>
<head>
    <#import "./common/common.macro.ftl" as netCommon>
    <@netCommon.commonStyle />
    <title>${I18n.admin_name}</title>
</head>
<body>

<@netCommon.commonScript />
</body>
</html>
<script>
$(function () {

    var code = getParam('code');
    if (code) {
        doLogin(code);
    }

    // 根据code授权码进行登录
    function doLogin(code) {
        $.ajax({
            url: base_url + '/authCodeLogin?code=' + code,
            dataType: 'json',
            success: function (res) {
                console.log('返回：', res);
                if (res.code == "200") {
                    layer.msg( I18n.login_success );
                    setTimeout(function(){
                        window.location.href = base_url;
                    }, 500);
                } else {
                    layer.msg(res.msg);
                }
            },
            error: function (xhr, type, errorThrown) {
                return layer.alert("异常：" + JSON.stringify(xhr));
            }
        });
    }
});

// 从url中查询到指定名称的参数值
function getParam(name, defaultValue) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == name) {
            return pair[1];
        }
    }
    return (defaultValue == undefined ? null : defaultValue);
}
</script>