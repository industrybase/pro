package com.inspur.spg.common.core.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description charts图标统一对象
 * @Date 2022/2/10 10:04
 **/
@Data
public class ChartsInfo<T> {

    //数值
    private T values;
    //标题
    private String title;
    //数值map
    private Map<String,T> valuesMap;

}
