package com.inspur.spg.common.core.util;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * list工具类
 */
public class ListUtils {


    /**
     * 计算切分次数
     */
    private static Integer countStep(Integer size, int input) {
        return (size + input - 1) / input;
    }

    /**
     * @param list  需要分隔的 集合
     * @param size 指定分隔size
     * @return
     */
    public static List<List> splitList(List<?> list, int size) {
        int limit = countStep(list.size(), size);
        List<List> splitList = Stream.iterate(0, n -> n + 1).limit(limit).
                map(a -> list.stream().skip(a * size).limit(size).collect(Collectors.toList())).
                collect(Collectors.toList());
        //当输入数量小于分隔数量需反转
        if (size < limit){
            splitList = Stream.iterate(0, n -> n + 1).limit(size).
                    map(a -> list.stream().skip(a * limit).limit(limit).collect(Collectors.toList())).
                    collect(Collectors.toList());
        }
        return splitList;
    }


}
