package com.inspur.spg.common.core.util;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * @program: xxl-api
 * @description:
 * @author: CJ
 * @create: 2020-12-24 17:16
 **/
public class Md5Utils {

	public static final String KEY_MAC = "HmacMD5";// "Hmacsha1"  ||  "Hmacsha256"

	/**
     * 生成32位md5
     *
     * @param str
     * @return
     */
    public static String string2Md5(String str) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");

            char[] charArray = str.toCharArray();
            byte[] byteArray = new byte[charArray.length];

            for (int i = 0; i < charArray.length; i++) {
                byteArray[i] = (byte) charArray[i];
            }
            byte[] md5Bytes = md5.digest(byteArray);

            StringBuffer hexValue = new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;
                if (val < 16) {
                    hexValue.append("0");
                }
                hexValue.append(Integer.toHexString(val));
            }

            return hexValue.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * 生成16位md5
     *
     * @param str
     * @return
     */
    public static String string2Md5_16(String str) {
        String md5 = string2Md5(str);
        return md5.substring(8, 24);
    }

	//初始化秘钥
	public static String initKey(){
		try{
			KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_MAC);
			return Base64.getEncoder().encodeToString(keyGenerator.generateKey().getEncoded());//秘钥使用base64加密存储
		}catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	//数据进行加密
	public static String encryption(String content,String key){
		try{
			SecretKeySpec  secretKey =new SecretKeySpec(Base64.getDecoder().decode(key),KEY_MAC);
			Mac mac=Mac.getInstance(KEY_MAC);
			mac.init(secretKey);
			mac.update(content.getBytes());
			return toHex(mac.doFinal()); //加密结果使用16进制字符串进行保存
		}catch (NoSuchAlgorithmException | InvalidKeyException ex){
			System.out.println(ex.getMessage());
		}
		return null;
	}
	//转16进制字符串
	public static String toHex(byte[] bytes) {
		BigInteger bi = new BigInteger(1, bytes);
		return String.format("%0" + (bytes.length << 1) + "X", bi);
	}


}
