package com.inspur.spg.common.core.constant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum GenderEnum {

    male("1","男性"), female("2","女性"),unknown("3","未知");

    private final String code;

    private final String name;

    public String getName(String code) {
        for (GenderEnum g : GenderEnum.values()) {
            if (g.getCode().equals(code)) {
                return g.name;
            }
        }
        return null;
    }

}
