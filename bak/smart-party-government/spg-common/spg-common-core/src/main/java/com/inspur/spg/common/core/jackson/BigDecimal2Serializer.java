package com.inspur.spg.common.core.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * BigDecimal转两位小数
 */
public class BigDecimal2Serializer extends JsonSerializer<BigDecimal> {


    @Override
    public void serialize(BigDecimal data, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (data != null) {
            jsonGenerator.writeNumber(data.setScale(2, BigDecimal.ROUND_HALF_UP));
        }

    }


}
