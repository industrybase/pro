package com.inspur.spg.common.core.constant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CityShortEnum {

    SG("韶关"),YJ("阳江"),MM("茂名"),ZQ("肇庆"),ZH("珠海"),CZ("潮州"),ZJ("湛江"),QY("清远"),SZ("深圳"),HY("河源"),JM("江门"),SW("汕尾"),ST("汕头"),MZ("梅州"),JY("揭阳"),HZ("惠州"),GZ("广州"),FS("佛山"),YF("云浮"),ZS("中山"),DG("东莞");

    private final String name;

}
