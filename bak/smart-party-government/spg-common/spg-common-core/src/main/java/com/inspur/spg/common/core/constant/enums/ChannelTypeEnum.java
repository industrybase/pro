package com.inspur.spg.common.core.constant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 渠道类型
 */
@Getter
@RequiredArgsConstructor
public enum ChannelTypeEnum {

    WATERWAY("3","水路"),HIGHWAY("1","公路"),RAILWAY("4","铁路"),CIVIL_AVIATION("2","民航");

    /**
     * 类型
     */
    private final String type;

    /**
     * 描述
     */
    private final String description;

}
