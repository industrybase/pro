package com.inspur.spg.common.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * date util
 *
 * @author
 */
public class DateUtil {

	// ---------------------- format parse ----------------------
	private static Logger logger = LoggerFactory.getLogger(DateUtil.class);

	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private static final ThreadLocal<Map<String, DateFormat>> dateFormatThreadLocal = new ThreadLocal<Map<String, DateFormat>>();

	private static DateFormat getDateFormat(String pattern) {
		if (pattern == null || pattern.trim().length() == 0) {
			throw new IllegalArgumentException("pattern cannot be empty.");
		}

		Map<String, DateFormat> dateFormatMap = dateFormatThreadLocal.get();
		if (dateFormatMap != null && dateFormatMap.containsKey(pattern)) {
			return dateFormatMap.get(pattern);
		}

		synchronized (dateFormatThreadLocal) {
			if (dateFormatMap == null) {
				dateFormatMap = new HashMap<String, DateFormat>();
			}
			dateFormatMap.put(pattern, new SimpleDateFormat(pattern));
			dateFormatThreadLocal.set(dateFormatMap);
		}

		return dateFormatMap.get(pattern);
	}

	/**
	 * format datetime. like "yyyy-MM-dd"
	 *
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static String formatDate(Date date) {
		return format(date, DATE_FORMAT);
	}

	/**
	 * format date. like "yyyy-MM-dd HH:mm:ss"
	 *
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static String formatDateTime(Date date) {
		return format(date, DATETIME_FORMAT);
	}

	/**
	 * format date
	 *
	 * @param date
	 * @param patten
	 * @return
	 * @throws ParseException
	 */
	public static String format(Date date, String patten) {
		return getDateFormat(patten).format(date);
	}

	/**
	 * parse date string, like "yyyy-MM-dd HH:mm:s"
	 *
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public static Date parseDate(String dateString) {
		return parse(dateString, DATE_FORMAT);
	}

	/**
	 * parse datetime string, like "yyyy-MM-dd HH:mm:ss"
	 *
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public static Date parseDateTime(String dateString) {
		return parse(dateString, DATETIME_FORMAT);
	}

	/**
	 * parse date
	 *
	 * @param dateString
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static Date parse(String dateString, String pattern) {
		try {
			Date date = getDateFormat(pattern).parse(dateString);
			return date;
		} catch (Exception e) {
			logger.warn("parse date error, dateString = {}, pattern={}; errorMsg = {}", dateString, pattern, e.getMessage());
			return null;
		}
	}

	public static Date formatDateToDate(Date date,String sourceFormat,String targetFormat){
		if(date == null){
			return null;
		}
		String dateStr = format(date,sourceFormat);
		return parse(dateStr,targetFormat);
	}

	public static String getTimestampByFormatAndSize(String format, Long minute, int size) {
		try {
			String ret = null;
			Date date = new Date();
			if (minute == null) {
				ret = format(date,"yyyyMMddHHmmss");
			} else {
				ret = format(new Date((date.getTime() - minute * 60 * 1000)),"yyyyMMddHHmmss");
			}
//            System.out.println(ret);
			int m = (Integer.parseInt(ret.substring(10, 12)) - Integer.parseInt(ret.substring(10, 12)) % size);
			ret = ret.substring(0, 10) + (m < 10 ? ("0" + m) : m) + "00";

			Date retDate = parse(ret,"yyyyMMddHHmmss");
			return format(retDate,format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	// ---------------------- add date ----------------------

	public static Date addYears(final Date date, final int amount) {
		return add(date, Calendar.YEAR, amount);
	}

	public static Date addMonths(final Date date, final int amount) {
		return add(date, Calendar.MONTH, amount);
	}

	public static Date addDays(final Date date, final int amount) {
		return add(date, Calendar.DAY_OF_MONTH, amount);
	}

	public static Date addHours(final Date date, final int amount) {
		return add(date, Calendar.HOUR_OF_DAY, amount);
	}

	public static Date addMinutes(final Date date, final int amount) {
		return add(date, Calendar.MINUTE, amount);
	}

	public static Date addSecond(final Date date, final int amount) {
		return add(date, Calendar.SECOND, amount);
	}

	private static Date add(final Date date, final int calendarField, final int amount) {
		if (date == null) {
			return null;
		}
		final Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(calendarField, amount);
		return c.getTime();
	}

	public static Integer getMonth(final Date date){
		return getTimeType(Calendar.MONTH,date);
	}

	public static Integer getYear(final Date date){
		return getTimeType(Calendar.YEAR,date);
	}

	public static Integer getHour(final Date date){

		return getTimeType(Calendar.HOUR_OF_DAY,date);
	}

	public static Integer getMinute(final Date date){
		return getTimeType(Calendar.MINUTE,date);
	}

	public static Integer getSecond(final Date date){
		return getTimeType(Calendar.SECOND,date);
	}

	public static Integer getDay(final Date date){
		return getTimeType(Calendar.DAY_OF_YEAR,date);
	}

	public static Integer getTimeType(final int type,final Date date){
		if (date == null) {
			return null;
		}
		final Calendar c = Calendar.getInstance();
		c.setTime(date);
		if(type == Calendar.MONTH){
			return c.get(Calendar.MONTH) + 1;
		}
		return c.get(type);
	}

	//LocalDate -> Date
	public static Date asDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	//LocalDateTime -> Date
	public static Date asDate(LocalDateTime localDateTime) {
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	//Date -> LocalDate
	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	//Date -> LocalDateTime
	public static LocalDateTime asLocalDateTime(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	/**获取上n个小时整点小时时间
	 * @param
	 * @return
	 */
	public static String getLastHourTime(int n,String format){
		Calendar ca = Calendar.getInstance();
		ca.set(Calendar.MINUTE, 0);
		ca.set(Calendar.SECOND, 0);
		ca.set(Calendar.HOUR_OF_DAY, ca.get(Calendar.HOUR_OF_DAY)-n);
		Date date = ca.getTime();
		return format(date,format);
	}

	/**获取当前时间的整点小时时间
	 * @param
	 * @return
	 */
	public static String getCurrHourTime(String format){
		Calendar ca = Calendar.getInstance();
		ca.set(Calendar.MINUTE, 0);
		ca.set(Calendar.SECOND, 0);
		Date date = ca.getTime();
		return format(date,format);
	}

	/**
	 * 获取每一小时时间
	 * @param date
	 * @return
	 */
	public static List<Date> getHourlyList(Date date){
		if(date == null){
			return null;
		}
		Calendar ca = Calendar.getInstance();
		ca.setTime(date);
		List<Date> dates = new ArrayList<>();
		for(int i = 0; i < 24; i++){
			ca.set(Calendar.HOUR_OF_DAY, i);
			ca.set(Calendar.MINUTE, 0);
			ca.set(Calendar.SECOND, 0);
			dates.add(ca.getTime());
		}
		return dates;
	}

	public static Date getCurrDate(){
		return parseDate(formatDate(new Date()));
	}

	public static String getYearByMillisecond(long millisecond) {
		return format(new Date(millisecond),"yyyy");
	}

	public static String getMonthByMillisecond(long millisecond) {
		return format(new Date(millisecond),"MM");
	}

	public static String getYearJoinMonthByMillisecond(long millisecond) {
		return format(new Date(millisecond),"yyyyMM");
	}

}