package com.inspur.spg.common.core.util;

import com.inspur.spg.common.core.constant.CommonConstants;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author CJ
 * @Date 2022/3/2 10:20
 * @Description: 计算工具类
 */
public class CalculationUtils {

    /**
     * 计算增长率(例：环比、同比)
     * @param sameNum 当次数
     * @param lastNum 上次数
     * @return 占比(保留两位小数)
     */
    public static BigDecimal growthRate(double sameNum,double lastNum){
        double proportion = (sameNum-lastNum)/lastNum * 100d;
        return BigDecimal.valueOf(proportion).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * 计算迁徙人口(万人)
     * @param num
     * @return
     */
    public static BigDecimal getMigrationTenThousand(Integer num){
        return BigDecimal.valueOf(num.doubleValue() / CommonConstants.MIGRATION_THRESHOLD / 10000d).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

}
