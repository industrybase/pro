/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.spg.common.core.constant;

/**
 * @Author CJ
 * @Description 服务名称
 * @Date 2022/1/4 14:55
 **/
public interface ServiceNameConstants {

	//疫情模块
	String EPIDEMIC_PREVENTION_CONTROL_SERVICE = "spg-epidemic-prevention-control-biz";

	//流动分析模块
	String FLOW_ANALYSIS_SERVICE = "spg-flow-analysis-biz";

	//流动实时分析模型模块
	String REAL_FLOW_ANALYSIS_SERVICE = "spg-real-time-personnel-mobility-biz";

	/**
	 * UMPS模块
	 */
	String UMPS_SERVICE = "spg-upms-biz";

	/**
	 * 认证服务的SERVICEID
	 */
	String AUTH_SERVICE = "spg-auth";

}
