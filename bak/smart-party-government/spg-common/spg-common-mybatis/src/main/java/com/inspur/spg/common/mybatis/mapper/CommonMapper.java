package com.inspur.spg.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

public interface CommonMapper<T> extends BaseMapper<T> {

    /**
     * 全量插入,等价于insert，忽略唯一索引冲突的行
     * {@link InsertIgnoreBatchAllColumn}
     *
     * @param entityList
     * @return
     */
    int insertIgnoreBatchAllColumn(List<T> entityList);

    /**
     * 批量新增替换
     * {@link ReplaceIntoBatchAllColumn}
     *
     * @param entityList
     * @return
     */
    int replaceIntoBatchAllColumn(List<T> entityList);

}
