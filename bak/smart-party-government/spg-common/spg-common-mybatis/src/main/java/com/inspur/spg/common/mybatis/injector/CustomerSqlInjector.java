package com.inspur.spg.common.mybatis.injector;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.inspur.spg.common.mybatis.mapper.InsertIgnoreBatchAllColumn;
import com.inspur.spg.common.mybatis.mapper.ReplaceIntoBatchAllColumn;

import java.util.List;

/*
 * @Author CJ
 * @Description 注入sql
 * @Date 2022/1/7 15:25
 **/
public class CustomerSqlInjector extends DefaultSqlInjector {

    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass) {
        List<AbstractMethod> methodList = super.getMethodList(mapperClass);
        methodList.add(new InsertIgnoreBatchAllColumn());
        methodList.add(new ReplaceIntoBatchAllColumn());
        return methodList;
    }
}