package com.inspur.spg.common.mybatis.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface ICommonService<T> extends IService<T> {

    boolean fastSaveIgnoreBatch(List<T> list, int batchSize);

    boolean fastSaveIgnoreBatch(List<T> list);

    boolean replaceIntoBatchAllColumn(List<T> list, int batchSize);

    boolean replaceIntoBatchAllColumn(List<T> list);
}
