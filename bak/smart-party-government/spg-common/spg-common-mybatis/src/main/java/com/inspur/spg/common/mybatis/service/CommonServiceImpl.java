package com.inspur.spg.common.mybatis.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/7 15:27
 **/
public class CommonServiceImpl<M extends CommonMapper<T>, T> extends ServiceImpl<M, T> implements ICommonService<T>{

    private static final int BATCH_SIZE = 1000;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean fastSaveIgnoreBatch(List<T> list, int batchSize) {
        if(CollectionUtils.isEmpty(list)) {
            return true;
        }

        batchSize = batchSize < 1 ? BATCH_SIZE : batchSize;

        if(list.size() <= batchSize) {
            return retBool(baseMapper.insertIgnoreBatchAllColumn(list));
        }

        for (int fromIdx = 0 , endIdx = batchSize ; ; fromIdx += batchSize, endIdx += batchSize) {
            if(endIdx > list.size()) {
                endIdx = list.size();
            }
            baseMapper.insertIgnoreBatchAllColumn(list.subList(fromIdx, endIdx));
            if(endIdx == list.size()) {
                return true;
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean fastSaveIgnoreBatch(List<T> list) {
        return fastSaveIgnoreBatch(list, BATCH_SIZE);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean replaceIntoBatchAllColumn(List<T> list, int batchSize) {
        if(CollectionUtils.isEmpty(list)) {
            return true;
        }

        batchSize = batchSize < 1 ? BATCH_SIZE : batchSize;

        if(list.size() <= batchSize) {
            return retBool(baseMapper.replaceIntoBatchAllColumn(list));
        }

        for (int fromIdx = 0 , endIdx = batchSize ; ; fromIdx += batchSize, endIdx += batchSize) {
            if(endIdx > list.size()) {
                endIdx = list.size();
            }
            baseMapper.replaceIntoBatchAllColumn(list.subList(fromIdx, endIdx));
            if(endIdx == list.size()) {
                return true;
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean replaceIntoBatchAllColumn(List<T> list) {
        return replaceIntoBatchAllColumn(list,BATCH_SIZE);
    }

}

