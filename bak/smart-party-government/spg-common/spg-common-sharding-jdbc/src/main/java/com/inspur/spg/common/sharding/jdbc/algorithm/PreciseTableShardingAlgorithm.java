package com.inspur.spg.common.sharding.jdbc.algorithm;

import com.inspur.spg.common.core.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.Date;


public class PreciseTableShardingAlgorithm implements PreciseShardingAlgorithm<Date> {


    /**
     * 精确分片算法
     *
     * @param availableTargetNames 所有配置的表列表，这里代表所匹配到库的所有表
     * @param shardingValue        分片值，也就是dau_id的值
     * @return                     所匹配表的结果
     */
    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Date> shardingValue) {
        Date value = shardingValue.getValue();

        if (value == null) throw new UnsupportedOperationException("preciseShardingValue is null");

        final String yearJoinMonthStr = DateUtil.getYearJoinMonthByMillisecond(value.getTime());
        for (String availableTargetName : availableTargetNames) {
            if (availableTargetName.endsWith(yearJoinMonthStr)) {
                return availableTargetName;
            }
        }
        throw new UnsupportedOperationException();
    }

}
