package com.inspur.spg.common.sharding.jdbc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/28 18:21
 **/
@Data
public class ShardingDataSourceConfig {

    //jdbc-url
    private String url;

    //连接驱动名
    private String driverClassName;

    //账号
    private String username;

    //密码
    private String password;

}
