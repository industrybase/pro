package com.inspur.spg.common.sharding.jdbc.algorithm;

import com.inspur.spg.common.core.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.Date;


public class PreciseDatabaseShardingAlgorithm implements PreciseShardingAlgorithm<Date> {


    /**
     * 精确分片算法
     *
     * @param availableTargetNames 所有配置的库列表
     * @param shardingValue        分片值，也就是save_time_com的值
     * @return 所匹配库的结果
     */
    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Date> shardingValue) {
        Date value = shardingValue.getValue();
        // 库后缀
        String yearStr = DateUtil.getYearByMillisecond(value.getTime());

        if (value == null) throw new UnsupportedOperationException("preciseShardingValue is null");

        for (String availableTargetName : availableTargetNames) {
            if (availableTargetName.endsWith(yearStr)) {
                return availableTargetName;
            }
        }
        throw new UnsupportedOperationException();
    }

}