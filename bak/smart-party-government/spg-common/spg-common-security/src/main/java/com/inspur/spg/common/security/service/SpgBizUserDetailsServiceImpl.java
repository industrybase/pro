package com.inspur.spg.common.security.service;


import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.inspur.spg.admin.api.dto.BizUserInfo;
import com.inspur.spg.admin.api.dto.UserInfo;
import com.inspur.spg.admin.api.entity.BizUser;
import com.inspur.spg.admin.api.entity.SysUser;
import com.inspur.spg.admin.api.feign.RemoteBizUserService;
import com.inspur.spg.common.core.constant.CacheConstants;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.util.R;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/28 20:38
 **/
@Slf4j
@RequiredArgsConstructor
public class SpgBizUserDetailsServiceImpl implements SpgUserDetailsService {

    private final RemoteBizUserService remoteBizUserService;

    private final CacheManager cacheManager;


    /**
     * 用户名密码登录
     *
     * @param username 用户名
     * @return
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Cache cache = cacheManager.getCache(CacheConstants.BIZ_USER_DETAILS);
        if (cache != null && cache.get(username) != null) {
            return (SpgUser) cache.get(username).get();
        }
        R<BizUserInfo> result = remoteBizUserService.info(username, SecurityConstants.FROM_IN);
        // 根据 result 构建security 框架需要的 用户对象
        BizUserInfo info =  result.getData();

        Set<String> dbAuthsSet = new HashSet<>();

        if (ArrayUtil.isNotEmpty(info.getRoles())) {
            // 获取角色
            Arrays.stream(info.getRoles()).forEach(role -> dbAuthsSet.add(SecurityConstants.ROLE_BIZ + role));
            // 获取资源
            dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));

        }

        Collection<? extends GrantedAuthority> authorities = AuthorityUtils
                .createAuthorityList(dbAuthsSet.toArray(new String[0]));
        BizUser user = info.getBizUser();

        // 构造security用户
        SpgUser spgUser = new SpgUser(user.getUserId(), user.getDeptId(), user.getUsername(),
                SecurityConstants.BCRYPT + user.getPassword(), user.getPhone(), true, true, true,
                StrUtil.equals(user.getLockFlag(), CommonConstants.STATUS_NORMAL), authorities);

        if (cache != null) {
            cache.put(username, spgUser);
        }

        return spgUser;
    }

    /**
     * 是否支持此客户端校验
     *
     * @param clientId  目标客户端
     * @param grantType
     * @return true/false
     */
    @Override
    public boolean support(String clientId, String grantType) {
        return "custom".equals(clientId);
    }

    @Override
    public int getOrder() {
        return Integer.MIN_VALUE + 1;
    }

}
