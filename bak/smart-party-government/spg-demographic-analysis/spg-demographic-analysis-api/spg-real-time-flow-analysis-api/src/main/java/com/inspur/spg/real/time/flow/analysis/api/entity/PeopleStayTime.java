package com.inspur.spg.real.time.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.io.Serializable;

/**
 * 实时人流驻留时长分析(PeopleStayTime)实体类
 *
 * @author cj
 * @since 2022-01-10 16:33:01
 */
@Data
@TableName("people_stay_time")
@EqualsAndHashCode(callSuper = true)
public class PeopleStayTime extends Model<PeopleStayTime> {
    
    private Long id;
    /**
    * 地区编号
    */
    private String positionId;
    /**
    * 地区名称
    */
    private String positionName;
    /**
     * 活动id
     */
    private Integer activityId;
    /**
    * 人数
    */
    private Integer num;
    /**
    * 驻留时长
    */
    private Integer stayTime;
    /**
    * 统计时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date statTime;
    /**
    * 统计日期
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date statDate;
    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}