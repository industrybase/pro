package com.inspur.spg.real.time.flow.analysis.api.vo;

import lombok.Data;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/9 19:42
 **/
@Data
public class PeopleRealTimeNumVO {

    //场站名称
    private String positionName;
    //场站id
    private String positionId;
    //状态
    private String status;
    //场站类别
    private String positionType;
    //地区
    private String city;
    //人数
    private Integer num;

}
