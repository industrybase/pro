package com.inspur.spg.real.time.flow.analysis.api.feign.factory;

import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeFlowOutService;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemotePeopleRealTimeFlowOutFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/29 12:55
 **/
@Component
public class RemotePeopleRealTimeFlowOutFallbackFactory implements FallbackFactory<RemotePeopleRealTimeFlowOutService> {
    @Override
    public RemotePeopleRealTimeFlowOutService create(Throwable cause) {
        RemotePeopleRealTimeFlowOutFallbackImpl fallback = new RemotePeopleRealTimeFlowOutFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }
}
