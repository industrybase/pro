package com.inspur.spg.real.time.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;


/**
 * 实时人口流出(PeopleRealFlowOut)表实体类
 *
 * @author CJ
 * @since 2022-01-28 18:12:33
 */
@Data
@TableName("people_real_time_flow_out")
@EqualsAndHashCode(callSuper = true)
public class PeopleRealTimeFlowOut extends Model<PeopleRealTimeFlowOut>{
    
    private Long id;
    //活动id
    private Integer activityId;
    //地区编号
    private String positionId;
    //地区名称
    private String positionName;
    //人数
    private Integer num;
    //统计时间
    private Date statTime;
    //统计日期
    private Date statDate;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }