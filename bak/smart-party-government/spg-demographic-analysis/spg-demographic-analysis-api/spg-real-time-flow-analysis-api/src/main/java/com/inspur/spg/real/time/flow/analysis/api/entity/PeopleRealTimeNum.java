package com.inspur.spg.real.time.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.io.Serializable;

/**
 * 实时人数(PeopleRealTimeNum)实体类
 *
 * @author cj
 * @since 2022-01-10 16:32:50
 */
@Data
@TableName("people_real_time_num")
@EqualsAndHashCode(callSuper = true)
public class PeopleRealTimeNum extends Model<PeopleRealTimeNum>  {
    
    private Long id;
    /**
     * 活动id
     */
    private Integer activityId;
    /**
    * 地区编号
    */
    private String positionId;
    /**
    * 地区名称
    */
    private String positionName;
    /**
    * 人数
    */
    private Integer num;
    /**
    * 统计时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date statTime;
    /**
    * 统计日期
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date statDate;
    /**
    * 创建时间
    */
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}