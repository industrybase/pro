package com.inspur.spg.real.time.flow.analysis.api.feign;

import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.constant.ServiceNameConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.feign.factory.RemotePeopleStayTimeFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;

@FeignClient(contextId = "remotePeopleStayTimeService", value = ServiceNameConstants.REAL_FLOW_ANALYSIS_SERVICE, fallbackFactory = RemotePeopleStayTimeFallbackFactory.class)
public interface RemotePeopleStayTimeService {

    @PostMapping("/peopleStayTime/saveBatch")
    R saveBatch(@RequestBody List<Map<String,Object>> list, @RequestHeader(SecurityConstants.FROM) String from);

}
