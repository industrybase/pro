package com.inspur.spg.real.time.flow.analysis.api.feign.factory;

import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeJobConfigService;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemotePeopleRealTimeJobConfigFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/13 18:01
 **/
@Component
public class RemotePeopleRealTimeJobConfigFallbackFactory implements FallbackFactory<RemotePeopleRealTimeJobConfigService> {
    @Override
    public RemotePeopleRealTimeJobConfigService create(Throwable cause) {
        RemotePeopleRealTimeJobConfigFallbackImpl fallback = new RemotePeopleRealTimeJobConfigFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }
}
