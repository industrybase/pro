package com.inspur.spg.real.time.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;


/**
 * 实时位置活动统计订阅任务(PeopleRealJobConfig)表实体类
 *
 * @author CJ
 * @since 2022-01-13 17:47:08
 */
@Data
@TableName("people_real_job_config")
@EqualsAndHashCode(callSuper = true)
public class PeopleRealTimeJobConfig extends Model<PeopleRealTimeJobConfig>{
    
    private Integer id;
    //活动任务编码
    private String transCode;
    //任务编号
    private String jobCode;
    //任务统计类型
    private Integer actionType;
    //所属地市
    private String cityCode;
    //是否过滤人群(1.过滤;0.不过滤)
    private Integer fcst;
    //客户群
    private String customCode;
    //是否过滤驻留(1.过滤;0.不过滤）
    private Integer fstay;
    //驻留时长周期(4;8;20)
    private Integer timePeriod;
    //实时驻留时长,分钟
    private Integer timeStay;
    //状态(1.新增或更新;-1.删除)
    private Integer status;
    //任务统计时间粒度
    private Integer timeParticleSize;
    //创建时间
    private Date createTime;
    //更新时间
    private Date updateTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }