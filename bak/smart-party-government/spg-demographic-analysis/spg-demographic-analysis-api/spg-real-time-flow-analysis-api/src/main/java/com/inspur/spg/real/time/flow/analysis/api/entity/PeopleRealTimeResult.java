package com.inspur.spg.real.time.flow.analysis.api.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * 实时人数结果表(PeopleRealTimeResult)表实体类
 *
 * @author CJ
 * @since 2022-02-13 10:22:40
 */
@Data
@TableName("people_real_time_result")
@EqualsAndHashCode(callSuper = true)
public class PeopleRealTimeResult extends Model<PeopleRealTimeResult> {

    private Long id;
    //表名
    private String tableName;
    //枢纽id
    private String positionId;
    //枢纽名称
    private String positionName;
    //告警表配置id
    private Integer activityId;
    //人数
    private Integer totalNum;
    //统计日期
    private Date statDate;
    //创建时间
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}