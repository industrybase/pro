package com.inspur.spg.real.time.flow.analysis.api.feign.factory;

import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeNumService;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeResultService;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemotePeopleRealTimeNumFallbackImpl;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemotePeopleRealTimeResultFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/13 12:51
 **/
@Component
public class RemotePeopleRealTimeResultFallbackFactory implements FallbackFactory<RemotePeopleRealTimeResultService> {

    @Override
    public RemotePeopleRealTimeResultService create(Throwable cause) {
        RemotePeopleRealTimeResultFallbackImpl fallback = new RemotePeopleRealTimeResultFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }

}
