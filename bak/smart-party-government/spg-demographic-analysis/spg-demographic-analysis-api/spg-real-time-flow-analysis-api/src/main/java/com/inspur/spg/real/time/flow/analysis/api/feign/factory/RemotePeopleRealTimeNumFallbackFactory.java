package com.inspur.spg.real.time.flow.analysis.api.feign.factory;

import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeNumService;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemotePeopleRealTimeNumFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/13 15:57
 **/
@Component
public class RemotePeopleRealTimeNumFallbackFactory implements FallbackFactory<RemotePeopleRealTimeNumService> {
    @Override
    public RemotePeopleRealTimeNumService create(Throwable cause) {
        RemotePeopleRealTimeNumFallbackImpl fallback = new RemotePeopleRealTimeNumFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }
}
