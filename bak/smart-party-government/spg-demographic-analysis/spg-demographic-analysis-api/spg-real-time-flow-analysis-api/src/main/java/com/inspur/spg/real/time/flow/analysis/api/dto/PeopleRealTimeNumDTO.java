package com.inspur.spg.real.time.flow.analysis.api.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/11 17:47
 **/
@Data
public class PeopleRealTimeNumDTO {

    /**
     * 地区编号
     */
    private String positionId;
    /**
     * 地区名称
     */
    private String positionName;

    /**
     * 统计开始日期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startStatDate;

    /**
     * 统计结束日期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endStatDate;

    /**
     * 统计开始时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date startStatTime;

    /**
     * 统计结束时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endStatTime;

}
