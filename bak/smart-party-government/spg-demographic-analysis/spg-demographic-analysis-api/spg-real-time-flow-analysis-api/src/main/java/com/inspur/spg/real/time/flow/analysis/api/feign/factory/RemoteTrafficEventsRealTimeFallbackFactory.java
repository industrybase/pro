package com.inspur.spg.real.time.flow.analysis.api.feign.factory;

import com.inspur.spg.real.time.flow.analysis.api.feign.RemoteTrafficEventsRealTimeService;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemoteTrafficEventsRealTimeFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/9 11:28
 **/
@Component
public class RemoteTrafficEventsRealTimeFallbackFactory implements FallbackFactory<RemoteTrafficEventsRealTimeService> {
    @Override
    public RemoteTrafficEventsRealTimeService create(Throwable cause) {
        RemoteTrafficEventsRealTimeFallbackImpl fallback = new RemoteTrafficEventsRealTimeFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }
}
