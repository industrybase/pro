package com.inspur.spg.real.time.flow.analysis.api.feign.factory;

import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleStayTimeService;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemotePeopleStayTimeFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/13 15:58
 **/
@Component
public class RemotePeopleStayTimeFallbackFactory implements FallbackFactory<RemotePeopleStayTimeService> {
    @Override
    public RemotePeopleStayTimeService create(Throwable cause) {
        RemotePeopleStayTimeFallbackImpl fallback = new RemotePeopleStayTimeFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }
}
