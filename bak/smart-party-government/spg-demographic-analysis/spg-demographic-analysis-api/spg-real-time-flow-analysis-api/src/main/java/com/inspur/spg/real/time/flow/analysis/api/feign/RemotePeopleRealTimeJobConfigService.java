package com.inspur.spg.real.time.flow.analysis.api.feign;

import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.constant.ServiceNameConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeJobConfig;
import com.inspur.spg.real.time.flow.analysis.api.feign.factory.RemotePeopleRealTimeJobConfigFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/13 17:56
 **/
@FeignClient(contextId = "remotePeopleRealTimeJobConfigService", value = ServiceNameConstants.REAL_FLOW_ANALYSIS_SERVICE, fallbackFactory = RemotePeopleRealTimeJobConfigFallbackFactory.class)
public interface RemotePeopleRealTimeJobConfigService {

    @GetMapping("/peopleRealTimeJobConfig/getAll")
    R<List<PeopleRealTimeJobConfig>> getAll(@RequestHeader(SecurityConstants.FROM) String from);

}
