package com.inspur.spg.real.time.flow.analysis.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/*
 * @Author CJ
 * @Description 交通实时事件VO
 * @Date 2022/2/15 10:28
 **/
@Data
@ApiModel(description = "交通实时事件实体VO")
public class TrafficEventsRealTimeVO {

    //事件标题
    @ApiModelProperty(value = "事件标题")
    private String brief;
    //事件预计结束时间
    @ApiModelProperty(value = "事件预计结束时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    //事件描述
    @ApiModelProperty(value = "事件描述")
    private String eventDesc;
    //线路坐标
    @ApiModelProperty(value = "线路坐标")
    private String coordinates;
    //事件图⽚链接
    @ApiModelProperty(value = "事件图⽚链接")
    private String picture;
    //道路名称
    @ApiModelProperty(value = "道路名称")
    private String roadName;
    //事件开始时间
    @ApiModelProperty(value = "事件开始时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    //经度坐标
    @ApiModelProperty(value = "经度坐标")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal x;
    //纬度坐标
    @ApiModelProperty(value = "纬度坐标")
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal y;


}
