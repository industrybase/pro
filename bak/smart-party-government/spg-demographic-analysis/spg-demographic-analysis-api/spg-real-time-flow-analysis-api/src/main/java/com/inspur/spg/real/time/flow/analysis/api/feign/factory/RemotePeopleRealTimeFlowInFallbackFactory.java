package com.inspur.spg.real.time.flow.analysis.api.feign.factory;

import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeFlowInService;
import com.inspur.spg.real.time.flow.analysis.api.feign.fallback.RemotePeopleRealTimeFlowInFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/29 12:55
 **/
@Component
public class RemotePeopleRealTimeFlowInFallbackFactory implements FallbackFactory<RemotePeopleRealTimeFlowInService> {

    @Override
    public RemotePeopleRealTimeFlowInService create(Throwable cause) {
        RemotePeopleRealTimeFlowInFallbackImpl fallback = new RemotePeopleRealTimeFlowInFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }

}
