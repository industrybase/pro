package com.inspur.spg.real.time.flow.analysis.api.feign.fallback;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeJobConfig;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeJobConfigService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/13 18:01
 **/
@Component
@Slf4j
public class RemotePeopleRealTimeJobConfigFallbackImpl implements RemotePeopleRealTimeJobConfigService {

    @Setter
    private Throwable cause;

    @Override
    public R<List<PeopleRealTimeJobConfig>> getAll(String from) {
        log.error("feign PeopleRealJobConfig getAll 查询失败", cause);
        return R.failed("feign PeopleRealJobConfig getAll 查询失败，" + cause.getMessage());
    }

}
