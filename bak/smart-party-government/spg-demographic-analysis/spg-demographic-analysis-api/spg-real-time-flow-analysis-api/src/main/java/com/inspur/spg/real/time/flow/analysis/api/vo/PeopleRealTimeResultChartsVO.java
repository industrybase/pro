package com.inspur.spg.real.time.flow.analysis.api.vo;

import com.inspur.spg.common.core.entity.ChartsInfo;
import lombok.Data;

import java.math.BigDecimal;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/21 10:17
 **/
@Data
public class PeopleRealTimeResultChartsVO extends ChartsInfo<BigDecimal> {

    //表名
    private String tableName;

}
