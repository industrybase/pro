package com.inspur.spg.real.time.flow.analysis.api.feign;

import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.constant.ServiceNameConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.feign.factory.RemotePeopleRealTimeNumFallbackFactory;
import com.inspur.spg.real.time.flow.analysis.api.feign.factory.RemotePeopleRealTimeResultFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Map;

@FeignClient(contextId = "remotePeopleRealTimeResultService", value = ServiceNameConstants.REAL_FLOW_ANALYSIS_SERVICE, fallbackFactory = RemotePeopleRealTimeResultFallbackFactory.class)
public interface RemotePeopleRealTimeResultService {

    @PostMapping("/peopleRealTimeResult/executeSave")
    R executeSave(@RequestBody Map<String,Object> paramsMap, @RequestHeader(SecurityConstants.FROM) String from);

}
