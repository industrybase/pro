package com.inspur.spg.real.time.flow.analysis.api.feign.fallback;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleStayTimeService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/13 15:57
 **/
@Component
@Slf4j
public class RemotePeopleStayTimeFallbackImpl implements RemotePeopleStayTimeService {

    @Setter
    private Throwable cause;

    @Override
    public R saveBatch(List<Map<String, Object>> list,String from) {
        log.error("feign PeopleStayTime saveBatch 保存失败", cause);
        return R.failed("feign PeopleStayTime saveBatch 保存失败，" + cause.getMessage());
    }
}
