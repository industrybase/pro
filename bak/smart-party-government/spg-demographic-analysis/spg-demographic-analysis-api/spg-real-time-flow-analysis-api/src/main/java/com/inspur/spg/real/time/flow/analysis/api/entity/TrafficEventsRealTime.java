package com.inspur.spg.real.time.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 实时交通事件(TrafficEventsRealTime)表实体类
 *
 * @author CJ
 * @since 2022-02-09 11:17:55
 */
@Data
@TableName("traffic_events_real_time")
@EqualsAndHashCode(callSuper = true)
public class TrafficEventsRealTime extends Model<TrafficEventsRealTime>{
    
    private Long id;
    //事件标题
    private String brief;
    //事件预计结束时间
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    //事件描述
    private String eventDesc;
    //事件ID
    private String eventId;
    //事件类型码
    private String eventType;
    //是否⾼速(1.是;0.否)
    private Integer expressway;
    //线路坐标
    private String coordinates;
    //发布⽅名称
    private String nickName;
    //是否权威发布(0.官方;1.权威;2.UGC)
    private Integer offcial;
    //事件图⽚链接
    private String picture;
    //道路名称
    private String roadName;
    //数据源编号
    private String source;
    //事件开始时间
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    //事件最后更新时间
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    //经度坐标
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal x;
    //纬度坐标
    @JsonSerialize(using= ToStringSerializer.class)
    private BigDecimal y;
    //创建时间
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //统计时间
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date statDate;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }