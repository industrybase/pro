package com.inspur.spg.real.time.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 实时位置活动配置(PeopleRealActivityConfig)表实体类
 *
 * @author CJ
 * @since 2022-01-29 11:46:03
 */
@Data
@TableName("people_real_time_activity_config")
@EqualsAndHashCode(callSuper = true)
public class PeopleRealTimeActivityConfig extends Model<PeopleRealTimeActivityConfig>{
    
    private Integer id;
    //活动唯一任务编码
    private String transCode;
    //活动任务名称
    private String taskName;
    //活动任务名称
    private String taskDesc;
    //所属地市
    private String cityCode;
    //所属地市
    private String cityName;
    //地区类型编号
    private Integer areaTypeId;
    //地区类型名称
    private String areaTypeName;
    //区域编码
    private String areaNo;
    //区域名称
    private String areaName;
    //告警阈值
    private Integer warningLow;
    //实时系数
    private BigDecimal rtc;
    //状态
    private Integer status;
    //cgi列表
    private String cgiList;
    //创建时间
    private Date createTime;
    //修改时间
    private Date updateTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }