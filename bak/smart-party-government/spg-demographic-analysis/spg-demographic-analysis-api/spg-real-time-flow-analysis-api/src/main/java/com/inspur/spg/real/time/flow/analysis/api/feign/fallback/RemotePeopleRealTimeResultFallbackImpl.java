package com.inspur.spg.real.time.flow.analysis.api.feign.fallback;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeResultService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/13 12:51
 **/
@Component
@Slf4j
public class RemotePeopleRealTimeResultFallbackImpl implements RemotePeopleRealTimeResultService {

    @Setter
    private Throwable cause;

    @Override
    public R executeSave(Map<String, Object> paramsMap,String from) {
        log.error("feign PeopleRealTimeNum executeSave 保存失败", cause);
        return R.failed("feign PeopleRealTimeNum executeSave 保存失败，" + cause.getMessage());
    }

}
