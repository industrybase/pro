package com.inspur.spg.flow.analysis.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/*
 *
 * Author CJ
 * @Date 2022/3/4 11:17
 * @Description 历史迁徙
 **/
@Data
@ApiModel(description = "历史迁徙DTO")
public class MigrantHistoricalTrendDTO extends BaseMigrantDTO implements Serializable {

    private static final long serialVersionUID = -3709046675596801147L;

    /**
     * 地市
     */
    @ApiModelProperty(value = "地市")
    private String area;

    @Override
    public String getArea() {
        if(StringUtils.isBlank(this.area) ||  "广东".equals(this.area)){
            this.area = "全省";
        }
        return this.area;
    }

}
