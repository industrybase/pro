package com.inspur.spg.flow.analysis.api.feign.factory;


import com.inspur.spg.flow.analysis.api.feign.RemoteApiFieldFormatConfigService;
import com.inspur.spg.flow.analysis.api.feign.fallback.RemoteApiFieldFormatConfigFallbackImpl;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/4 14:57
 **/
@Component
public class RemoteApiFieldFormatConfigFallbackFactory implements FallbackFactory<RemoteApiFieldFormatConfigService> {
    @Override
    public RemoteApiFieldFormatConfigService create(Throwable cause) {
        RemoteApiFieldFormatConfigFallbackImpl fallback = new RemoteApiFieldFormatConfigFallbackImpl();
        fallback.setCause(cause);
        return fallback;
    }
}
