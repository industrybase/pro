package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口迁徙-省际迁出去向及渠道(MigrantProvinceChannel)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:06
 */
@Data
@TableName("migrant_out_province_channel")
@EqualsAndHashCode(callSuper = true)
public class MigrantOutProvinceChannel extends Model<MigrantOutProvinceChannel>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //去向渠道
    private String outChannel;
    //去向地区
    private String outArea;
    //人数
    private Integer outNum;
    //占比
    private BigDecimal outProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }