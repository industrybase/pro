package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 港珠澳大桥客流洞察-客流方向(GzaBridgeDirectFlow)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:37:08
 */
@Data
@TableName("gza_bridge_direct_flow")
@EqualsAndHashCode(callSuper = true)
public class GzaBridgeDirectFlow extends BasePosition<GzaBridgeDirectFlow> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //去往香港旅客量
    private Integer toHkTra;
    //去往珠海澳门旅客量
    private Integer toMzTra;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }