package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 枢纽阀值列表(positionConfig)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:47:58
 */
@Data
@TableName("position_config")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "枢纽配置实体")
public class PositionConfig extends Model<PositionConfig> {

    //主键
    @ApiModelProperty(value = "主键id")
    private Integer id;
    //类别id
    @ApiModelProperty(value = "类别id")
    private Integer typeId;
    //类别名称
    @ApiModelProperty(value = "类别名称")
    private String typeName;
    //名称
    @ApiModelProperty(value = "名称")
    private String name;
    //名称简称
    @ApiModelProperty(value = "名称简称")
    private String curtName;
    //编码
    @ApiModelProperty(value = "编码")
    private String code;
    //地市名称
    @ApiModelProperty(value = "地市名称")
    private String cityName;
    //地市编号
    @ApiModelProperty(value = "地市编号")
    private String cityCode;
    //区县
    @ApiModelProperty(value = "区县")
    private String district;
    //低阀值
    @ApiModelProperty(value = "低阀值")
    private Integer warningLow;
    //是否启用:0否，1是
    @ApiModelProperty(value = "是否启用",notes = "0.否;1.是")
    private Integer isEnable;
    //横坐标
    @ApiModelProperty(value = "横坐标")
    private BigDecimal coordinateX;
    //纵坐标
    @ApiModelProperty(value = "纵坐标")
    private BigDecimal coordinateY;
    //坐标数组
    @ApiModelProperty(value = "坐标数组")
    private String coordinateXy;
    //坐标集合
    @ApiModelProperty(value = "坐标集合")
    private String coordinateXys;
    //实时系数
    @ApiModelProperty(value = "实时系数")
    private BigDecimal rtc;
    //天系数
    @ApiModelProperty(value = "天系数")
    private BigDecimal dc;
    //缩放级别
    @ApiModelProperty(value = "缩放级别")
    private Integer zoom;
    //楼层信息（机场、车站）
    @ApiModelProperty(value = "楼层信息（机场、车站）")
    private String floor;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}