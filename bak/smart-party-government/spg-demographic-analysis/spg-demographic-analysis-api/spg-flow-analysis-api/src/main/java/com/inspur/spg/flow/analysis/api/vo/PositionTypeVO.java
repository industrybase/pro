package com.inspur.spg.flow.analysis.api.vo;

import com.inspur.spg.flow.analysis.api.entity.PositionConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author CJ
 * @Date 2022/2/22 9:50 （可以根据需要修改）
 * @Description:
 */

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/22 9:50
 **/
@Data
@ApiModel(description = "枢纽类别VO")
public class PositionTypeVO {

    //主键id
    @ApiModelProperty(value = "主键id")
    private Integer id;
    //名称
    @ApiModelProperty(value = "类别名称")
    private String name;
    //枢纽配置数据
    @ApiModelProperty("枢纽配置数据")
    private List<PositionConfig> positionConfigs;

}
