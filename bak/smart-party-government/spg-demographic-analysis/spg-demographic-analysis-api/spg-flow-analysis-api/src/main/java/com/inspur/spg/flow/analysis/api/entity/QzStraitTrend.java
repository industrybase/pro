package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 琼州海峡客流趋势(QzStraitTrend)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("qz_strait_trend")
@EqualsAndHashCode(callSuper = true)
public class QzStraitTrend extends BasePosition<QzStraitTrend>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //到港人数
    private Integer inPeople;
    //离港人数
    private Integer outPeople;
    //总人数
    private Integer allPeople;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }