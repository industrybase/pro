package com.inspur.spg.flow.analysis.api.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.jackson.BigDecimal2Serializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/15 12:57
 **/
@Data
@ApiModel(description = "人口数统占比计实体VO")
@Builder
public class PopulationProportionStatsVO {

    //省份占比
    @ApiModelProperty(value = "省份占比")
    @JsonSerialize(using= BigDecimal2Serializer.class)
    private BigDecimal provinceProportion;

    //省份占比
    @ApiModelProperty(value = "国外占比")
    @JsonSerialize(using= BigDecimal2Serializer.class)
    private BigDecimal foreignProportion;

    @ApiModelProperty(value = "国外占比")
    private List<ChartsInfo<BigDecimal>> chartsInfos;

}
