package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 服务区客流画像驻留时长(RestAreaStayTime)表实体类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Data
@TableName("rest_area_stay_time")
@EqualsAndHashCode(callSuper = true)
public class RestAreaStayTime extends BasePosition<RestAreaStayTime>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //驻留时长分组:0表示0-1小时，1表示1-2小时等等，8表示8小时以上
    private Integer timeGroup;
    //驻留时长人数
    private Integer timeValue;
    //占比
    private BigDecimal timeProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }