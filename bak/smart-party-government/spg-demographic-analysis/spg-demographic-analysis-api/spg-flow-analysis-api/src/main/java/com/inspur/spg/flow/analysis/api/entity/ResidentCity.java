package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口洞察-地市常驻人口(ResidentCity)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("resident_city")
@EqualsAndHashCode(callSuper = true)
public class ResidentCity extends Model<ResidentCity>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //市名
    private String city;
    //人口数量
    private Integer num;
    //性别
    private String gender;
    //占比
    private BigDecimal proportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }