package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * 港珠澳大桥客流趋势-每日平均通关时长(GzaBridgeAvgDay)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:34:56
 */
@Data
@TableName("gza_bridge_avg_day")
@EqualsAndHashCode(callSuper = true)
public class GzaBridgeAvgDay extends BasePosition<GzaBridgeAvgDay> {

    private Long id;
    //区域类型
    private String positionType;
    //统计日期
    private Date statDate;
    //每日平均通关时长 (分钟)
    private Integer avePassTime;
    //创建时间
    private Date createTime;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}