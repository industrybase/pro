package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 港珠澳大桥客流洞察-归属地市分布(GzaBridgeCity)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:37:08
 */
@Data
@TableName("gza_bridge_city")
@EqualsAndHashCode(callSuper = true)
public class GzaBridgeCity extends BasePosition<GzaBridgeCity> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //人员类别
    private String toType;
    //归属地市
    private String city;
    //人数
    private Integer stayNum;
    //占比
    private BigDecimal proportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }