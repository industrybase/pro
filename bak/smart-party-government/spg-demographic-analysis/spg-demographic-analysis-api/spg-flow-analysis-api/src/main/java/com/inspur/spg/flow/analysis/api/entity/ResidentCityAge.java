package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口洞察-地市常驻人口年龄(ResidentCityAge)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("resident_city_age")
@EqualsAndHashCode(callSuper = true)
public class ResidentCityAge extends Model<ResidentCityAge>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //城市
    private String city;
    //年龄段
    private Integer ageGroup;
    //数量
    private Integer passNum;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }