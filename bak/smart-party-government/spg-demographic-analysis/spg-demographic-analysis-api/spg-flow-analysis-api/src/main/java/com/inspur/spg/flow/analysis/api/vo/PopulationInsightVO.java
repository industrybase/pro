package com.inspur.spg.flow.analysis.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/*
 * @Author CJ
 * @Description 人口洞察实体VO
 * @Date 2022/2/15 13:04
 **/
@Data
@ApiModel(description = "人口洞察实体VO")
@Builder
public class PopulationInsightVO {

    //去向
    @ApiModelProperty(value = "去向")
    private PopulationProportionStatsVO leave;
    //来源
    @ApiModelProperty(value = "来源")
    private PopulationProportionStatsVO origin;

}
