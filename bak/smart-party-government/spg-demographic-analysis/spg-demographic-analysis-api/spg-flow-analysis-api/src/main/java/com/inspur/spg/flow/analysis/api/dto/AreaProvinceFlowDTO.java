package com.inspur.spg.flow.analysis.api.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class AreaProvinceFlowDTO implements Serializable {

    private static final long serialVersionUID = 4577633324973585191L;

    @ApiModelProperty(value = "地区名称")
    private String area;

    @ApiModelProperty(value = "查询日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "查询日期不能为空")
    private Date statDate;

    public String getArea() {
        if(StringUtils.isEmpty(this.area)){
            this.area = "广东省";
        }else{
            this.area = this.area.replace("广东","广东省");
        }
        return area;
    }
}
