package com.inspur.spg.flow.analysis.api.feign.fallback;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.feign.RemoteApiFieldFormatConfigService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/4 14:58
 **/
@Component
@Slf4j
public class RemoteApiFieldFormatConfigFallbackImpl implements RemoteApiFieldFormatConfigService {

    @Setter
    private Throwable cause;

    @Override
    public R saveByApiResult(Map<String,Object> dataMap,String from) {
        log.error("feign API接口结果保存 saveByApiResult 保存失败", cause);
        return R.failed("feign API接口结果保存 saveByApiResult 保存失败，" + cause.getMessage());
    }


}
