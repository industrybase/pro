package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 场站旅客洞察-省外去向地(StationProvinceLeave)表实体类
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
@Data
@TableName("station_province_leave")
@EqualsAndHashCode(callSuper = true)
public class StationProvinceLeave extends BasePosition<StationProvinceLeave> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //去向省份名称
    private String toAreaName;
    //旅客人数
    private Integer travelerNum;
    //旅客占比
    private BigDecimal travelerProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }