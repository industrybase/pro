package com.inspur.spg.flow.analysis.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 省内地市迁入迁出排名
 */
@Data
@ApiModel(description = "省内地市迁徙排名Vo")
@Builder
public class MigrantCityRankVO {

    //迁入
    @ApiModelProperty(value = "迁入")
    private List<MigrantRankVO> originList;

    //迁出
    @ApiModelProperty(value = "迁出")
    private  List<MigrantRankVO> leaveList;

}
