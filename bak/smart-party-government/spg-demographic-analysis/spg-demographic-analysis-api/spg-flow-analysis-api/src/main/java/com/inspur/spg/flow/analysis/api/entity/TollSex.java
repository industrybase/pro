package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 收费站热力客流画像-性别(TollSex)表实体类
 *
 * @author CJ
 * @since 2022-01-06 10:52:03
 */
@Data
@TableName("toll_sex")
@EqualsAndHashCode(callSuper = true)
public class TollSex extends BasePosition<TollSex>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //性别: 1男； 2女 ；3未知
    private String gender;
    //人数
    private Integer num;
    //占比
    private BigDecimal genderProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }