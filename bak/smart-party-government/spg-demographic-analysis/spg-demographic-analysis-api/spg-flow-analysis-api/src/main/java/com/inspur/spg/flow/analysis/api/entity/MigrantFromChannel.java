package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 迁徙来源省份(MigrantFromChannel)表实体类
 *
 * @author CJ
 * @since 2022-02-10 11:27:35
 */
@Data
@TableName("migrant_from_channel")
@EqualsAndHashCode(callSuper = true)
public class MigrantFromChannel extends Model<MigrantFromChannel>{
    
    private Long id;
    //统计时间
    private Date statDate;
    //人数
    private Integer num;
    //渠道类别:1.公路;2.民航;3.水路;4.铁路;
    private String channelType;
    //来源类别:1.境外;2.省外
    private String scopeType;
    //来源占比
    private BigDecimal proportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }