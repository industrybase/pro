package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 场站旅客洞察-旅客量(StationPassenger)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("station_passenger")
@EqualsAndHashCode(callSuper = true)
public class StationPassenger extends BasePosition<StationPassenger>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //当日总旅客量
    private Integer travelersNum;
    //当日到达旅客量
    private Integer arrivalNum;
    //当日出发旅客量
    private Integer leaveNum;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }