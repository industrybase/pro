package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口迁徙-类别(MigrantType)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:06
 */
@Data
@TableName("migrant_type")
@EqualsAndHashCode(callSuper = true)
public class MigrantType extends Model<MigrantType>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //类别
    private String migType;
    //人次
    private Integer num;
    //占比
    private BigDecimal proportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }