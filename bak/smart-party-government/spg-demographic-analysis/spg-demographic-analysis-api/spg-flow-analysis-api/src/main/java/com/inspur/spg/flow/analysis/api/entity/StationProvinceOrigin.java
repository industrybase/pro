package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 场站旅客洞察-省内来源地(StationProvinceOrigin)表实体类
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
@Data
@TableName("station_province_origin")
@EqualsAndHashCode(callSuper = true)
public class StationProvinceOrigin extends BasePosition<StationProvinceOrigin> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //省外来源城市名称
    private String originAreaName;
    //旅客人数
    private Integer travelerNum;
    //旅客占比
    private BigDecimal travelerProportion;
    //创建日期
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }