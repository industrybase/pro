package com.inspur.spg.flow.analysis.api.feign;

import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.constant.ServiceNameConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.feign.factory.RemoteApiFieldFormatConfigFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.Map;

@FeignClient(contextId = "remoteApiFieldFormatConfigService", value = ServiceNameConstants.FLOW_ANALYSIS_SERVICE, fallbackFactory = RemoteApiFieldFormatConfigFallbackFactory.class)
public interface RemoteApiFieldFormatConfigService {

    @PostMapping("/apiFieldFormatConfig/saveByApiResult")
    R saveByApiResult(Map<String,Object> dataMap, @RequestHeader(SecurityConstants.FROM) String from);

}
