package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 港珠澳大桥客流洞察-归属省份分布(GzaBridgeProvince)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:37:08
 */
@Data
@TableName("gza_bridge_province")
@EqualsAndHashCode(callSuper = true)
public class GzaBridgeProvince extends BasePosition<GzaBridgeProvince>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //人员类型
    private String toType;
    //归属省份
    private String province;
    //人数
    private Integer stayNum;
    //占比
    private BigDecimal proportion;
    //占比
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }