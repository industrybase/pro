package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 场站旅客洞察-国际去向(StationForeignLeave)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("station_foreign_leave")
@EqualsAndHashCode(callSuper = true)
public class StationForeignLeave extends BasePosition<StationForeignLeave>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //类型:1单个国家，2国际汇总
    private String leaveType;
    //去向国家名称
    private String toAreaName;
    //旅客人数
    private Integer travelerNum;
    //旅客占比
    private BigDecimal travelerProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }