package com.inspur.spg.flow.analysis.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.util.Date;

/*
 * @Author CJ
 * @Description 迁徙基类DTO
 * @Date 2022/1/11 18:00
 **/
@Data
@ApiModel(description = "迁徙基类DTO")
public class BaseMigrantDTO implements Serializable {

    private static final long serialVersionUID = -5843669672121748514L;
    /**
     * 地市
     */
    @ApiModelProperty(value = "地市")
    private String area;

    /**
     * 统计开始日期
     */
    @ApiModelProperty(value = "查询开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 统计结束日期
     */
    @ApiModelProperty(value = "查询结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 统计结束日期
     */
    @ApiModelProperty(value = "查询日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date statDate;

    /**
     * 当页大小
     */
    @ApiModelProperty(value = "当页大小")
    private Integer limit;

    //默认十
    public Integer getLimit() {
        if (this.limit == null) {
            this.limit = 10;
        }
        return this.limit;
    }

}
