package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 场站旅客洞察-国际来源地(StationForeignOrigin)表实体类
 *
 * @author cj
 * @since 2022-01-31 17:49:54
 */
@Data
@TableName("station_foreign_origin")
@EqualsAndHashCode(callSuper = true)
public class StationForeignOrigin extends BasePosition<StationForeignOrigin> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //来源国家名称
    private String originAreaName;
    //旅客人数
    private Integer travelerNum;
    //旅客占比
    private BigDecimal travelerProportion;
    //创建时间
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }


}