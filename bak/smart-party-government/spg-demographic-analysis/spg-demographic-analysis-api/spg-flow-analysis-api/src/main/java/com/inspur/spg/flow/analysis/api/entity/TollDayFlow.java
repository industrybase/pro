package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 收费站每日客流(TollDayFlow)表实体类
 *
 * @author CJ
 * @since 2022-01-06 10:52:03
 */
@Data
@TableName("toll_day_flow")
@EqualsAndHashCode(callSuper = true)
public class TollDayFlow extends BasePosition<TollDayFlow>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //总客流
    private Integer allPeople;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }