package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 服务区客流画像-年龄(RestAreaAge)表实体类
 *
 * @author CJ
 * @since 2022-01-31 09:36:36
 */
@Data
@TableName("rest_area_age")
@EqualsAndHashCode(callSuper = true)
public class RestAreaAge extends BasePosition<RestAreaAge>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //分组0为6-11岁，1为12-15岁，2为16-18岁，3为19-22岁，4为23-25岁，5为26-35岁，6为36-45岁，7为46-55岁，8为56-65岁，9为66岁以上
    private Integer ageGroup;
    //年龄段人数
    private Integer ageNum;
    //年龄段占比
    private BigDecimal ageProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }