package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口洞察-省常驻人口年龄(ResidentProvince)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("resident_province")
@EqualsAndHashCode(callSuper = true)
public class ResidentProvince extends Model<ResidentProvince>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //省名
    private String province;
    //性别
    private String gender;
    //常驻人口数量
    private Integer num;
    //占比
    private BigDecimal proportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }