package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 港珠澳大桥客流洞察-时点通关时长(GzaBridgeAvgTime)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:37:07
 */
@Data
@TableName("gza_bridge_avg_time")
@EqualsAndHashCode(callSuper = true)
public class GzaBridgeAvgTime extends BasePosition<GzaBridgeAvgTime> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //时点
    private Integer pointTime;
    //时点平均通关时长 单位（分钟）
    private Date pointTimePass;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }