package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * API接口字段转换配置(ApiFieldFormatConfig)表实体类
 *
 * @author CJ
 * @since 2022-01-06 16:15:10
 */
@Data
@TableName("api_field_format_config")
@EqualsAndHashCode(callSuper = true)
public class ApiFieldFormatConfig extends Model<ApiFieldFormatConfig>{
    
    private Integer id;
    //服务表id
    private Integer apiId;
    //接口字段
    private String apiFieldName;
    //接口字段类型
    private String apiFieldType;
    //接口字段转换字符串
    private String apiFieldFormat;
    //对应数据库表字段
    private String fieldName;
    //字段类型
    private String fieldType;
    //字段转换字符串
    private String fieldFormat;
    //描述
    private String fieldDescription;
    //创建时间
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }