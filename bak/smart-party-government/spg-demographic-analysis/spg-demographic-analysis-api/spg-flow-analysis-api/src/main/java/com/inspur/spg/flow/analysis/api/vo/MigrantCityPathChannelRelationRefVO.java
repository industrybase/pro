package com.inspur.spg.flow.analysis.api.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 省内迁徙关联VO
 */
@Data
public class MigrantCityPathChannelRelationRefVO {

    //迁徙线路
    private String route;
    //渠道类型
    private String channelType;
    //迁徙人口
    private BigDecimal migNum;

}
