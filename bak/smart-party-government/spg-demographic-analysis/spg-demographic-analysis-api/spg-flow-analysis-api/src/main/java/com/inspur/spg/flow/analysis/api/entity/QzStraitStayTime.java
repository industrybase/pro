package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 琼州海峡客流驻留时长(QzStraitStayTime)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("qz_strait_stay_time")
@EqualsAndHashCode(callSuper = true)
public class QzStraitStayTime extends BasePosition<QzStraitStayTime>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //驻留时长
    private Integer statyTime;
    //人数
    private Integer stayNum;
    //占比
    private BigDecimal stayProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }