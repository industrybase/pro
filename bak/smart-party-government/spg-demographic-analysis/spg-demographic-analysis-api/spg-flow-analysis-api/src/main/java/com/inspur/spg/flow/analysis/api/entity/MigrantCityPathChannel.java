package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口迁徙-省内路线及渠道迁徙(MigrantCityPathChannel)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:06
 */
@Data
@TableName("migrant_city_path_channel")
@EqualsAndHashCode(callSuper = true)
public class MigrantCityPathChannel extends Model<MigrantCityPathChannel>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //出发城市简称
    private String fromCityShort;
    //出发城市
    private String fromCity;
    //到达城市简称
    private String toCityShort;
    //到达城市
    private String toCity;
    //渠道
    private String migChannel;
    //迁徙人数
    private Integer migNum;
    //渠道占出发城市人数的占比
    private BigDecimal outProportion;
    //渠道占到达城市人数的占比
    private BigDecimal inProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }