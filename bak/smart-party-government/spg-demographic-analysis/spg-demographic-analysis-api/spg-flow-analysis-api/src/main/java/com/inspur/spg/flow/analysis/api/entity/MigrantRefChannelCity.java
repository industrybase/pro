package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 关联表省内(MigrantRefChannelCity)表实体类
 *
 * @author CJ
 * @since 2022-02-10 11:30:00
 */
@Data
@TableName("migrant_ref_channel_city")
@EqualsAndHashCode(callSuper = true)
public class MigrantRefChannelCity extends Model<MigrantRefChannelCity>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //出发城市
    private String fromCity;
    //到达城市
    private String toCity;
    //来源渠道
    private String channelType;
    //来源占比
    private BigDecimal proportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }