package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 琼州海峡客流归属地(QzStraitBelong)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:59
 */
@Data
@TableName("qz_strait_belong")
@EqualsAndHashCode(callSuper = true)
public class QzStraitBelong extends BasePosition<QzStraitBelong>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //归属类型 1、境外；2、省外；3、省内
    private Integer belong;
    //归属地区
    private String belongArea;
    //人数
    private Integer num;
    //占比
    private BigDecimal belongProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }