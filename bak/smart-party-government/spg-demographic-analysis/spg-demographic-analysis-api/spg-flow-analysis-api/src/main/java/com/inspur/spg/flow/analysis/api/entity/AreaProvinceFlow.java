package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;


/**
 * 省客流实时数据（5分钟）(AreaProvinceFlow)表实体类
 *
 * @author CJ
 * @since 2022-02-07 11:33:36
 */
@Data
@TableName("area_province_flow")
@EqualsAndHashCode(callSuper = true)
public class AreaProvinceFlow extends Model<AreaProvinceFlow>{
    
    private Long id;
    //统计时间
    private Date statTime;
    //统计日期
    private Date statDate;
    //城市
    private String provinceCity;
    //当前总人数
    private Integer populationTotal;
    //当前进入人数
    private Integer populationIn;
    //当前离开人数
    private Integer populationOut;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }