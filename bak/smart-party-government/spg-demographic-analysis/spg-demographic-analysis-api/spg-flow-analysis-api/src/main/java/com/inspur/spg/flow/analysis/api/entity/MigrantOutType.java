package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口迁徙-迁出类别分布(MigrantOutType)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:06
 */
@Data
@TableName("migrant_out_type")
@EqualsAndHashCode(callSuper = true)
public class MigrantOutType extends Model<MigrantOutType>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //去向类别
    private String outType;
    //人数
    private Integer outNum;
    //占比
    private BigDecimal outProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }