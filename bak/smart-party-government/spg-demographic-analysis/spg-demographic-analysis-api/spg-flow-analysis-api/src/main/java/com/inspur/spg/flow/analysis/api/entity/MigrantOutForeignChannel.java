package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 人口迁徙-境外迁出去向及渠道(MigrantOutForeignChannel)表实体类
 *
 * @author cj
 * @since 2022-02-04 15:07:08
 */
@Data
@TableName("migrant_out_foreign_channel")
@EqualsAndHashCode(callSuper = true)
public class MigrantOutForeignChannel extends Model<MigrantOutForeignChannel> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //去向类别
    private String outType;
    //去向渠道
    private String outChannel;
    //人数
    private Integer outNum;
    //占比
    private BigDecimal outProportion;
    //去向地区
    private String outArea;
    //创建时间
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }