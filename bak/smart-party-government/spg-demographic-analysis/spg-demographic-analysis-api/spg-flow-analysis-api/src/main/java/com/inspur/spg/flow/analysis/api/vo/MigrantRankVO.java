package com.inspur.spg.flow.analysis.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 迁徙排名
 */
@Data
@ApiModel(description = "迁徙排名VO")
public class MigrantRankVO {

    //地市
    @ApiModelProperty(value = "地市")
    private String area;
    //人数
    @ApiModelProperty(value = "人数")
    private BigDecimal num;
    //环比
    @ApiModelProperty(value = "环比")
    private BigDecimal mom;
    //同比
    @ApiModelProperty(value = "同比")
    private BigDecimal yoy;

}
