package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

@Data
public class BasePosition<T extends Model<?>> extends Model<T> {

    //区域类型
    private String positionType;
    //区域名称
    private String positionName;
    //区域编号
    private String positionId;
    //场站配置Id
    private Integer positionConfigId;
    //场站所属地市
    //private String positionCityName;

}
