package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 关联表迁入迁出(包含境外,省外)(MigrantRefChannelForeignProvince)表实体类
 *
 * @author CJ
 * @since 2022-02-10 11:30:13
 */
@Data
@TableName("migrant_ref_channel_foreign_province")
@EqualsAndHashCode(callSuper = true)
public class MigrantRefChannelForeignProvince extends Model<MigrantRefChannelForeignProvince>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //迁徙类型
    private String migrateType;
    //来源类别
    private String sourceType;
    //来源地区
    private String area;
    //来源渠道
    private String channelType;
    //来源占比
    private BigDecimal proportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }