package com.inspur.spg.flow.analysis.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 省内迁徙所有渠道VO
 */
@Data
@ApiModel(description = "省内迁徙所有渠道VO")
public class MigrantCityPathAllChannelVO {

    //迁徙线路
    @ApiModelProperty("迁徙线路")
    private String route;
    //公路数
    @ApiModelProperty("公路数")
    private BigDecimal highwayNum;
    //水路数
    @ApiModelProperty("水路数")
    private BigDecimal waterwayNum;
    //铁路数
    @ApiModelProperty("铁路数")
    private BigDecimal railwayNum;
    //民航数
    @ApiModelProperty("民航数")
    private BigDecimal civilAviationNum;
    //总数
    @ApiModelProperty("总数")
    private BigDecimal totalNum;

}
