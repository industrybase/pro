package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口迁徙-历史迁徙趋势(MigrantHistoricalTrend)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:06
 */
@Data
@TableName("migrant_historical_trend")
@EqualsAndHashCode(callSuper = true)
public class MigrantHistoricalTrend extends Model<MigrantHistoricalTrend>{
    
    private Long id;
    //统计日期
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date statDate;
    //省市简称
    private String provinceCityShort;
    //省市
    private String provinceCity;
    //总人口
    private Integer populationTotal;
    //迁入量
    private Integer migIn;
    //迁出量
    private Integer migOut;
    //常驻人口
    private Integer populationResi;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }