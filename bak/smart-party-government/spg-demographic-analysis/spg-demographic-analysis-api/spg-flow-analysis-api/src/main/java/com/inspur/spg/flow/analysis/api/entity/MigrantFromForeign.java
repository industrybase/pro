package com.inspur.spg.flow.analysis.api.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 人口迁徙-境外迁入来源及渠道(MigrantFromForgein)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:44:06
 */
@Data
@TableName("migrant_from_foreign")
@EqualsAndHashCode(callSuper = true)
public class MigrantFromForeign extends Model<MigrantFromForeign>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //来源类别
    private String inType;
    //来源地区
    private String inArea;
    //来源渠道0市内 1省内 2省外 3境外
    private String inChannel;
    //来源人数
    private Integer inNum;
    //来源人数
    private BigDecimal inProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }