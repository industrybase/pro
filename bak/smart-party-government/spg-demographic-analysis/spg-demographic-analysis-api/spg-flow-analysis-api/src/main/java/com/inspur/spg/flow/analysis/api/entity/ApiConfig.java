package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * API接口配置(ApiDbTableConfig)表实体类
 *
 * @author CJ
 * @since 2022-01-06 16:15:10
 */
@Data
@TableName("api_config")
@EqualsAndHashCode(callSuper = true)
public class ApiConfig extends Model<ApiConfig>{
    
    private Integer id;
    //所属企业名称
    private String companyName;
    //平台名称
    private String platformName;
    //服务标识
    private String apiName;
    //服务描述
    private String apiDescription;
    //服务地址
    private String apiUrl;
    //状态
    private Integer status;
    //创建时间
    private Date createTime;
    //bean名称
    private String beanName;
    //包路径
    private String packagePath;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }