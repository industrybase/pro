package com.inspur.spg.flow.analysis.api.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "站场旅客实体VO")
public class StationPassengerVO {

    //旅客数量
    @ApiModelProperty(value = "旅客数量")
    private Integer travelersNum;
    //到达数量
    @ApiModelProperty(value = "到达数量")
    private Integer arrivalNum;
    //发送数量
    @ApiModelProperty(value = "发送数量")
    private Integer leaveNum;

}
