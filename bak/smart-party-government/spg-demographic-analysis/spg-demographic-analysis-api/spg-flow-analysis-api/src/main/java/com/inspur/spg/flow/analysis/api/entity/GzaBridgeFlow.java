package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;


/**
 * 港珠澳大桥客流趋势-大桥客流量(GzaBridgeFlow)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:37:08
 */
@Data
@TableName("gza_bridge_flow")
@EqualsAndHashCode(callSuper = true)
public class GzaBridgeFlow extends BasePosition<GzaBridgeFlow> {
    
    private Long id;
    //统计日期
    private Date statDate;
    //每日总客流量
    private Integer traValue;
    //每日去往珠海澳门客流量
    private Integer toMzValue;
    //每日去往香港客流量
    private Integer toHkValue;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }