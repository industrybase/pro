package com.inspur.spg.flow.analysis.api.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * 港珠澳大桥客流趋势-口岸客流量(GzaBridgePortFlow)表实体类
 *
 * @author CJ
 * @since 2022-01-06 09:37:08
 */
@Data
@TableName("gza_bridge_port_flow")
@EqualsAndHashCode(callSuper = true)
public class GzaBridgePortFlow extends BasePosition<GzaBridgePortFlow> {

    private Long id;
    //统计日期
    private Date statDate;
    //人工岛每日总客流量
    private Integer rgdValue;
    //人工岛每日观光游客量
    private Integer rgdVisitor;
    //创建时间
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}