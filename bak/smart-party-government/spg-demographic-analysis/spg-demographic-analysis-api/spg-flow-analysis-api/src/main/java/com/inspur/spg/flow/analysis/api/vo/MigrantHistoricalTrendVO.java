package com.inspur.spg.flow.analysis.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.inspur.spg.common.core.util.CalculationUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


/*
 * @Author CJ
 * @Description 迁徙人口VO
 * @Date 2022/3/2 10:02
 **/
@Data
@ApiModel(description = "迁徙人口实体VO")
public class MigrantHistoricalTrendVO {

    //统计日期
    @ApiModelProperty(value = "统计日期")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date statDate;
    //省市
    @ApiModelProperty(value = "省市")
    private String provinceCity;
    //总人口(万人)
    @ApiModelProperty(value = "总人口(万人)")
    private BigDecimal populationTotal;
    //迁入量(万人)
    @ApiModelProperty(value = "迁入量(万人)")
    private BigDecimal migIn;
    //迁出量(万人)
    @ApiModelProperty(value = "迁出量(万人)")
    private BigDecimal migOut;
    //常驻人口(万人)
    @ApiModelProperty(value = "常驻人口(万人)")
    private BigDecimal populationResi;
    //环比上月
    @ApiModelProperty(value = "环比上月")
    private BigDecimal mom;
    //同比去年
    @ApiModelProperty(value = "同比去年")
    private BigDecimal yoy;

    public String getProvinceCity() {
        if("全省".equals(this.provinceCity)){
            this.provinceCity = "广东省";
        }
        return this.provinceCity;
    }

    public void setMigIn(Integer migIn) {
        this.migIn = CalculationUtils.getMigrationTenThousand(migIn);
    }

    public void setMigOut(Integer migOut) {
        this.migOut = CalculationUtils.getMigrationTenThousand(migOut);
    }

    public void setPopulationTotal(Integer populationTotal) {
        this.populationTotal = CalculationUtils.getMigrationTenThousand(populationTotal);
    }

    public void setPopulationResi(Integer populationResi) {
        this.populationResi = CalculationUtils.getMigrationTenThousand(populationResi);
    }
}
