package com.inspur.spg.flow.analysis.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/*
 * @Author CJ
 * @Description 场站DTO类
 * @Date 2022/1/11 18:00
 **/
@Data
@ApiModel(description = "枢纽基类DTO")
public class BasePositionDTO implements Serializable {

    private static final long serialVersionUID = 7529818781403618337L;

    /**
     * 枢纽编号
     */
    @ApiModelProperty(value = "枢纽编号")
    private String positionId;
    /**
     * 枢纽名称
     */
    @ApiModelProperty(value = "枢纽名称")
    private String positionName;
    /**
     * 枢纽类型Id
     */
    @ApiModelProperty(value = "枢纽类型Id")
    private Integer positionTypeId;

    /**
     * 城市名称
     */
    @ApiModelProperty(value = "城市名称")
    private String cityName;

    /**
     * 统计开始日期
     */
    @ApiModelProperty(value = "查询开始日期")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startDate;

    /**
     * 统计结束日期
     */
    @ApiModelProperty(value = "查询结束日期")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endDate;

    /**
     * 统计结束日期
     */
    @ApiModelProperty(value = "查询日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date statDate;

}
