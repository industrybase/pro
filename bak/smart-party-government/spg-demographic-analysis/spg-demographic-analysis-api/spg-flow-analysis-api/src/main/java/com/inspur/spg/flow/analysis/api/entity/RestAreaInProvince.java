package com.inspur.spg.flow.analysis.api.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 服务区客流画像归属-省内(RestAreaInProvince)表实体类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Data
@TableName("rest_area_in_province")
@EqualsAndHashCode(callSuper = true)
public class RestAreaInProvince extends BasePosition<RestAreaInProvince>{
    
    private Long id;
    //统计日期
    private Date statDate;
    //统计时点
    private Date statTime;
    //城市名称
    private String cityName;
    //旅客人数
    private Integer travelerNum;
    //旅客占比
    private BigDecimal travelerProportion;
    //创建时间
    private Date createTime;



    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    protected Serializable pkVal() {
        return this.id;
    }
    }