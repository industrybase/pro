package com.inspur.spg.real.time.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.real.time.flow.analysis.mapper.PeopleRealTimeJobConfigMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeJobConfig;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeJobConfigService;
import org.springframework.stereotype.Service;

/**
 * 实时位置活动统计订阅任务(PeopleRealJobConfig)表服务实现类
 *
 * @author CJ
 * @since 2022-01-13 17:48:55
 */
@Service
public class PeopleRealTimeJobConfigServiceImpl extends ServiceImpl<PeopleRealTimeJobConfigMapper, PeopleRealTimeJobConfig> implements PeopleRealTimeJobConfigService {

}