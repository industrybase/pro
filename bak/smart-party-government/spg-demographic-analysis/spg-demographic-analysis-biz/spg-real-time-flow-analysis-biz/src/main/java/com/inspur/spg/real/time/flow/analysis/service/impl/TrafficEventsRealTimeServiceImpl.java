package com.inspur.spg.real.time.flow.analysis.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.real.time.flow.analysis.api.vo.TrafficEventsRealTimeVO;
import com.inspur.spg.real.time.flow.analysis.mapper.TrafficEventsRealTimeMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.TrafficEventsRealTime;
import com.inspur.spg.real.time.flow.analysis.service.TrafficEventsRealTimeService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 实时交通事件(TrafficEventsRealTime)表服务实现类
 *
 * @author CJ
 * @since 2022-02-09 11:17:56
 */
@Service

public class TrafficEventsRealTimeServiceImpl extends CommonServiceImpl<TrafficEventsRealTimeMapper, TrafficEventsRealTime> implements TrafficEventsRealTimeService {

    @Override
    @Transactional
    public void saveMapList(List<Map<String, Object>> list) {
        List<TrafficEventsRealTime> saveList = new ArrayList<>();
        List<String> eventIds = new ArrayList<>();
        for(Map<String,Object> item : list){
            Map<String, Object> map = new HashMap<>();
            map.putAll(item);
            //只统计当天发生的事故
            String startTime = (String) item.get("startTime");
            String today = DateUtil.formatDate(new Date());
            String roadName = (String) map.get("roadName");
            if((DateUtil.parseDate(today).getTime() != DateUtil.parseDate(startTime).getTime())
            || StringUtils.isBlank(roadName)){
                continue;
            }
            map.put("startTime", DateUtil.parse((String) item.get("startTime"), "yyyy-MM-dd H:m:s"));
            map.put("endTime", DateUtil.parse((String) item.get("endTime"), "yyyy-MM-dd H:m:s"));
            map.put("coordinates", item.get("lines"));
            TrafficEventsRealTime trafficEventsRealTime = JSONObject.parseObject(JSONObject.toJSONString(map), TrafficEventsRealTime.class);
            /*TrafficEventsRealTime oldEntity = selectByEventId(trafficEventsRealTime.getEventId());
            if(oldEntity != null){
                TrafficEventsRealTime newEntity = new TrafficEventsRealTime();
                BeanUtil.copyProperties(trafficEventsRealTime,newEntity);
                newEntity.setId(oldEntity.getId());
                removeList.add(newEntity);
            }*/
            eventIds.add(trafficEventsRealTime.getEventId());
            trafficEventsRealTime.setStatDate(trafficEventsRealTime.getStartTime());
            trafficEventsRealTime.setCreateTime(new Date());
            saveList.add(trafficEventsRealTime);
        }
        if(CollectionUtils.isNotEmpty(saveList)){
            remove(Wrappers.<TrafficEventsRealTime>lambdaQuery().in(TrafficEventsRealTime::getEventId,eventIds));
            //remove(Wrappers.<TrafficEventsRealTime>lambdaQuery().in(TrafficEventsRealTime::getEventId,eventIds));
            saveBatch(saveList);
        }
    }

    @Override
    public TrafficEventsRealTime getByEventId(String eventId) {
        return getOne(Wrappers.<TrafficEventsRealTime>lambdaQuery().eq(TrafficEventsRealTime::getEventId,eventId));
    }

    @Override
    public List<TrafficEventsRealTimeVO> geTrafficEventsRealTimeVoList(Date startTime, Date endTime) {
        return baseMapper.geTrafficEventsRealTimeVoList(startTime,endTime);
    }


}