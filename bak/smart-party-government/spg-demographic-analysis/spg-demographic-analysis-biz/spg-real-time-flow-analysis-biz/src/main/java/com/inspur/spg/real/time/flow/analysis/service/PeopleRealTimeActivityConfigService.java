package com.inspur.spg.real.time.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeActivityConfig;

import java.util.List;

/**
 * 实时位置活动配置(PeopleRealActivityConfig)表服务接口
 *
 * @author CJ
 * @since 2022-01-29 11:46:04
 */
public interface PeopleRealTimeActivityConfigService extends IService<PeopleRealTimeActivityConfig> {

    /**
     * @Author CJ
     * @Description 根据场站名称编码
     * @Date 2022/2/21 20:01
     * @param areaNo
     * @param list
     **/
    PeopleRealTimeActivityConfig getByAreaNo(String areaNo, List<PeopleRealTimeActivityConfig> list);

    /**
     * @Author CJ
     * @Description 查询所有
     * @Date 2022/2/21 20:02
     * @param basePositionDto
     **/
    List<PeopleRealTimeActivityConfig> getAll(BasePositionDTO basePositionDto);

}