package com.inspur.spg.real.time.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeFlowInService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/28 19:02
 **/
@RestController
@RequestMapping("/peopleRealTimeFlowIn")
public class PeopleRealTimeFlowInController {

    @Resource
    private PeopleRealTimeFlowInService peopleRealTimeFlowInService;

    @PostMapping("/saveBatch")
    @ApiIgnore
    @Inner
    public R saveBatch(@RequestBody List<Map<String,Object>> list){
        peopleRealTimeFlowInService.saveBatchByList(list);
        return R.ok();
    }


}
