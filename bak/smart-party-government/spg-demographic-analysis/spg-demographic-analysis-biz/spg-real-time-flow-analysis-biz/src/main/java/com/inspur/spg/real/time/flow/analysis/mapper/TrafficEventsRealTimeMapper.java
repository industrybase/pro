package com.inspur.spg.real.time.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.TrafficEventsRealTime;
import com.inspur.spg.real.time.flow.analysis.api.vo.TrafficEventsRealTimeVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 实时交通事件(TrafficEventsRealTime)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-09 11:17:56
 */
@Mapper
public interface TrafficEventsRealTimeMapper extends CommonMapper<TrafficEventsRealTime> {

    /**
     * @Author CJ
     * @Description 根据时间查询实时事件
     * @Date 2022/2/15 10:39
     * @param startTime
     * @param endTime
     * @return java.util.List<com.inspur.spg.real.time.flow.analysis.api.vo.TrafficEventsRealTimeVo>
     **/
    List<TrafficEventsRealTimeVO> geTrafficEventsRealTimeVoList(@Param("startTime") Date startTime, @Param("endTime") Date endTime);



}