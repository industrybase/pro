package com.inspur.spg.real.time.flow.analysis;

import com.inspur.spg.common.feign.annotation.EnableSpgFeignClients;
import com.inspur.spg.common.security.annotation.EnableSpgResourceServer;
import com.inspur.spg.common.swagger.annotation.EnableSpgSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/6 11:00
 **/
@EnableSpgFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@EnableSpgResourceServer
@EnableSpgSwagger2
public class SpgRealTimeFlowAnalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpgRealTimeFlowAnalysisApplication.class, args);
    }

}
