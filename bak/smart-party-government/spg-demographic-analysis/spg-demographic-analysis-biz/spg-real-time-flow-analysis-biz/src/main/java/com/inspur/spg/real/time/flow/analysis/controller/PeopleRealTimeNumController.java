package com.inspur.spg.real.time.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.vo.PeopleRealTimeNumVO;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeNumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description 实时客流控制层
 * @Date 2022/1/13 14:23
 **/
@RestController
@RequestMapping("/peopleRealTimeNum")
@Api(value = "peopleRealTimeNum", tags = "实时客流")
public class PeopleRealTimeNumController {

    @Resource
    private PeopleRealTimeNumService peopleRealTimeNumService;

    @PostMapping("/saveBatch")
    @ApiIgnore
    @Inner
    public R saveBatch(@RequestBody List<Map<String, Object>> list) {
        peopleRealTimeNumService.saveBatchByList(list);
        return R.ok();
    }

    /**
     * @param
     * @return java.util.List<com.inspur.spg.real.flow.analysis.api.entity.PeopleRealTimeNum>
     * @Author CJ
     * @Description 获取实时数据
     * @Date 2022/2/9 16:37
     **/
    @GetMapping("/getRealTimeList")
    @ApiOperation(value = "获取实时人数", notes = "获取实时人数")
    public R<List<PeopleRealTimeNumVO>> getRealTimeList(@Valid BasePositionDTO positionDto) {
        if(positionDto.getStartDate() == null || positionDto.getEndDate() == null){
            return R.failed("查询开始日期和结束日期不能为空");
        }
        return R.ok(peopleRealTimeNumService.getRealTimeList(positionDto));
    }

}
