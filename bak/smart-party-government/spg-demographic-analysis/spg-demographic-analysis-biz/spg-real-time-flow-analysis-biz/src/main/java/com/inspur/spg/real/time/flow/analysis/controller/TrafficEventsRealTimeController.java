package com.inspur.spg.real.time.flow.analysis.controller;

import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.real.time.flow.analysis.api.vo.TrafficEventsRealTimeVO;
import com.inspur.spg.real.time.flow.analysis.service.TrafficEventsRealTimeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description 实时交通事件
 * @Date 2022/2/9 11:24
 **/
@RestController
@RequestMapping("/trafficEventsRealTime")
@Api(value = "trafficEventsRealTime", tags = "实时交通事件")
public class TrafficEventsRealTimeController {

    @Resource
    private TrafficEventsRealTimeService trafficEventsRealTimeService;

    @PostMapping("/saveBatch")
    @ApiIgnore
    @Inner
    public R saveBatch(@RequestBody List<Map<String,Object>> list){
        trafficEventsRealTimeService.saveMapList(list);
        return R.ok();
    }

    /**
     * @Author CJ
     * @Description 获取最新一小时实时数据
     * @Date 2022/2/9 15:18
     * @param
     * @return com.inspur.spg.common.core.util.R<java.util.List<com.inspur.spg.real.flow.analysis.api.entity.TrafficEventsRealTime>>
     **/
    @GetMapping("/getRealTimeList")
    @ApiOperation(value = "获取实时数据", notes = "获取实时数据")
    public R<List<TrafficEventsRealTimeVO>> getRealTimeList(){
        List<TrafficEventsRealTimeVO> list = trafficEventsRealTimeService.geTrafficEventsRealTimeVoList(DateUtil.addHours(new Date(),-1),new Date());
        return R.ok(list);
    }

}
