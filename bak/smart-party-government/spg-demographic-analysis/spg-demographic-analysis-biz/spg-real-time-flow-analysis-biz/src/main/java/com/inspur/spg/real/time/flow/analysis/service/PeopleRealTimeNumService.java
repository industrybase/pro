package com.inspur.spg.real.time.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeNum;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;
import com.inspur.spg.real.time.flow.analysis.api.vo.PeopleRealTimeNumVO;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实时人数(PeopleRealTimeNum)表服务接口
 *
 * @author cj
 * @since 2022-01-10 16:34:51
 */
public interface PeopleRealTimeNumService extends ICommonService<PeopleRealTimeNum> {

    void saveBatchByList(List<Map<String,Object>> listMap);

    /**
     * @Author CJ
     * @Description 获取实时数据
     * @Date 2022/2/9 17:30
     * @param
     * @return java.util.List<com.inspur.spg.real.flow.analysis.api.entity.PeopleRealTimeNum>
     **/
    List<PeopleRealTimeNumVO> getRealTimeList(BasePositionDTO positionDto);

    /**
     * @Author CJ
     * @Description 根据日期统计人数
     * @Date 2022/2/13 10:56
     * @param statDate
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<PeopleRealTimeResult> getTotalNumByStatDateGroupByPositionId(Date statDate);


}