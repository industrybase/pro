package com.inspur.spg.real.time.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeJobConfig;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeJobConfigService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;

/*
 * @Author CJ
 * @Description 人员实时位置活动任务配置
 * @Date 2022/1/13 17:53
 **/
@RestController
@RequestMapping("/peopleRealTimeJobConfig")
@ApiIgnore
public class PeopleRealTimeJobConfigController {

    @Resource
    private PeopleRealTimeJobConfigService peopleRealTimeJobConfigService;

    @GetMapping("/getAll")
    @ApiIgnore
    @Inner
    public R<List<PeopleRealTimeJobConfig>> getAll(){
        return R.ok(peopleRealTimeJobConfigService.list());
    }

}
