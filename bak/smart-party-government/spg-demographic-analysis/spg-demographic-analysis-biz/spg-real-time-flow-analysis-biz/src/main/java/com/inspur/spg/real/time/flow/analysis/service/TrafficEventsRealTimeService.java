package com.inspur.spg.real.time.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.real.time.flow.analysis.api.entity.TrafficEventsRealTime;
import com.inspur.spg.real.time.flow.analysis.api.vo.TrafficEventsRealTimeVO;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实时交通事件(TrafficEventsRealTime)表服务接口
 *
 * @author CJ
 * @since 2022-02-09 11:17:56
 */
public interface TrafficEventsRealTimeService extends ICommonService<TrafficEventsRealTime> {

    void saveMapList(List<Map<String,Object>> list);

    /**
     * @Author CJ
     * @Description 根据eventId查询
     * @Date 2022/2/9 15:39
     * @param eventId
     * @return com.inspur.spg.real.flow.analysis.api.entity.TrafficEventsRealTime
     **/
    TrafficEventsRealTime getByEventId(String eventId);

    /**
     * @Author CJ
     * @Description 根据时间查询实时事件
     * @Date 2022/2/15 10:39
     * @param startTime
     * @param endTime
     * @return java.util.List<com.inspur.spg.real.time.flow.analysis.api.vo.TrafficEventsRealTimeVo>
     **/
    List<TrafficEventsRealTimeVO> geTrafficEventsRealTimeVoList(Date startTime, Date endTime);




}