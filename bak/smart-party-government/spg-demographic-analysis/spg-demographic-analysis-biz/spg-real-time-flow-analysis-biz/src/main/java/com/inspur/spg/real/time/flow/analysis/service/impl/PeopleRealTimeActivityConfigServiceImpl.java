package com.inspur.spg.real.time.flow.analysis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeActivityConfig;
import com.inspur.spg.real.time.flow.analysis.mapper.PeopleRealTimeActivityConfigMapper;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeActivityConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实时位置活动配置(PeopleRealActivityConfig)表服务实现类
 *
 * @author CJ
 * @since 2022-01-29 11:46:04
 */
@Service
public class PeopleRealTimeActivityConfigServiceImpl extends ServiceImpl<PeopleRealTimeActivityConfigMapper, PeopleRealTimeActivityConfig> implements PeopleRealTimeActivityConfigService {


    @Override
    public PeopleRealTimeActivityConfig getByAreaNo(String areaNo, List<PeopleRealTimeActivityConfig> list) {
        return list.stream().filter(item -> item.getAreaNo().equals(areaNo)).findFirst().orElse(null);
    }

    @Override
    public List<PeopleRealTimeActivityConfig> getAll(BasePositionDTO basePositionDto) {
        LambdaQueryWrapper<PeopleRealTimeActivityConfig> lambdaQueryWrapper = Wrappers.lambdaQuery();
        if(StringUtils.isNotBlank(basePositionDto.getCityName())){
            lambdaQueryWrapper.eq(PeopleRealTimeActivityConfig::getCityName,basePositionDto.getCityName());
        }
        if(basePositionDto.getPositionTypeId() != null){
            lambdaQueryWrapper.eq(PeopleRealTimeActivityConfig::getAreaTypeId,basePositionDto.getPositionTypeId());
        }
        if(StringUtils.isNotBlank(basePositionDto.getPositionId())){
            lambdaQueryWrapper.eq(PeopleRealTimeActivityConfig::getAreaNo,basePositionDto.getPositionId());
        }
        lambdaQueryWrapper.eq(PeopleRealTimeActivityConfig::getStatus, CommonConstants.ENABLE);
        return list(lambdaQueryWrapper);
    }


}