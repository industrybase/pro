package com.inspur.spg.real.time.flow.analysis.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.core.util.PredicateUtils;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeActivityConfig;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeNum;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;
import com.inspur.spg.real.time.flow.analysis.api.vo.PeopleRealTimeNumVO;
import com.inspur.spg.real.time.flow.analysis.mapper.PeopleRealTimeNumMapper;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeActivityConfigService;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeNumService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 实时人数(PeopleRealTimeNum)表服务实现类
 *
 * @author cj
 * @since 2022-01-10 16:34:53
 */
@Service
public class PeopleRealTimeNumServiceImpl extends CommonServiceImpl<PeopleRealTimeNumMapper, PeopleRealTimeNum> implements PeopleRealTimeNumService {

    @Resource
    private PeopleRealTimeActivityConfigService peopleRealTimeActivityConfigService;

    @Override
    public void saveBatchByList(List<Map<String, Object>> listMap) {
        List<PeopleRealTimeNum> saveList = new ArrayList<>();
        List<PeopleRealTimeActivityConfig> peopleRealTimeActivityConfigs = peopleRealTimeActivityConfigService.list();
        for (Map<String, Object> itemMap : listMap) {
            String positionId = (String) itemMap.get("areaNo");
            String timeStr = (String) itemMap.get("timeCode");
            PeopleRealTimeActivityConfig peopleRealTimeActivityConfig = peopleRealTimeActivityConfigService.getByAreaNo(positionId, peopleRealTimeActivityConfigs);
            if (peopleRealTimeActivityConfig == null) {
                continue;
            }
            Date statTime = DateUtil.parse(timeStr.toString(), "yyyyMMddHHmmss");
            Integer num = (Integer) itemMap.get("areaCnt");
            PeopleRealTimeNum peopleRealTimeNum = new PeopleRealTimeNum();
            peopleRealTimeNum.setPositionName(peopleRealTimeActivityConfig.getAreaName());
            peopleRealTimeNum.setPositionId(positionId);
            peopleRealTimeNum.setActivityId(peopleRealTimeActivityConfig.getId());
            peopleRealTimeNum.setNum(num);
            peopleRealTimeNum.setStatTime(statTime);
            peopleRealTimeNum.setStatDate(statTime);
            peopleRealTimeNum.setCreateTime(new Date());
            saveList.add(peopleRealTimeNum);
        }
        fastSaveIgnoreBatch(saveList);
    }


    @Override
    public List<PeopleRealTimeNumVO> getRealTimeList(BasePositionDTO positionDto) {
        List<PeopleRealTimeActivityConfig> configList = peopleRealTimeActivityConfigService.getAll(positionDto);
        List<String> positionIds = configList.stream().map(item -> item.getAreaNo()).collect(Collectors.toList());
        Set<Date> maxTimeArr = baseMapper.getMaxTimesByPositionIds(positionIds);
        //获取当天数据
        LambdaQueryWrapper<PeopleRealTimeNum> wrapper = Wrappers.<PeopleRealTimeNum>lambdaQuery().
                in(PeopleRealTimeNum::getStatTime, maxTimeArr).
                eq(PeopleRealTimeNum::getStatDate, DateUtil.getCurrDate());
        wrapper.orderByDesc(PeopleRealTimeNum::getStatTime);
        List<PeopleRealTimeNum> list = list(wrapper);
        //日期排序后去除重复场站
        list = list.stream().sorted(Comparator.comparing(PeopleRealTimeNum::getStatTime).reversed()).filter(PredicateUtils.distinctByKey(PeopleRealTimeNum::getPositionId)).collect(Collectors.toList());
        List<PeopleRealTimeNumVO> peopleRealTimeNumVOS = new ArrayList<>();
        for(PeopleRealTimeActivityConfig config : configList){
            out:for(PeopleRealTimeNum peopleRealTimeNum : list){
                if(peopleRealTimeNum.getPositionId().equals(config.getAreaNo())){
                    PeopleRealTimeNumVO peopleRealTimeNumVo = new PeopleRealTimeNumVO();
                    BeanUtil.copyProperties(peopleRealTimeNum,peopleRealTimeNumVo);
                    //根据实时系数阈值计算人数
                    Integer num = (int) Math.round(config.getRtc().doubleValue() * peopleRealTimeNum.getNum());
                    peopleRealTimeNumVo.setNum(num);
                    peopleRealTimeNumVo.setPositionType(config.getAreaTypeName());
                    peopleRealTimeNumVo.setCity(config.getCityName());
                    peopleRealTimeNumVo.setStatus(getStatusByWarningLow(num,config.getWarningLow().doubleValue()));
                    peopleRealTimeNumVOS.add(peopleRealTimeNumVo);
                    break out;
                }
            }
        }
        return peopleRealTimeNumVOS;
    }

    @Override
    public List<PeopleRealTimeResult> getTotalNumByStatDateGroupByPositionId(Date statDate) {
        return baseMapper.getTotalNumByStatDateGroupByPositionId(statDate);
    }


    /**
     * @Author CJ
     * @Description 根据告警阈值计算告警状态
     * @Date 2022/2/9 19:57
     * @param num
     * @param warningLow
     * @return java.lang.String
     **/
    private String getStatusByWarningLow(Integer num,Double warningLow){
        if(num > warningLow * 0.3 && num <= warningLow * 0.6){
            return "适中";
        }
        if(num > warningLow * 0.6 && num <= warningLow * 0.9){
            return "拥挤";
        }
        if(num > warningLow * 0.6 && num <= warningLow * 1.5){
            return "轻度告警";
        }
        if(num > warningLow * 1.5 && num <= warningLow * 2){
            return "中度告警";
        }
        if(num > warningLow * 2){
            return "'重度告警";
        }
        return "舒适";
    }
}