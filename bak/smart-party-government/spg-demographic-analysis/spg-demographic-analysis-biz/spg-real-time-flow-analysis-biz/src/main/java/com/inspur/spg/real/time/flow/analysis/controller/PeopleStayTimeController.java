package com.inspur.spg.real.time.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.real.time.flow.analysis.service.PeopleStayTimeService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description 实时驻留控制层
 * @Date 2022/1/13 14:23
 **/
@RestController
@RequestMapping("/peopleStayTime")
public class PeopleStayTimeController {

    @Resource
    private PeopleStayTimeService peopleStayTimeService;

    @PostMapping("/saveBatch")
    @ApiIgnore
    @Inner
    public R saveBatch(@RequestBody List<Map<String,Object>> list){
        peopleStayTimeService.saveBatchByList(list);
        return R.ok();
    }

}
