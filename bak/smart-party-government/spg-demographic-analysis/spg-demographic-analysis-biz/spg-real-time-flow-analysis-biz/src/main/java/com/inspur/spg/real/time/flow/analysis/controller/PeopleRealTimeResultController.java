package com.inspur.spg.real.time.flow.analysis.controller;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeResultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/13 10:30
 **/
@RestController
@RequestMapping("/peopleRealTimeResult")
@Api(value = "peopleRealTimeResult", tags = "实时人数（天）结果")
public class PeopleRealTimeResultController {

    @Resource
    private PeopleRealTimeResultService peopleRealTimeResultService;

    @PostMapping("/executeSave")
    @ApiIgnore
    @Inner
    public R executeSave(@RequestBody Map<String,Object> paramsMap) throws ClassNotFoundException {
        String statDate = (String) paramsMap.get("statDate");
        String entityClassName = (String) paramsMap.get("entityClassName");
        peopleRealTimeResultService.saveByStatDate(entityClassName, DateUtil.parseDate(statDate));
        return R.ok();
    }

    /**
     * @Author CJ
     * @Description 获取统计图数据
     * @Date 2022/2/13 14:57
     * @param
     * @return com.inspur.spg.common.core.util.R<java.util.List<com.inspur.spg.common.core.entity.ChartsInfo<java.math.BigDecimal>>>
     **/
    @GetMapping("/getChartsInfo")
    @ApiOperation(value = "获取统计图数据", notes = "获取统计图数据")
    public R<List<ChartsInfo<BigDecimal>>> getChartsInfo(@Valid BasePositionDTO positionDto){
        if(positionDto.getStartDate() == null || positionDto.getEndDate() == null){
            return R.failed("查询开始日期和结束日期不能为空");
        }
        return R.ok(peopleRealTimeResultService.getChartsInfo(positionDto));
    }

}
