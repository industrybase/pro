package com.inspur.spg.real.time.flow.analysis.service.impl;

import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeActivityConfig;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleStayTime;
import com.inspur.spg.real.time.flow.analysis.mapper.PeopleStayTimeMapper;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeActivityConfigService;
import com.inspur.spg.real.time.flow.analysis.service.PeopleStayTimeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实时人流驻留时长分析(PeopleStayTime)表服务实现类
 *
 * @author cj
 * @since 2022-01-10 16:35:30
 */
@Service
public class PeopleStayTimeServiceImpl extends CommonServiceImpl<PeopleStayTimeMapper, PeopleStayTime> implements PeopleStayTimeService {

    @Resource
    private PeopleRealTimeActivityConfigService peopleRealTimeActivityConfigService;

    @Override
    public void saveBatchByList(List<Map<String, Object>> listMap) {
        List<PeopleStayTime> saveList = new ArrayList<>();
        List<PeopleRealTimeActivityConfig> peopleRealTimeActivityConfigs = peopleRealTimeActivityConfigService.list();
        for (Map<String, Object> itemMap : listMap) {
            String positionId = (String) itemMap.get("areaNo");
            String timeStr = (String) itemMap.get("timeCode");
            PeopleRealTimeActivityConfig peopleRealTimeActivityConfig = peopleRealTimeActivityConfigService.getByAreaNo(positionId, peopleRealTimeActivityConfigs);
            if (peopleRealTimeActivityConfig == null) {
                continue;
            }
            Date statTime = DateUtil.parse(timeStr.toString(), "yyyyMMddHHmmss");
            Integer num = (Integer) itemMap.get("areaCnt");
            Integer stayTime = Integer.valueOf(itemMap.get("tSort").toString());
            PeopleStayTime peopleStayTime = new PeopleStayTime();
            peopleStayTime.setPositionName(peopleRealTimeActivityConfig.getAreaName());
            peopleStayTime.setPositionId(positionId);
            peopleStayTime.setActivityId(peopleRealTimeActivityConfig.getId());
            peopleStayTime.setNum(num);
            peopleStayTime.setStayTime(stayTime);
            peopleStayTime.setStatTime(statTime);
            peopleStayTime.setStatDate(statTime);
            peopleStayTime.setCreateTime(new Date());
            saveList.add(peopleStayTime);
        }
        fastSaveIgnoreBatch(saveList);
    }
}