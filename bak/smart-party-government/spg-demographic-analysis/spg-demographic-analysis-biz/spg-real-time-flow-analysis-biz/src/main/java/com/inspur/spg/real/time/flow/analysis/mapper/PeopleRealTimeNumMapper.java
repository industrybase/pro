package com.inspur.spg.real.time.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeNum;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 实时人数(PeopleRealTimeNum)表数据库访问层
 *
 * @author cj
 * @since 2022-01-10 16:34:44
 */
@Mapper
public interface PeopleRealTimeNumMapper extends CommonMapper<PeopleRealTimeNum> {

    /**
     * @Author CJ
     * @Description 获取城市或枢纽id最新日期
     * @Date 2022/2/9 18:34
     * @param positionIds
     * @return java.util.List<java.util.Date>
     **/
    Set<Date> getMaxTimesByPositionIds(@Param("positionIds") List<String> positionIds);

    /**
     * @Author CJ
     * @Description 根据日期统计人数
     * @Date 2022/2/13 10:56
     * @param statDate
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<PeopleRealTimeResult> getTotalNumByStatDateGroupByPositionId(@Param("statDate")Date statDate);

}