package com.inspur.spg.real.time.flow.analysis.service.impl;

import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeActivityConfig;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeFlowIn;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;
import com.inspur.spg.real.time.flow.analysis.mapper.PeopleRealTimeFlowInMapper;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeActivityConfigService;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeFlowInService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实时人口流入(PeopleRealFlowIn)表服务实现类
 *
 * @author CJ
 * @since 2022-01-28 18:13:11
 */
@Service
public class PeopleRealTimeFlowInServiceImpl extends CommonServiceImpl<PeopleRealTimeFlowInMapper, PeopleRealTimeFlowIn> implements PeopleRealTimeFlowInService {

    @Resource
    private PeopleRealTimeActivityConfigService peopleRealTimeActivityConfigService;

    @Override
    public void saveBatchByList(List<Map<String, Object>> listMap) {
        List<PeopleRealTimeFlowIn> saveList = new ArrayList<>();
        List<PeopleRealTimeActivityConfig> peopleRealTimeActivityConfigs = peopleRealTimeActivityConfigService.list();
        for (Map<String, Object> itemMap : listMap) {
            String positionId = (String) itemMap.get("areaNo");
            String timeStr = (String) itemMap.get("timeCode");
            PeopleRealTimeActivityConfig peopleRealTimeActivityConfig = peopleRealTimeActivityConfigService.getByAreaNo(positionId, peopleRealTimeActivityConfigs);
            if (peopleRealTimeActivityConfig == null) {
                continue;
            }
            Date statTime = DateUtil.parse(timeStr.toString(), "yyyyMMddHHmmss");
            Integer num = (Integer) itemMap.get("areaCnt");
            PeopleRealTimeFlowIn peopleRealTimeFlowIn = new PeopleRealTimeFlowIn();
            peopleRealTimeFlowIn.setPositionName(peopleRealTimeActivityConfig.getAreaName());
            peopleRealTimeFlowIn.setPositionId(positionId);
            peopleRealTimeFlowIn.setNum(num);
            peopleRealTimeFlowIn.setActivityId(peopleRealTimeActivityConfig.getId());
            peopleRealTimeFlowIn.setStatTime(statTime);
            peopleRealTimeFlowIn.setStatDate(statTime);
            peopleRealTimeFlowIn.setCreateTime(new Date());
            saveList.add(peopleRealTimeFlowIn);
        }
        fastSaveIgnoreBatch(saveList);
    }

    @Override
    public List<PeopleRealTimeResult> getTotalNumByStatDateGroupByPositionId(Date statDate) {
        return baseMapper.getTotalNumByStatDateGroupByPositionId(statDate);
    }

}