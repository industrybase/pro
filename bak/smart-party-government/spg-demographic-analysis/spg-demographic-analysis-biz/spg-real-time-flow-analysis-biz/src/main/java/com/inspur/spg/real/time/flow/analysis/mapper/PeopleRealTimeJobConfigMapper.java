package com.inspur.spg.real.time.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeJobConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 实时位置活动统计订阅任务(PeopleRealJobConfig)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-13 17:48:53
 */
@Mapper
public interface PeopleRealTimeJobConfigMapper extends BaseMapper<PeopleRealTimeJobConfig> {

}