package com.inspur.spg.real.time.flow.analysis.service;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 实时人数结果表(PeopleRealTimeResult)表服务接口
 *
 * @author CJ
 * @since 2022-02-13 10:22:40
 */
public interface PeopleRealTimeResultService extends ICommonService<PeopleRealTimeResult> {

    /**
     * @Author CJ
     * @Description 每天保存实时人数前一天结果数
     * @Date 2022/2/13 12:12
     * @param entityClassName
     * @param statDate
     * @return void
     **/
    void saveByStatDate(String entityClassName,Date statDate) throws ClassNotFoundException;

    /**
     * @Author CJ
     * @Description 查询统计
     * @Date 2022/2/13 14:28
     * @param positionDto
     * @return java.util.List<java.util.List<com.inspur.spg.common.core.entity.ChartsInfo>>
     **/
    List<ChartsInfo<BigDecimal>> getChartsInfo(BasePositionDTO positionDto);
}