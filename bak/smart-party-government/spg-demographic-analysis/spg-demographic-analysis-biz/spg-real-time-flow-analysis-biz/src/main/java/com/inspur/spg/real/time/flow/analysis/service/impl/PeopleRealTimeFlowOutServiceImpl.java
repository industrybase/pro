package com.inspur.spg.real.time.flow.analysis.service.impl;

import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeActivityConfig;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeFlowOut;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;
import com.inspur.spg.real.time.flow.analysis.mapper.PeopleRealTimeFlowOutMapper;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeActivityConfigService;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeFlowOutService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实时人口流出(PeopleRealFlowOut)表服务实现类
 *
 * @author CJ
 * @since 2022-01-28 18:12:40
 */
@Service
public class PeopleRealTimeFlowOutServiceImpl extends CommonServiceImpl<PeopleRealTimeFlowOutMapper, PeopleRealTimeFlowOut> implements PeopleRealTimeFlowOutService {

    @Resource
    private PeopleRealTimeActivityConfigService peopleRealTimeActivityConfigService;

    @Override
    public void saveBatchByList(List<Map<String, Object>> listMap) {
        List<PeopleRealTimeFlowOut> saveList = new ArrayList<>();
        List<PeopleRealTimeActivityConfig> peopleRealTimeActivityConfigs = peopleRealTimeActivityConfigService.list();
        for(Map<String,Object> itemMap : listMap){
            String positionId = (String) itemMap.get("areaNo");
            String timeStr = (String) itemMap.get("timeCode");
            PeopleRealTimeActivityConfig peopleRealTimeActivityConfig = peopleRealTimeActivityConfigService.getByAreaNo(positionId, peopleRealTimeActivityConfigs);
            if(peopleRealTimeActivityConfig == null){
                continue;
            }
            Date statTime = DateUtil.parse(timeStr.toString(),"yyyyMMddHHmmss");
            Integer num = (Integer) itemMap.get("areaCnt");
            PeopleRealTimeFlowOut peopleRealTimeFlowOut = new PeopleRealTimeFlowOut();
            peopleRealTimeFlowOut.setPositionName(peopleRealTimeActivityConfig.getAreaName());
            peopleRealTimeFlowOut.setPositionId(positionId);
            peopleRealTimeFlowOut.setActivityId(peopleRealTimeActivityConfig.getId());
            peopleRealTimeFlowOut.setNum(num);
            peopleRealTimeFlowOut.setStatTime(statTime);
            peopleRealTimeFlowOut.setStatDate(statTime);
            peopleRealTimeFlowOut.setCreateTime(new Date());
            saveList.add(peopleRealTimeFlowOut);
        }
        fastSaveIgnoreBatch(saveList);
    }

    @Override
    public List<PeopleRealTimeResult> getTotalNumByStatDateGroupByPositionId(Date statDate) {
        return baseMapper.getTotalNumByStatDateGroupByPositionId(statDate);
    }

}