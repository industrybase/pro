package com.inspur.spg.real.time.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeFlowIn;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实时人口流入(PeopleRealFlowIn)表服务接口
 *
 * @author CJ
 * @since 2022-01-28 18:13:11
 */
public interface PeopleRealTimeFlowInService extends ICommonService<PeopleRealTimeFlowIn> {

    void saveBatchByList(List<Map<String,Object>> listMap);


    /**
     * @Author CJ
     * @Description 根据日期统计人数
     * @Date 2022/2/13 10:56
     * @param statDate
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<PeopleRealTimeResult> getTotalNumByStatDateGroupByPositionId(Date statDate);


}