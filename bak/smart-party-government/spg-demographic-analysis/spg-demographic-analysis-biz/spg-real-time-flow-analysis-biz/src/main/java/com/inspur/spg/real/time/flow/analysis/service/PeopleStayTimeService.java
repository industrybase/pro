package com.inspur.spg.real.time.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleStayTime;

import java.util.List;
import java.util.Map;

/**
 * 实时人流驻留时长分析(PeopleStayTime)表服务接口
 *
 * @author cj
 * @since 2022-01-10 16:35:30
 */
public interface PeopleStayTimeService  extends ICommonService<PeopleStayTime> {

    void saveBatchByList(List<Map<String, Object>> listMap);

}