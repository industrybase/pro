package com.inspur.spg.real.time.flow.analysis.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.StringUtils;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeFlowIn;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeFlowOut;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeNum;
import com.inspur.spg.real.time.flow.analysis.api.vo.PeopleRealTimeResultChartsVO;
import com.inspur.spg.real.time.flow.analysis.mapper.PeopleRealTimeResultMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeFlowInService;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeFlowOutService;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeNumService;
import com.inspur.spg.real.time.flow.analysis.service.PeopleRealTimeResultService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 实时人数结果表(PeopleRealTimeResult)表服务实现类
 *
 * @author CJ
 * @since 2022-02-13 10:22:40
 */
@Service
public class PeopleRealTimeResultServiceImpl extends CommonServiceImpl<PeopleRealTimeResultMapper, PeopleRealTimeResult> implements PeopleRealTimeResultService {

    @Resource
    private PeopleRealTimeFlowInService peopleRealTimeFlowInService;
    @Resource
    private PeopleRealTimeFlowOutService peopleRealTimeFlowOutService;
    @Resource
    private PeopleRealTimeNumService peopleRealTimeNumService;


    @Override
    public void saveByStatDate(String entityClassName,Date statDate) throws ClassNotFoundException {
        List<PeopleRealTimeResult> resultMapList = null;
        String packagePath = "com.inspur.spg.real.time.flow.analysis.api.entity";
        Class<?> entityClass = Class.forName(packagePath + "." + entityClassName);
        //实时人数
        if(entityClass == PeopleRealTimeNum.class){
            resultMapList = peopleRealTimeNumService.getTotalNumByStatDateGroupByPositionId(statDate);
        //实时流入
        }else if(entityClass == PeopleRealTimeFlowIn.class){
            resultMapList = peopleRealTimeFlowInService.getTotalNumByStatDateGroupByPositionId(statDate);
        //实时流出
        }else if(entityClass == PeopleRealTimeFlowOut.class){
            resultMapList = peopleRealTimeFlowOutService.getTotalNumByStatDateGroupByPositionId(statDate);
        }
        if(CollectionUtil.isEmpty(resultMapList)){
            return;
        }
        //根据实体类映射表名
        String tableName = SqlHelper.table(entityClass).getTableName();
        List<PeopleRealTimeResult> saveList = resultMapList.stream().map(item -> {
            PeopleRealTimeResult peopleRealTimeResult = JSONObject.parseObject(JSONObject.toJSONString(item),PeopleRealTimeResult.class);
            peopleRealTimeResult.setCreateTime(new Date());
            peopleRealTimeResult.setTableName(tableName);
            return peopleRealTimeResult;
        }).collect(Collectors.toList());
        fastSaveIgnoreBatch(saveList);
    }

    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfo(BasePositionDTO positionDto) {
        List<PeopleRealTimeResultChartsVO> list = baseMapper.getChartsInfoTotalNumGroupByStatDate(positionDto);
        Set<String> dateSet = list.stream().map(item -> item.getTitle()).collect(Collectors.toSet());
        List<ChartsInfo<BigDecimal>> chartsInfos = new ArrayList<>();
        for(String date : dateSet) {
            ChartsInfo<BigDecimal> chartsInfo = new ChartsInfo<>();
            Map<String,BigDecimal> valuesMap = new HashMap<>();
            for(PeopleRealTimeResultChartsVO chartsVo : list){
                if(date.equals(chartsVo.getTitle())){
                    chartsInfo.setTitle(date);
                    valuesMap.put(StringUtils.lineToHump(chartsVo.getTableName()),chartsVo.getValues());
                }
            }
            chartsInfo.setValuesMap(valuesMap);
            chartsInfos.add(chartsInfo);
        }
        //降序
        chartsInfos = chartsInfos.stream().sorted(Comparator.comparing(ChartsInfo::getTitle)).collect(Collectors.toList());
        return chartsInfos;
    }


}