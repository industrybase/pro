package com.inspur.spg.real.time.flow.analysis.config;

import com.inspur.spg.common.sharding.jdbc.config.ShardingDataSourceConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/28 18:58
 **/
@Component
@Configuration
public class DataSourceConfig {

    @Bean
    @ConfigurationProperties("sharding-jdbc.datasource.db2022")
    public ShardingDataSourceConfig db2022()
    {
        return new ShardingDataSourceConfig();
    }

}
