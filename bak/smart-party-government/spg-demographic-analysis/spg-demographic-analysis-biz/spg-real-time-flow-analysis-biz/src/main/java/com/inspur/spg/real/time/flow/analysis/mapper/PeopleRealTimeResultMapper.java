package com.inspur.spg.real.time.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.real.time.flow.analysis.api.vo.PeopleRealTimeResultChartsVO;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;

import java.util.List;

/**
 * 实时人数结果表(PeopleRealTimeResult)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-13 10:22:40
 */
@Mapper
public interface PeopleRealTimeResultMapper extends CommonMapper<PeopleRealTimeResult> {

    /**
     * @Author CJ
     * @Description 根据日期获取总数
     * @Date 2022/2/13 14:32
     * @param positionDto
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<PeopleRealTimeResultChartsVO> getChartsInfoTotalNumGroupByStatDate(BasePositionDTO positionDto);

}