package com.inspur.spg.real.time.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeActivityConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * 实时位置活动配置(PeopleRealActivityConfig)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-29 11:46:03
 */
@Mapper
public interface PeopleRealTimeActivityConfigMapper extends BaseMapper<PeopleRealTimeActivityConfig> {

}