package com.inspur.spg.real.time.flow.analysis.config;
;
import com.inspur.spg.common.sharding.jdbc.algorithm.PreciseDatabaseShardingAlgorithm;
import com.inspur.spg.common.sharding.jdbc.algorithm.PreciseTableShardingAlgorithm;
import com.inspur.spg.common.sharding.jdbc.algorithm.RangeDatabaseShardingAlgorithm;
import com.inspur.spg.common.sharding.jdbc.algorithm.RangeTableShardingAlgorithm;
import com.inspur.spg.common.sharding.jdbc.config.ShardingDataSourceConfig;
import com.inspur.spg.common.sharding.jdbc.util.DataSourceUtil;
import org.apache.shardingsphere.api.config.sharding.KeyGeneratorConfiguration;
import org.apache.shardingsphere.api.config.sharding.ShardingRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.TableRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.StandardShardingStrategyConfiguration;
import org.apache.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;

/**
 * @Author CJ
 * @Description sharding-jdbc配置分库分表
 * @Date 2022/1/28 18:56
 **/
@SpringBootConfiguration
public class ShardingConfig {

    @Resource
    private DataSourceProperties dataSourceProperties;
    @Resource
    private DataSourceConfig dataSourceConfig;

    private final String DEFAULT_DATASOURCE = "default_dataSource";

    /**
     * 逻辑表名称
     */
    private final String[] LOGICAL_TABLES = {"people_stay_time","people_real_time_num","people_real_time_flow_in","people_real_time_flow_out","traffic_events_real_time"};

    /**
     * 公共名称
     */
    private final String[] COMMON_TABLES = {"people_real_time_activity_config"};

    /**
     * 分片键
     */
    private final String DATABASE_SHARDING_COLUMN = "stat_date";

    /**
     * 逻辑库
     */
    private final String LOGICAL_DATABASE = "spg_real_time_personnel_mobility";


    @Bean
    public DataSource getShardingDataSource() throws SQLException {
        // 分片规则配置对象
        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
        List<TableRuleConfiguration> tableRuleConfigurations = getTableRuleConfigurations();
        for(TableRuleConfiguration tableRuleConfiguration : tableRuleConfigurations){
            // 规则配置
            shardingRuleConfig.getTableRuleConfigs().add(tableRuleConfiguration);
        }
        //绑定公共表
        //shardingRuleConfig.setBroadcastTables(Arrays.asList(COMMON_TABLES));
        // 数据库分片算法（精确、范围），按年分库
        shardingRuleConfig.setDefaultDatabaseShardingStrategyConfig(new StandardShardingStrategyConfiguration(DATABASE_SHARDING_COLUMN, new PreciseDatabaseShardingAlgorithm(), new RangeDatabaseShardingAlgorithm()));
        // 表分片算法（精确、范围），按月分表
        shardingRuleConfig.setDefaultTableShardingStrategyConfig(new StandardShardingStrategyConfiguration(DATABASE_SHARDING_COLUMN, new PreciseTableShardingAlgorithm(), new RangeTableShardingAlgorithm()));
        // 默认库，如果存在广播表和绑定表也可在此配置
        shardingRuleConfig.setDefaultDataSourceName(DEFAULT_DATASOURCE);
        // 开启日志打印
        final Properties properties = new Properties();
        properties.setProperty("sql.show", "true");
        return ShardingDataSourceFactory.createDataSource(createDataSourceMap(), shardingRuleConfig, properties);
    }


    private List<TableRuleConfiguration> getTableRuleConfigurations(){
        final List<TableRuleConfiguration> ruleConfigurations = new ArrayList<>();
        for(String logicalTable : LOGICAL_TABLES){
            // 暂定为两年，关于此表达式，可查看官方文档 https://shardingsphere.apache.org/document/legacy/4.x/document/cn/features/sharding/other-features/inline-expression/
            /*String inLineExpressionStr = LOGICAL_DATABASE + "_2022." + logicalTable + "_20220${1..9}" + "," + LOGICAL_DATABASE + "_2021." + logicalTable + "_20210${1..9}" + "," +
                    LOGICAL_DATABASE + "_2022." + logicalTable + "_20221${0..2}" + "," + LOGICAL_DATABASE + "_2021." + logicalTable + "_20211${0..2}";*/
            String inLineExpressionStr = LOGICAL_DATABASE + "_2022." + logicalTable + "_20220${1..9}" +  "," +
                    LOGICAL_DATABASE + "_2022." + logicalTable + "_20221${0..2}";
            final TableRuleConfiguration ruleConfiguration = new TableRuleConfiguration(logicalTable, inLineExpressionStr);
            // 设置主键生成策略
            ruleConfiguration.setKeyGeneratorConfig(getKeyGeneratorConfiguration());
            ruleConfigurations.add(ruleConfiguration);
        }
        return ruleConfigurations;
    }

    /**
     * @Author CJ
     * @Description 雪花算法生成id
     * @Date 2022/1/28 18:57
     * @param
     * @return org.apache.shardingsphere.api.config.sharding.KeyGeneratorConfiguration
     **/
    private KeyGeneratorConfiguration getKeyGeneratorConfiguration() {
        return new KeyGeneratorConfiguration("SNOWFLAKE", "id");
    }

    /**
     * @Author CJ
     * @Description 创建数据源
     * @Date 2022/1/28 18:56
     * @param
     * @return java.util.Map<java.lang.String,javax.sql.DataSource>
     **/
    private Map<String, DataSource> createDataSourceMap() {
        ShardingDataSourceConfig db2022 = dataSourceConfig.db2022();
        // key为数据源名称，后面分片算法取得就是这个，value为具体的数据源
        final HashMap<String, DataSource> shardingDataSourceMap = new HashMap<>();
        shardingDataSourceMap.put(DEFAULT_DATASOURCE, DataSourceUtil.createDataSource(dataSourceProperties.getDriverClassName(),dataSourceProperties.getUrl(),dataSourceProperties.getUsername(),dataSourceProperties.getPassword()));
        shardingDataSourceMap.put(LOGICAL_DATABASE + "_2022", DataSourceUtil.createDataSource(db2022.getDriverClassName(),db2022.getUrl(),db2022.getUsername(),db2022.getPassword()));
        return shardingDataSourceMap;
    }

}

