package com.inspur.spg.real.time.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeFlowIn;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleRealTimeResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 实时人口流入(PeopleRealFlowIn)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-28 18:15:25
 */
@Mapper
public interface PeopleRealTimeFlowInMapper extends CommonMapper<PeopleRealTimeFlowIn> {

    /**
     * @Author CJ
     * @Description 根据日期统计人数
     * @Date 2022/2/13 10:56
     * @param statDate
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<PeopleRealTimeResult> getTotalNumByStatDateGroupByPositionId(@Param("statDate")Date statDate);

}