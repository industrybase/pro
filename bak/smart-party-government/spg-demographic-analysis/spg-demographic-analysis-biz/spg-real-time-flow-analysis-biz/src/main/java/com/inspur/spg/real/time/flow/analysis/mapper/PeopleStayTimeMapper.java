package com.inspur.spg.real.time.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.real.time.flow.analysis.api.entity.PeopleStayTime;
import org.apache.ibatis.annotations.Mapper;

/**
 * 实时人流驻留时长分析(PeopleStayTime)表数据库访问层
 *
 * @author cj
 * @since 2022-01-10 16:35:30
 */
@Mapper
public interface PeopleStayTimeMapper  extends CommonMapper<PeopleStayTime> {



}