package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.StationProvinceLeave;

import java.math.BigDecimal;
import java.util.List;

/**
 * 场站旅客洞察-省外去向地(StationProvinceLeave)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
@Mapper
public interface StationProvinceLeaveMapper extends CommonMapper<StationProvinceLeave> {

    /**
     * @Author CJ
     * @Description 统计前十省份人数
     * @Date 2022/2/11 15:21
     * @param positionDto
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<ChartsInfo<BigDecimal>> getChartsInfoGroupByAreaName(BasePositionDTO positionDto);


    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/11 16:17
     * @param positionDto
     * @return int
     **/
    int getTotalNum(BasePositionDTO positionDto);

}