package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.ApiFieldFormatConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * API接口字段转换配置(ApiFieldFormatConfig)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 16:15:46
 */
@Mapper
public interface ApiFieldFormatConfigMapper extends BaseMapper<ApiFieldFormatConfig> {

}