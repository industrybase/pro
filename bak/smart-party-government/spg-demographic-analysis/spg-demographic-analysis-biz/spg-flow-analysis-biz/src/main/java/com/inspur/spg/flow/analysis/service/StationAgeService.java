package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.StationAge;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 场站客流画像-年龄(StationAge)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface StationAgeService extends ICommonService<StationAge> {

}