package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.MigrantHistoricalTrendDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantHistoricalTrend;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigDecimal;
import java.util.List;

/**
 * 人口迁徙-历史迁徙趋势(MigrantHistoricalTrend)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface MigrantHistoricalTrendMapper extends CommonMapper<MigrantHistoricalTrend> {

    /**
     * 查询统计数据
     * @param migrantHistoricalTrendDto
     * @return
     */
    List<ChartsInfo<BigDecimal>> getChartsInfo(MigrantHistoricalTrendDTO migrantHistoricalTrendDto);

}