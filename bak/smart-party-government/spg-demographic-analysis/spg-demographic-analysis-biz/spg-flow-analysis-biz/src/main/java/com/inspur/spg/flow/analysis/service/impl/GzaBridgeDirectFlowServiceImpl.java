package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgeDirectFlow;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeDirectFlowMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeDirectFlowService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流洞察-客流方向(GzaBridgeDirectFlow)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Service
public class GzaBridgeDirectFlowServiceImpl extends BasePositionServiceImpl<GzaBridgeDirectFlowMapper, GzaBridgeDirectFlow> implements GzaBridgeDirectFlowService {


    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeDirectFlow> list) {
        for(GzaBridgeDirectFlow entity : list){
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}