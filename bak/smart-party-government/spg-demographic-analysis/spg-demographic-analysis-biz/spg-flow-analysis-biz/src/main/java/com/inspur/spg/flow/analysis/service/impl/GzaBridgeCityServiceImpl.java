package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeBelong;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeCity;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeCityMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeCityService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流洞察-归属地市分布(GzaBridgeCity)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Service
public class GzaBridgeCityServiceImpl extends BasePositionServiceImpl<GzaBridgeCityMapper, GzaBridgeCity> implements GzaBridgeCityService {

    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeCity> list) {
        for (GzaBridgeCity entity : list) {
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}