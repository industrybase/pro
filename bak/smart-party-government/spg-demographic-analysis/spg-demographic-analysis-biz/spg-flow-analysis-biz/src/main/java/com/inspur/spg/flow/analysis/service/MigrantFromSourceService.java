package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromSource;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 人口迁徙-迁入来源类型(MigrantFromSource)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface MigrantFromSourceService extends ICommonService<MigrantFromSource> {

}