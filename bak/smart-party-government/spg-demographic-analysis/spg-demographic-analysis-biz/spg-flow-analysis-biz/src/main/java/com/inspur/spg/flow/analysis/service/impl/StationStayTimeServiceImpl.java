package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.StationStayTime;
import com.inspur.spg.flow.analysis.mapper.StationStayTimeMapper;
import com.inspur.spg.flow.analysis.service.PositionConfigService;
import com.inspur.spg.flow.analysis.service.StationStayTimeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站旅客-驻留时长(StationStayTime)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class StationStayTimeServiceImpl extends BasePositionServiceImpl<StationStayTimeMapper, StationStayTime> implements StationStayTimeService {

    @Resource
    private PositionConfigService positionConfigService;

    @Override
    public boolean fastSaveIgnoreBatch(List<StationStayTime> list) {
        for (StationStayTime entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}