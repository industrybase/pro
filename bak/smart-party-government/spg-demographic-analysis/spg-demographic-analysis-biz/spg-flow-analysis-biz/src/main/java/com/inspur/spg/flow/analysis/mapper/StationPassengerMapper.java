package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationPassenger;
import com.inspur.spg.flow.analysis.api.vo.StationPassengerVO;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigDecimal;
import java.util.List;

/**
 * 场站旅客洞察-旅客量(StationPassenger)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface StationPassengerMapper extends CommonMapper<StationPassenger> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/11 16:23
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    StationPassengerVO getTotalNum(BasePositionDTO positionDto);

    /**
     * @Author CJ
     * @Description 根据日期分组统计趋势
     * @Date 2022/3/10 16:23
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    List<ChartsInfo<BigDecimal>> getChartsInfoGroupByStatDate(BasePositionDTO positionDTO);
}