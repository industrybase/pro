package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgeBelong;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeBelongMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeBelongService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流洞察-归属地类型(GzaBridgeBelong)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:59:24
 */
@Service
public class GzaBridgeBelongServiceImpl extends BasePositionServiceImpl<GzaBridgeBelongMapper, GzaBridgeBelong> implements GzaBridgeBelongService {

    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeBelong> list) {
        for (GzaBridgeBelong entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}