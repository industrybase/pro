package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantCityPathChannel;

import com.inspur.spg.flow.analysis.api.vo.MigrantCityPathChannelRelationRefVO;
import com.inspur.spg.flow.analysis.api.vo.MigrantRankVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 人口迁徙-省内路线及渠道迁徙(MigrantCityPathChannel)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:41
 */
@Mapper
public interface MigrantCityPathChannelMapper extends CommonMapper<MigrantCityPathChannel> {

    /**
     * 获取城市迁徙来源总数
     * @param statDate
     * @return
     */
    List<MigrantRankVO> getTotalNumListGroupByFromCity(@Param("statDate") Date statDate, @Param("area") String area, @Param("limit") Integer limit);

    /**
     * 获取城市迁徙去向总数
     * @param statDate
     * @return
     */
    List<MigrantRankVO> getTotalNumListGroupByToCity(@Param("statDate") Date statDate, @Param("area") String area, @Param("limit") Integer limit);

    /**
     * 根据日期获取迁徙关联渠道数据(万人)
     * @param statDate
     * @return
     */
    List<MigrantCityPathChannelRelationRefVO> getRelationRefVoList(BaseMigrantDTO migrantDto);
}