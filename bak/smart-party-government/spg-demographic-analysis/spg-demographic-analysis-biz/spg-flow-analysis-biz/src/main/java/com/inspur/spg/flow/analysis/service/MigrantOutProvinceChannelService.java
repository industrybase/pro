package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutProvinceChannel;
import com.inspur.spg.common.mybatis.service.ICommonService;

import java.math.BigDecimal;
import java.util.List;

/**
 * 人口迁徙-省际迁出去向及渠道(MigrantProvinceChannel)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface MigrantOutProvinceChannelService extends ICommonService<MigrantOutProvinceChannel> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/10 12:46
     * @param
     * @return java.lang.Integer
     **/
    int getTotalNum(BaseMigrantDTO migrantDto);

    /**
     * @Author CJ
     * @Description 统计图表
     * @Date 2022/2/10 14:43
     * @param migrantDto
     * @return java.util.List<com.inspur.spg.common.core.entity.ChartsInfo>
     **/
    List<ChartsInfo<BigDecimal>> getChartsInfo(BaseMigrantDTO migrantDto);

}