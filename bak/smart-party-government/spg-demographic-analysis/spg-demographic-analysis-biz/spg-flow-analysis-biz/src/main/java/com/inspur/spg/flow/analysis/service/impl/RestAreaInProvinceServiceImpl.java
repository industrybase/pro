package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.RestAreaForeign;
import com.inspur.spg.flow.analysis.mapper.RestAreaInProvinceMapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaInProvince;
import com.inspur.spg.flow.analysis.service.RestAreaInProvinceService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务区客流画像归属-省内(RestAreaInProvince)表服务实现类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Service
public class RestAreaInProvinceServiceImpl extends BasePositionServiceImpl<RestAreaInProvinceMapper, RestAreaInProvince> implements RestAreaInProvinceService {

    @Override
    public boolean fastSaveIgnoreBatch(List<RestAreaInProvince> list) {
        for (RestAreaInProvince entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}