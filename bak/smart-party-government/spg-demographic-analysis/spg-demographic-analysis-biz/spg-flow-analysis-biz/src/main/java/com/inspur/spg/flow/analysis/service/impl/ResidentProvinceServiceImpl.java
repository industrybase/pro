package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.constant.enums.GenderEnum;
import com.inspur.spg.flow.analysis.api.entity.ResidentProvince;
import com.inspur.spg.flow.analysis.mapper.ResidentProvinceMapper;
import com.inspur.spg.flow.analysis.service.ResidentProvinceService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 人口洞察-省常驻人口年龄(ResidentProvince)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class ResidentProvinceServiceImpl extends CommonServiceImpl<ResidentProvinceMapper, ResidentProvince> implements ResidentProvinceService {

    @Override
    public boolean fastSaveIgnoreBatch(List<ResidentProvince> list) {
        List<ResidentProvince> saveList = new ArrayList<>();
        for(ResidentProvince item : list){
            //男性人口
            ResidentProvince male = new ResidentProvince();
            BeanUtils.copyProperties(item,male);
            male.setGender(GenderEnum.male.getCode());
            Integer maleNum = (int)Math.round(item.getNum().doubleValue() * item.getProportion().doubleValue());
            male.setNum(maleNum);
            saveList.add(male);

            //女性人口
            ResidentProvince female = new ResidentProvince();
            BeanUtils.copyProperties(item,female);
            female.setGender(GenderEnum.female.getCode());
            Double proportion = 1d - male.getProportion().doubleValue();
            female.setProportion(new BigDecimal(proportion));
            female.setNum(item.getNum().intValue() - maleNum.intValue());
            saveList.add(female);
            //总人口
            item.setProportion(BigDecimal.valueOf(1));
            saveList.add(item);
        }
        return super.fastSaveIgnoreBatch(saveList);
    }

}