package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantRefChannelCity;

/**
 * 关联表省内(MigrantRefChannelCity)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-10 11:30:00
 */
@Mapper
public interface MigrantRefChannelCityMapper extends CommonMapper<MigrantRefChannelCity> {

}