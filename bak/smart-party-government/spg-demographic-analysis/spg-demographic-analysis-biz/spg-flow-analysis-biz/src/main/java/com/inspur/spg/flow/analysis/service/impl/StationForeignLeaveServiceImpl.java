package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationForeignLeave;
import com.inspur.spg.flow.analysis.mapper.StationForeignLeaveMapper;
import com.inspur.spg.flow.analysis.service.StationForeignLeaveService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站旅客洞察-国际去向(StationForeignLeave)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class StationForeignLeaveServiceImpl extends BasePositionServiceImpl<StationForeignLeaveMapper, StationForeignLeave> implements StationForeignLeaveService {

    @Override
    public boolean fastSaveIgnoreBatch(List<StationForeignLeave> list) {
        for (StationForeignLeave entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

    @Override
    public int getTotalNum(BasePositionDTO positionDto) {
        return baseMapper.getTotalNum(positionDto);
    }
}