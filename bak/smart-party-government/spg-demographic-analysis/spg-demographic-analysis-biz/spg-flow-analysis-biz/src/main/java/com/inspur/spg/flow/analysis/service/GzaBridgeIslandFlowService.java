package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeIslandFlow;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * (GzaBridgeIslandFlow)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
public interface GzaBridgeIslandFlowService extends ICommonService<GzaBridgeIslandFlow> {

}