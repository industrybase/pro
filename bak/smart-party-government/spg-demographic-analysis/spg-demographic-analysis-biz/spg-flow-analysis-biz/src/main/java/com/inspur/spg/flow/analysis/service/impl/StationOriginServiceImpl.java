package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.vo.PopulationProportionStatsVO;
import com.inspur.spg.flow.analysis.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/11 18:19
 **/
@Service
public class StationOriginServiceImpl implements StationOriginService {

    @Resource
    private StationForeignOriginService stationForeignOriginService;
    @Resource
    private StationProvinceOriginService stationProvinceOriginService;

    @Override
    public PopulationProportionStatsVO getPopulationStats(BasePositionDTO positionDto) {
        //省外总数
        int provinceTotalNum = stationProvinceOriginService.getTotalNum(positionDto);
        //境外总数
        int foreignTotalNum = stationForeignOriginService.getTotalNum(positionDto);
        int totalNum = foreignTotalNum + provinceTotalNum;
        //计算占比
        BigDecimal provinceProportion = totalNum == 0 ? BigDecimal.valueOf(0) : BigDecimal.valueOf((double) provinceTotalNum/totalNum * 100).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal foreignProportion = BigDecimal.valueOf(100d - provinceProportion.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
        //数据统计
        return PopulationProportionStatsVO.builder()
                .chartsInfos(stationProvinceOriginService.getChartsInfo(positionDto))
                .foreignProportion(foreignProportion)
                .provinceProportion(provinceProportion).build();
    }
}
