package com.inspur.spg.flow.analysis.controller;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.dto.AreaProvinceFlowDTO;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.service.AreaProvinceFlowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author CJ
 * @Date 2022/3/4 10:23
 * @Description: 省客流实时数据模块
 */
@RestController
@RequestMapping("/areaProvinceFlow")
@Api(value = "areaProvinceFlow", tags = "省客流实时数据模块")
public class AreaProvinceFlowController {

    @Resource
    private AreaProvinceFlowService areaProvinceFlowService;

    /**
     * 获取统计数据
     * @param areaProvinceFlowDTO
     * @return
     */
    @GetMapping("/getChartsInfo")
    @ApiOperation(value = "获取统计数据", notes = "获取统计数据")
    public R<List<ChartsInfo<BigDecimal>>> getChartsInfo(AreaProvinceFlowDTO areaProvinceFlowDTO){
        String area = areaProvinceFlowDTO.getArea();
        if(StringUtils.isEmpty(area) || (StringUtils.isNotBlank(area) && "广东".equals(area))){
            areaProvinceFlowDTO.setArea("广东省");
        }
        return R.ok(areaProvinceFlowService.getChartsInfo(areaProvinceFlowDTO));
    }

}
