package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.inspur.spg.common.core.constant.enums.CityShortEnum;
import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.CalculationUtils;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.api.dto.MigrantHistoricalTrendDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantHistoricalTrend;
import com.inspur.spg.flow.analysis.api.vo.MigrantHistoricalTrendVO;
import com.inspur.spg.flow.analysis.mapper.MigrantHistoricalTrendMapper;
import com.inspur.spg.flow.analysis.service.MigrantHistoricalTrendService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 人口迁徙-历史迁徙趋势(MigrantHistoricalTrend)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class MigrantHistoricalTrendServiceImpl extends CommonServiceImpl<MigrantHistoricalTrendMapper, MigrantHistoricalTrend> implements MigrantHistoricalTrendService {

    @Override
    public boolean fastSaveIgnoreBatch(List<MigrantHistoricalTrend> list) {
        list = list.stream().map(item -> {
            if(item.getProvinceCityShort().equals("全省")){
                item.setProvinceCityShort("GD");
                item.setProvinceCity("全省");
            }else {
                item.setProvinceCity(CityShortEnum.valueOf(item.getProvinceCityShort()).getName());
            }
            return item;
        }).collect(Collectors.toList());
        return super.replaceIntoBatchAllColumn(list);
    }

    @Override
    public MigrantHistoricalTrendVO getMigrantInfo(MigrantHistoricalTrendDTO migrantDto) {
        MigrantHistoricalTrend migrantHistoricalTrend = getOneByDto(migrantDto);
        if(migrantHistoricalTrend == null){
            return null;
        }
        MigrantHistoricalTrendVO migrantHistoricalTrendVo = new MigrantHistoricalTrendVO();
        BeanUtils.copyProperties(migrantHistoricalTrend,migrantHistoricalTrendVo);
        return calculatedProportion(migrantHistoricalTrend,migrantHistoricalTrendVo);
    }

    @Override
    public MigrantHistoricalTrend getOneByDto(MigrantHistoricalTrendDTO migrantDto){
        LambdaQueryWrapper<MigrantHistoricalTrend> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper.eq(MigrantHistoricalTrend::getProvinceCity,migrantDto.getArea()).eq(MigrantHistoricalTrend::getStatDate,migrantDto.getStatDate());
        return getOne(lambdaQueryWrapper);
    }

    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfo(MigrantHistoricalTrendDTO migrantDto) {
        return baseMapper.getChartsInfo(migrantDto);
    }

    /**
     * 计算单个占比
     * @param migrantHistoricalTrendVo
     * @return
     */
    private MigrantHistoricalTrendVO calculatedProportion(MigrantHistoricalTrend migrantHistoricalTrend, MigrantHistoricalTrendVO migrantHistoricalTrendVo){
        Date statDate = migrantHistoricalTrend.getStatDate();
        MigrantHistoricalTrendDTO migrantDto = new MigrantHistoricalTrendDTO();
        migrantDto.setArea(migrantHistoricalTrend.getProvinceCity());
        //计算上月
        Date lastMonDate = DateUtil.addMonths(statDate,-1);
        migrantDto.setStatDate(lastMonDate);
        MigrantHistoricalTrend lastMonMigrantHistoricalTrend = getOneByDto(migrantDto);
        if(lastMonMigrantHistoricalTrend == null){
            migrantHistoricalTrendVo.setMom(BigDecimal.valueOf(100));
        }else{
            migrantHistoricalTrendVo.setMom(CalculationUtils.growthRate(migrantHistoricalTrend.getPopulationTotal().doubleValue(),lastMonMigrantHistoricalTrend.getPopulationTotal().doubleValue()));
        }

        //计算上年
        Date lastYearDate = DateUtil.addYears(statDate,-1);
        migrantDto.setStatDate(lastYearDate);
        MigrantHistoricalTrend lastYearMigrantHistoricalTrend = getOneByDto(migrantDto);
        if(lastYearMigrantHistoricalTrend == null){
            migrantHistoricalTrendVo.setYoy(BigDecimal.valueOf(100));
        }else{
            migrantHistoricalTrendVo.setYoy(CalculationUtils.growthRate(migrantHistoricalTrend.getPopulationTotal().doubleValue(),lastYearMigrantHistoricalTrend.getPopulationTotal().doubleValue()));
        }
        return migrantHistoricalTrendVo;
    }
}