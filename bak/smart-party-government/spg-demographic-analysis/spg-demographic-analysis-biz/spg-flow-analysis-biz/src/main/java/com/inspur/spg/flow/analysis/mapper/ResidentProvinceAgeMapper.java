package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.ResidentProvinceAge;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人口洞察-地市常驻人口(ResidentProvinceAge)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface ResidentProvinceAgeMapper extends CommonMapper<ResidentProvinceAge> {

}