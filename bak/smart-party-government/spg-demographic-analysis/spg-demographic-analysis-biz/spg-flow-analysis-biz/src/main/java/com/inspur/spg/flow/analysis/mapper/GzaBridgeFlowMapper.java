package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeFlow;
import org.apache.ibatis.annotations.Mapper;

/**
 * 港珠澳大桥客流趋势-大桥客流量(GzaBridgeFlow)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Mapper
public interface GzaBridgeFlowMapper extends CommonMapper<GzaBridgeFlow> {

}