package com.inspur.spg.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.vo.PositionTypeVO;
import com.inspur.spg.flow.analysis.service.PositionTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author CJ
 * @Date 2022/2/21 20:31 （可以根据需要修改）
 * @Description:
 */

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/21 20:31
 **/
@RestController
@RequestMapping("/positionType")
@Api(value = "positionType", tags = "枢纽类别模块")
public class PositionTypeController {

    @Resource
    private PositionTypeService positionTypeService;

    @GetMapping("/getAll")
    @ApiOperation(value = "查询所有数据", notes = "查询所有数据")
    public R<List<PositionTypeVO>> getAll(){
        return R.ok(positionTypeService.getAll());
    }

}
