package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationForeignOrigin;

/**
 * 场站旅客洞察-国际来源地(StationForeignOrigin)表服务接口
 *
 * @author cj
 * @since 2022-01-31 17:49:55
 */
public interface StationForeignOriginService extends ICommonService<StationForeignOrigin> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/11 16:17
     * @param positionDto
     * @return int
     **/
    int getTotalNum(BasePositionDTO positionDto);

}