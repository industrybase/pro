package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.mapper.MigrantOutChannelMapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutChannel;
import com.inspur.spg.flow.analysis.service.MigrantOutChannelService;
import org.springframework.stereotype.Service;

/**
 * 迁出渠道分布(MigrantOutChannel)表服务实现类
 *
 * @author CJ
 * @since 2022-02-10 11:28:01
 */
@Service
public class MigrantOutChannelServiceImpl extends CommonServiceImpl<MigrantOutChannelMapper, MigrantOutChannel> implements MigrantOutChannelService {

}