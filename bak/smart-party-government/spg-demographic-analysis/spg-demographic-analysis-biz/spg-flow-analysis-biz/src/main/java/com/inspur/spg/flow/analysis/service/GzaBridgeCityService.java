package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeCity;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 港珠澳大桥客流洞察-归属地市分布(GzaBridgeCity)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
public interface GzaBridgeCityService extends ICommonService<GzaBridgeCity> {

}