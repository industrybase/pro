package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutForeignChannel;

/**
 * 人口迁徙-境外迁出去向及渠道(MigrantOutForeignChannel)表服务接口
 *
 * @author cj
 * @since 2022-02-04 15:07:09
 */
public interface MigrantOutForeignChannelService extends ICommonService<MigrantOutForeignChannel> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/10 12:46
     * @param migrantDto
     * @return java.lang.Integer
     **/
    int getTotalNum(BaseMigrantDTO migrantDto);

}