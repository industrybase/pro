package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgeProvince;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeProvinceMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeProvinceService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流洞察-归属省份分布(GzaBridgeProvince)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Service
public class GzaBridgeProvinceServiceImpl extends BasePositionServiceImpl<GzaBridgeProvinceMapper, GzaBridgeProvince> implements GzaBridgeProvinceService {


    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeProvince> list) {
        for (GzaBridgeProvince entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}