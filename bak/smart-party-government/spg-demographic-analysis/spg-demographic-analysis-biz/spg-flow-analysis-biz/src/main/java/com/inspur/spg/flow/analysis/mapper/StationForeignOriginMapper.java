package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationForeignOrigin;
import org.apache.ibatis.annotations.Mapper;

/**
 * 场站旅客洞察-国际来源地(StationForeignOrigin)表数据库访问层
 *
 * @author cj
 * @since 2022-01-31 17:50:31
 */
@Mapper
public interface StationForeignOriginMapper extends CommonMapper<StationForeignOrigin> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/11 16:17
     * @return int
     **/
    int getTotalNum(BasePositionDTO positionDto);

}