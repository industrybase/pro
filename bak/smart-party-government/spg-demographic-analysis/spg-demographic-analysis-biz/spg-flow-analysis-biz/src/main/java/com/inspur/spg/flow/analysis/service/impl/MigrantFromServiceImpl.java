package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.vo.PopulationProportionStatsVO;
import com.inspur.spg.flow.analysis.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/15 12:52
 **/
@Service
public class MigrantFromServiceImpl implements MigrantFromService {

    @Resource
    private MigrantFromProvinceService migrantFromProvinceService;
    @Resource
    private MigrantFromForeignService migrantFromForeignService;

    @Override
    public PopulationProportionStatsVO getPopulationStats(BaseMigrantDTO migrantDto) {
        //境外总数
        int foreignTotalNum = migrantFromProvinceService.getTotalNum(migrantDto);
        //省外总数
        int provinceTotalNum = migrantFromForeignService.getTotalNum(migrantDto);
        int totalNum = foreignTotalNum + provinceTotalNum;
        //计算占比
        BigDecimal provinceProportion = totalNum == 0 ? BigDecimal.valueOf(0) : BigDecimal.valueOf((double) provinceTotalNum/totalNum * 100).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal foreignProportion = BigDecimal.valueOf(100d - provinceProportion.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
        return PopulationProportionStatsVO.builder()
                .chartsInfos(migrantFromProvinceService.getChartsInfo(migrantDto))
                .foreignProportion(foreignProportion)
                .provinceProportion(provinceProportion).build();
    }
}
