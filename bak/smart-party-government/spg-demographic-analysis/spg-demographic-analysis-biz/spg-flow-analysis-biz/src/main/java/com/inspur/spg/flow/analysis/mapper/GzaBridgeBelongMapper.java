package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeBelong;
import org.apache.ibatis.annotations.Mapper;

/**
 * 港珠澳大桥客流洞察-归属地类型(GzaBridgeBelong)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 09:59:23
 */
@Mapper
public interface GzaBridgeBelongMapper extends CommonMapper<GzaBridgeBelong> {

}