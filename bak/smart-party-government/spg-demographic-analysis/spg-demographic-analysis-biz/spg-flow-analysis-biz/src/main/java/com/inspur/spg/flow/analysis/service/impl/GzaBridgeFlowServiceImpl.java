package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgeFlow;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeFlowMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeFlowService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流趋势-大桥客流量(GzaBridgeFlow)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Service
public class GzaBridgeFlowServiceImpl extends BasePositionServiceImpl<GzaBridgeFlowMapper, GzaBridgeFlow> implements GzaBridgeFlowService {

    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeFlow> list) {
        for (GzaBridgeFlow entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}