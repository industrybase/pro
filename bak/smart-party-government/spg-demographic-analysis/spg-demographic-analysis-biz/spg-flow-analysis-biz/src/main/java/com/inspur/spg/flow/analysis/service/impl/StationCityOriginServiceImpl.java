package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.StationProvinceOrigin;
import com.inspur.spg.flow.analysis.mapper.StationCityOriginMapper;
import com.inspur.spg.flow.analysis.api.entity.StationCityOrigin;
import com.inspur.spg.flow.analysis.service.StationCityOriginService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站旅客洞察-省内来源(StationCityOrigin)表服务实现类
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
@Service
public class StationCityOriginServiceImpl extends BasePositionServiceImpl<StationCityOriginMapper, StationCityOrigin> implements StationCityOriginService {

    @Override
    public boolean fastSaveIgnoreBatch(List<StationCityOrigin> list) {
        for (StationCityOrigin entity : list) {
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}