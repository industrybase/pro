package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.ApiConfig;

/**
 * API接口配置(ApiDbTableConfig)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 16:15:45
 */
public interface ApiConfigService extends IService<ApiConfig> {

    ApiConfig getByApiName(String apiName);
}