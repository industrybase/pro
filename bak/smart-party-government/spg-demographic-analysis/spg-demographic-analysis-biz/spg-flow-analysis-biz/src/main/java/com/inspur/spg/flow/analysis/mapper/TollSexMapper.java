package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.TollSex;
import org.apache.ibatis.annotations.Mapper;

/**
 * 收费站热力客流画像-性别(TollSex)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:52:43
 */
@Mapper
public interface TollSexMapper extends CommonMapper<TollSex> {

}