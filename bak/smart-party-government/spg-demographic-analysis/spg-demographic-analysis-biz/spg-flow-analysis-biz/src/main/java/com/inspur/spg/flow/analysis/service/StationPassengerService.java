package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationPassenger;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.vo.StationPassengerVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * 场站旅客洞察-旅客量(StationPassenger)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface StationPassengerService extends ICommonService<StationPassenger> {

    /**
     * @Author CJ
     * @Description 查询数据
     * @Date 2022/2/11 10:55
     * @param positionDto
     * @return com.inspur.spg.flow.analysis.api.entity.StationPassenger
     **/
    StationPassengerVO getPassengerNum(BasePositionDTO positionDto);

    /**
     * @Author CJ
     * @Description 根据日期分组统计趋势
     * @Date 2022/3/10 16:23
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    List<ChartsInfo<BigDecimal>> getChartsInfoGroupByStatDate(BasePositionDTO positionDTO);

}