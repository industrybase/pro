package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaStayTime;

/**
 * 服务区客流画像驻留时长(RestAreaStayTime)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Mapper
public interface RestAreaStayTimeMapper extends CommonMapper<RestAreaStayTime> {

}