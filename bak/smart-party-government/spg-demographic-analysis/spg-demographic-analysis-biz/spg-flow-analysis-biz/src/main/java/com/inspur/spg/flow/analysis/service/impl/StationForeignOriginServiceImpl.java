package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.mapper.StationForeignOriginMapper;
import com.inspur.spg.flow.analysis.api.entity.StationForeignOrigin;
import com.inspur.spg.flow.analysis.service.StationForeignOriginService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站旅客洞察-国际来源地(StationForeignOrigin)表服务实现类
 *
 * @author cj
 * @since 2022-01-31 17:49:55
 */
@Service
public class StationForeignOriginServiceImpl extends BasePositionServiceImpl<StationForeignOriginMapper, StationForeignOrigin> implements StationForeignOriginService {

    @Override
    public boolean fastSaveIgnoreBatch(List<StationForeignOrigin> list) {
        for (StationForeignOrigin entity : list) {
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

    @Override
    public int getTotalNum(BasePositionDTO positionDto) {
        return baseMapper.getTotalNum(positionDto);
    }
}