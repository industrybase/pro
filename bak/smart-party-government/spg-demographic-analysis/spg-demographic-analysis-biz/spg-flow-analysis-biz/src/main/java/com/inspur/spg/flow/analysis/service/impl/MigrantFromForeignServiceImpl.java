package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromForeign;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromProvince;
import com.inspur.spg.flow.analysis.mapper.MigrantFromForeignMapper;
import com.inspur.spg.flow.analysis.service.MigrantFromForeignService;
import com.inspur.spg.flow.analysis.service.MigrantFromProvinceService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 人口迁徙-境外迁入来源及渠道(MigrantFromForgein)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class MigrantFromForeignServiceImpl extends CommonServiceImpl<MigrantFromForeignMapper, MigrantFromForeign> implements MigrantFromForeignService {

    @Resource
    private MigrantFromProvinceService migrantFromProvinceService;

    @Override
    public int getTotalNum(BaseMigrantDTO migrantDto) {
        return baseMapper.getTotalNum(migrantDto);
    }

    @Override
    public boolean fastSaveIgnoreBatch(List<MigrantFromForeign> list) {
        List<MigrantFromProvince> migrantFromProvinceList = new ArrayList<>();
        List<MigrantFromForeign> migrantFromForeignList = new ArrayList<>();
        for(MigrantFromForeign foreignChannel : list){
            /*String inArea = foreignChannel.getInArea();
            if(
                    "中国澳门".equals(inArea) ||
                            "中国台湾".equals(inArea) ||
                            "中国香港".equals(inArea)){
                inArea = inArea.replace("中国","");
                MigrantFromProvince migrantFromProvince = new MigrantFromProvince();
                BeanUtil.copyProperties(foreignChannel,migrantFromProvince);
                migrantFromProvince.setInProvince(inArea);
                migrantFromProvinceList.add(migrantFromProvince);
            }else{
                migrantFromForeignList.add(foreignChannel);
            }*/
            migrantFromForeignList.add(foreignChannel);
        }
        migrantFromProvinceService.fastSaveIgnoreBatch(migrantFromProvinceList);
        return super.fastSaveIgnoreBatch(migrantFromForeignList);
    }


}