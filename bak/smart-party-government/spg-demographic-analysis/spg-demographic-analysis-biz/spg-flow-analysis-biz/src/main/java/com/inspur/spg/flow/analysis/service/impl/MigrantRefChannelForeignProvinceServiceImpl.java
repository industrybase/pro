package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.mapper.MigrantRefChannelForeignProvinceMapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantRefChannelForeignProvince;
import com.inspur.spg.flow.analysis.service.MigrantRefChannelForeignProvinceService;
import org.springframework.stereotype.Service;

/**
 * 关联表迁入迁出(包含境外,省外)(MigrantRefChannelForeignProvince)表服务实现类
 *
 * @author CJ
 * @since 2022-02-10 11:30:13
 */
@Service
public class MigrantRefChannelForeignProvinceServiceImpl extends CommonServiceImpl<MigrantRefChannelForeignProvinceMapper, MigrantRefChannelForeignProvince> implements MigrantRefChannelForeignProvinceService {

}