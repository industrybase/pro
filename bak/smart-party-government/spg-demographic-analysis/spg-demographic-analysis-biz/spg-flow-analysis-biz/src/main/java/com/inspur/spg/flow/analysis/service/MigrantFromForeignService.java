package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromForeign;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 人口迁徙-境外迁入来源及渠道(MigrantFromForgein)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:45
 */
public interface MigrantFromForeignService extends ICommonService<MigrantFromForeign> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/10 14:55
     * @param
     * @return int
     **/
    int getTotalNum(BaseMigrantDTO migrantDto);

}