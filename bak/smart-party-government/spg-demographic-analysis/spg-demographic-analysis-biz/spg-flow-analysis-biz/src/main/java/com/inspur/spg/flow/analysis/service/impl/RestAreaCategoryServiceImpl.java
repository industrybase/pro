package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.RestAreaAge;
import com.inspur.spg.flow.analysis.mapper.RestAreaCategoryMapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaCategory;
import com.inspur.spg.flow.analysis.service.RestAreaCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务区客流画像-归属类别(RestAreaCategory)表服务实现类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Service
public class RestAreaCategoryServiceImpl extends BasePositionServiceImpl<RestAreaCategoryMapper, RestAreaCategory> implements RestAreaCategoryService {

    @Override
    public boolean fastSaveIgnoreBatch(List<RestAreaCategory> list) {
        for (RestAreaCategory entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}