package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.TollDayFlow;
import com.inspur.spg.flow.analysis.mapper.TollDayFlowMapper;
import com.inspur.spg.flow.analysis.service.TollDayFlowService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 收费站每日客流(TollDayFlow)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:52:43
 */
@Service
public class TollDayFlowServiceImpl extends BasePositionServiceImpl<TollDayFlowMapper, TollDayFlow> implements TollDayFlowService {

    @Override
    public boolean fastSaveIgnoreBatch(List<TollDayFlow> list) {
        for (TollDayFlow entity : list) {
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }


}