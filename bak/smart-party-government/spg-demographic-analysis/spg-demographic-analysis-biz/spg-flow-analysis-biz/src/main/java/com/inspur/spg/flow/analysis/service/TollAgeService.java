package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.TollAge;

public interface TollAgeService  extends ICommonService<TollAge> {
}
