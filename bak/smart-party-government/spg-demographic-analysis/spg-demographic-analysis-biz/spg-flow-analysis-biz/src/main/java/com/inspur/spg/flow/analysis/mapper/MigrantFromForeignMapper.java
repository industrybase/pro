package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromForeign;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人口迁徙-境外迁入来源及渠道(MigrantFromForgein)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:43
 */
@Mapper
public interface MigrantFromForeignMapper extends CommonMapper<MigrantFromForeign> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/10 12:46
     * @param migrantDto
     * @return java.lang.Integer
     **/
    int getTotalNum(BaseMigrantDTO migrantDto);

}