package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgePortFlow;
import com.inspur.spg.flow.analysis.mapper.GzaBridgePortFlowMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgePortFlowService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流趋势-口岸客流量(GzaBridgePortFlow)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Service
public class GzaBridgePortFlowServiceImpl extends BasePositionServiceImpl<GzaBridgePortFlowMapper, GzaBridgePortFlow> implements GzaBridgePortFlowService {


    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgePortFlow> list) {
        for (GzaBridgePortFlow entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}