package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.constant.enums.ChannelTypeEnum;
import com.inspur.spg.common.core.constant.enums.CityShortEnum;
import com.inspur.spg.common.core.util.CalculationUtils;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantCityPathChannel;
import com.inspur.spg.flow.analysis.api.vo.MigrantCityPathAllChannelVO;
import com.inspur.spg.flow.analysis.api.vo.MigrantCityPathChannelRelationRefVO;
import com.inspur.spg.flow.analysis.api.vo.MigrantCityRankVO;
import com.inspur.spg.flow.analysis.api.vo.MigrantRankVO;
import com.inspur.spg.flow.analysis.mapper.MigrantCityPathChannelMapper;
import com.inspur.spg.flow.analysis.service.MigrantCityPathChannelService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 人口迁徙-省内路线及渠道迁徙(MigrantCityPathChannel)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:42
 */
@Service
public class MigrantCityPathChannelServiceImpl extends CommonServiceImpl<MigrantCityPathChannelMapper, MigrantCityPathChannel> implements MigrantCityPathChannelService {

    @Override
    public boolean fastSaveIgnoreBatch(List<MigrantCityPathChannel> list) {
        list = list.stream().map(item -> {
            item.setFromCity(CityShortEnum.valueOf(item.getFromCityShort()).getName());
            item.setToCity(CityShortEnum.valueOf(item.getToCityShort()).getName());
            return item;
        }).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

    @Override
    public MigrantCityRankVO getRank(BaseMigrantDTO migrantDto) {
        //迁入
        List<MigrantRankVO> originList = baseMapper.getTotalNumListGroupByFromCity(migrantDto.getStatDate(),migrantDto.getArea(),migrantDto.getLimit());
        //迁出
        List<MigrantRankVO> leaveList = baseMapper.getTotalNumListGroupByToCity(migrantDto.getStatDate(),migrantDto.getArea(),migrantDto.getLimit());
        //计算增长率
        migrantDto.setLimit(null);
        getOriginGrowthRate(migrantDto,originList);
        getLeaveGrowthRate(migrantDto,leaveList);
        return MigrantCityRankVO.builder().originList(originList).leaveList(leaveList).build();
    }

    @Override
    public List<MigrantCityPathAllChannelVO> getAllChannelList(BaseMigrantDTO migrantDto) {
        List<MigrantCityPathChannelRelationRefVO> refVos = baseMapper.getRelationRefVoList(migrantDto);
        List<MigrantCityPathAllChannelVO> allChannelVos = new ArrayList<>();
        Set<String> routes = refVos.stream().map(item -> item.getRoute()).collect(Collectors.toSet());
        for(String route : routes){
            MigrantCityPathAllChannelVO migrantCityPathAllChannelVo = new MigrantCityPathAllChannelVO();
            migrantCityPathAllChannelVo.setRoute(route);
            Double totalNum = 0d;
            for(MigrantCityPathChannelRelationRefVO refVo : refVos){
                if(route.equals(refVo.getRoute())){
                    //民航
                    if(ChannelTypeEnum.CIVIL_AVIATION.getType().equals(refVo.getChannelType())){
                        migrantCityPathAllChannelVo.setCivilAviationNum(refVo.getMigNum());
                    //公路
                    }else if(ChannelTypeEnum.HIGHWAY.getType().equals(refVo.getChannelType())){
                        migrantCityPathAllChannelVo.setHighwayNum(refVo.getMigNum());
                    //铁路
                    }else if(ChannelTypeEnum.RAILWAY.getType().equals(refVo.getChannelType())){
                        migrantCityPathAllChannelVo.setRailwayNum(refVo.getMigNum());
                    //水路
                    }else if(ChannelTypeEnum.WATERWAY.getType().equals(refVo.getChannelType())){
                        migrantCityPathAllChannelVo.setWaterwayNum(refVo.getMigNum());
                    }
                    //计算总数
                    totalNum += refVo.getMigNum().doubleValue();
                }
                migrantCityPathAllChannelVo.setTotalNum(BigDecimal.valueOf(totalNum).setScale(2, BigDecimal.ROUND_HALF_UP));
            }
            allChannelVos.add(migrantCityPathAllChannelVo);
        }
        return allChannelVos.stream().sorted(Comparator.comparing(MigrantCityPathAllChannelVO::getTotalNum).reversed()).limit(migrantDto.getLimit()).collect(Collectors.toList());
    }

    /**
     * 计算迁入增长率
     * @param list
     * @return
     */
    private void getOriginGrowthRate(BaseMigrantDTO migrantDto, List<MigrantRankVO> list){
        //获取上个月数据
        List<MigrantRankVO> lastMonList = baseMapper.getTotalNumListGroupByFromCity(DateUtil.addMonths(migrantDto.getStatDate(),-1),migrantDto.getArea(),null);
        //获取去年数据
        List<MigrantRankVO> lastYearList = baseMapper.getTotalNumListGroupByFromCity(DateUtil.addYears(migrantDto.getStatDate(),-1),migrantDto.getArea(),null);
        for(MigrantRankVO sameVo : list){
            if(CollectionUtils.isEmpty(lastMonList)){
                sameVo.setMom(BigDecimal.valueOf(100));
            }
            if(CollectionUtils.isEmpty(lastYearList)){
                sameVo.setYoy(BigDecimal.valueOf(100));
            }
            //计算环比上月
            for(MigrantRankVO lastVo : lastMonList){
                if(sameVo.getArea().equals(lastVo.getArea())){
                    sameVo.setMom(CalculationUtils.growthRate(sameVo.getNum().doubleValue(),lastVo.getNum().doubleValue()));
                    break;
                }
            }
            //计算同比去年
            for(MigrantRankVO lastVo : lastYearList){
                if(sameVo.getArea().equals(lastVo.getArea())){
                    sameVo.setYoy(CalculationUtils.growthRate(sameVo.getNum().doubleValue(),lastVo.getNum().doubleValue()));
                    break;
                }
            }
        }
    }

    /**
     * 计算迁出增长率
     * @param list
     * @return
     */
    private void getLeaveGrowthRate(BaseMigrantDTO migrantDto, List<MigrantRankVO> list){
        //获取上个月数据
        List<MigrantRankVO> lastMonList = baseMapper.getTotalNumListGroupByToCity(DateUtil.addMonths(migrantDto.getStatDate(),-1),migrantDto.getArea(),null);
        //获取去年数据
        List<MigrantRankVO> lastYearList = baseMapper.getTotalNumListGroupByToCity(DateUtil.addYears(migrantDto.getStatDate(),-1),migrantDto.getArea(),null);
        for(MigrantRankVO sameVo : list){
            if(CollectionUtils.isEmpty(lastMonList)){
                sameVo.setMom(BigDecimal.valueOf(100));
            }
            if(CollectionUtils.isEmpty(lastYearList)){
                sameVo.setYoy(BigDecimal.valueOf(100));
            }
            //计算环比上月
            for(MigrantRankVO lastVo : lastMonList){
                if(sameVo.getArea().equals(lastVo.getArea())){
                    sameVo.setMom(CalculationUtils.growthRate(sameVo.getNum().doubleValue(),lastVo.getNum().doubleValue()));
                    break;
                }
            }
            //计算同比去年
            for(MigrantRankVO lastVo : lastYearList){
                if(sameVo.getArea().equals(lastVo.getArea())){
                    sameVo.setYoy(CalculationUtils.growthRate(sameVo.getNum().doubleValue(),lastVo.getNum().doubleValue()));
                    break;
                }
            }
        }
    }
}