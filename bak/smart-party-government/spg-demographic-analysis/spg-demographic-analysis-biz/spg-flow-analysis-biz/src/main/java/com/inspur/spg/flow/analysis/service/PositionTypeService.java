/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.PositionType;
import com.inspur.spg.flow.analysis.api.vo.PositionTypeVO;

import java.util.List;

/**
 * 枢纽类别
 *
 * @author chenshun
 * @date 2022-02-21 20:25:21
 */
public interface PositionTypeService extends ICommonService<PositionType> {

    /**
     * @Author CJ
     * @Description 查询所有
     * @Date 2022/2/22 9:59
     * @param
     **/
    List<PositionTypeVO> getAll();



}
