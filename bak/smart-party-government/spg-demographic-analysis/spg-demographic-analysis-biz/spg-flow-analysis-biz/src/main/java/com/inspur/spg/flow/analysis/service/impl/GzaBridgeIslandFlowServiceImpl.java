package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgeIslandFlow;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeIslandFlowMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeIslandFlowService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * (GzaBridgeIslandFlow)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Service
public class GzaBridgeIslandFlowServiceImpl extends BasePositionServiceImpl<GzaBridgeIslandFlowMapper, GzaBridgeIslandFlow> implements GzaBridgeIslandFlowService {


    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeIslandFlow> list) {
        for (GzaBridgeIslandFlow entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}