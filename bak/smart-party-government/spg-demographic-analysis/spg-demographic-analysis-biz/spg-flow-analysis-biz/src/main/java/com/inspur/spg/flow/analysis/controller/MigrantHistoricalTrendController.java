package com.inspur.spg.flow.analysis.controller;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.dto.MigrantHistoricalTrendDTO;
import com.inspur.spg.flow.analysis.api.vo.MigrantHistoricalTrendVO;
import com.inspur.spg.flow.analysis.service.MigrantHistoricalTrendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author CJ
 * @Date 2022/3/4 10:00  历史迁徙模块
 * @Description:
 */
@RestController
@RequestMapping("/migrantHistoricalTrend")
@Api(value = "migrantHistoricalTrend", tags = "历史迁徙模块")
public class MigrantHistoricalTrendController {

    @Resource
    private MigrantHistoricalTrendService migrantHistoricalTrendService;

    /**
     * 获取省内地市迁徙人口信息
     * @param migrantDto
     * @return
     */
    @GetMapping("/getInfo")
    @ApiOperation(value = "获取人口信息", notes = "获取人口信息")
    public R<MigrantHistoricalTrendVO> getInfo(@Valid MigrantHistoricalTrendDTO migrantDto){
        if(migrantDto.getStatDate() == null){
            return R.failed("查询日期不能为空");
        }
        return R.ok(migrantHistoricalTrendService.getMigrantInfo(migrantDto));
    }

    /**
     * 获取省内地市迁徙人口统计趋势
     * @param migrantDto
     * @return
     */
    @GetMapping("/getChartInfo")
    @ApiOperation(value = "获取统计趋势", notes = "获取统计趋势")
    public R<List<ChartsInfo<BigDecimal>>> getChartInfo(@Valid MigrantHistoricalTrendDTO migrantDto){
        if(migrantDto.getStartDate() == null || migrantDto.getEndDate() == null){
            return R.failed("查询开始日期和结束日期不能为空");
        }
        return R.ok(migrantHistoricalTrendService.getChartsInfo(migrantDto));
    }

}
