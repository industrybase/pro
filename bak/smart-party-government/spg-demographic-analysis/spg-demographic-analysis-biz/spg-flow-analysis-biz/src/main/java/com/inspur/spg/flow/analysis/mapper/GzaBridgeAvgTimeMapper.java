package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeAvgTime;
import org.apache.ibatis.annotations.Mapper;

/**
 * 港珠澳大桥客流洞察-时点通关时长(GzaBridgeAvgTime)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 09:52:24
 */
@Mapper
public interface GzaBridgeAvgTimeMapper extends CommonMapper<GzaBridgeAvgTime> {

}