package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.StationStayTime;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 场站旅客-驻留时长(StationStayTime)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface StationStayTimeService extends ICommonService<StationStayTime> {

}