package com.inspur.spg.flow.analysis.controller;

import cn.hutool.core.thread.ExecutorBuilder;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.core.util.ThreadPoolUtil;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.vo.MigrantCityPathAllChannelVO;
import com.inspur.spg.flow.analysis.api.vo.MigrantCityRankVO;
import com.inspur.spg.flow.analysis.service.MigrantCityPathChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @Author CJ
 * @Date 2022/3/4 10:02
 * @Description: 省内地市迁徙模块
 */
@RestController
@RequestMapping("/migrantCity")
@Api(value = "migrantCity", tags = "省内地市迁徙模块")
public class MigrantCityController {

    @Resource
    private MigrantCityPathChannelService migrantCityPathChannelService;

    /**
     * @Author CJ
     * @Description 获取省内地市迁入迁出排名前十
     * @Date 2022/2/15 13:10
     * @param migrantDto
     * @return MigrantCityRankVo
     **/
    @GetMapping("/getRank")
    @ApiOperation(value = "获取省内地市迁入迁出排名前十", notes = "获取省内地市迁入迁出排名前十")
    public R<MigrantCityRankVO> getRank(@Valid BaseMigrantDTO migrantDto){
        if(migrantDto.getStatDate() == null){
            return R.failed("查询日期不能为空");
        }
        return R.ok(migrantCityPathChannelService.getRank(migrantDto));
    }

    /**
     * 热门迁徙地市路线排行
     * @param migrantDto
     * @return
     */
    @GetMapping("/getPopularRouteRank")
    @ApiOperation(value = "热门迁徙地市路线排行", notes = "热门迁徙地市路线排行")
    public R<List<MigrantCityPathAllChannelVO>> getPopularRouteRank(@Valid BaseMigrantDTO migrantDto){
        if(migrantDto.getStatDate() == null){
            return R.failed("查询日期不能为空");
        }
        return R.ok(migrantCityPathChannelService.getAllChannelList(migrantDto));
    }

}
