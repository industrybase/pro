package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeAvgDay;

/**
 * 港珠澳大桥客流趋势-每日平均通关时长(GzaBridgeAvgDay)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 09:52:23
 */
public interface GzaBridgeAvgDayService extends ICommonService<GzaBridgeAvgDay> {

}