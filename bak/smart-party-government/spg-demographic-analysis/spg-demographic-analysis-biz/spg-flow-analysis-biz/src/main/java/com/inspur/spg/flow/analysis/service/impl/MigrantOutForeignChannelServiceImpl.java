package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutForeignChannel;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutProvinceChannel;
import com.inspur.spg.flow.analysis.mapper.MigrantOutForeignChannelMapper;
import com.inspur.spg.flow.analysis.service.MigrantOutForeignChannelService;
import com.inspur.spg.flow.analysis.service.MigrantOutProvinceChannelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 人口迁徙-境外迁出去向及渠道(MigrantOutForeignChannel)表服务实现类
 *
 * @author cj
 * @since 2022-02-04 15:07:10
 */
@Service
public class MigrantOutForeignChannelServiceImpl extends CommonServiceImpl<MigrantOutForeignChannelMapper, MigrantOutForeignChannel> implements MigrantOutForeignChannelService {

    @Resource
    private MigrantOutProvinceChannelService migrantOutProvinceChannelService;

    @Override
    public int getTotalNum(BaseMigrantDTO migrantDto) {
        return baseMapper.getTotalNum(migrantDto);
    }

    @Override
    @Transactional
    public boolean fastSaveIgnoreBatch(List<MigrantOutForeignChannel> list) {
        List<MigrantOutProvinceChannel> migrantOutProvinceChannelList = new ArrayList<>();
        List<MigrantOutForeignChannel> migrantOutForeignChannelList = new ArrayList<>();
        for(MigrantOutForeignChannel foreignChannel : list){
            /*String outArea = foreignChannel.getOutArea();
            if(
                    "中国澳门".equals(outArea) ||
                    "中国台湾".equals(outArea) ||
                    "中国香港".equals(outArea)){
                outArea = outArea.replace("中国","");
                MigrantOutProvinceChannel migrantOutProvinceChannel = new MigrantOutProvinceChannel();
                BeanUtil.copyProperties(foreignChannel, migrantOutProvinceChannel);
                migrantOutProvinceChannel.setOutArea(outArea);
                migrantOutProvinceChannelList.add(migrantOutProvinceChannel);
            }else{
                migrantOutForeignChannelList.add(foreignChannel);
            }*/
            migrantOutForeignChannelList.add(foreignChannel);
        }
        migrantOutProvinceChannelService.fastSaveIgnoreBatch(migrantOutProvinceChannelList);
        return super.fastSaveIgnoreBatch(migrantOutForeignChannelList);
    }
}