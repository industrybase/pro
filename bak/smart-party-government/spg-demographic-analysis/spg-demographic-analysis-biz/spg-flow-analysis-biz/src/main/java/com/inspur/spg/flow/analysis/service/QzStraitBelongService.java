package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.QzStraitBelong;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 琼州海峡客流归属地(QzStraitBelong)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface QzStraitBelongService extends ICommonService<QzStraitBelong> {

}