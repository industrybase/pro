package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.RestAreaInProvince;
import com.inspur.spg.flow.analysis.mapper.RestAreaProvinceMapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaProvince;
import com.inspur.spg.flow.analysis.service.RestAreaProvinceService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务区客流画像-归属省际(RestAreaProvince)表服务实现类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Service
public class RestAreaProvinceServiceImpl extends BasePositionServiceImpl<RestAreaProvinceMapper, RestAreaProvince> implements RestAreaProvinceService {

    @Override
    public boolean fastSaveIgnoreBatch(List<RestAreaProvince> list) {
        for (RestAreaProvince entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}