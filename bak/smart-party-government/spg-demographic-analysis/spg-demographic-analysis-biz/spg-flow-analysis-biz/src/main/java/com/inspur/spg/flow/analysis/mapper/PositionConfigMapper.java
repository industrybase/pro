package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.flow.analysis.api.entity.PositionConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 枢纽阀值列表(PostionWarning)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface PositionConfigMapper extends BaseMapper<PositionConfig> {

    @Select("select code from position_config where name = #{name}")
    String getCodeByName(@Param("name") String name);

}