package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.StationCityOrigin;

/**
 * 场站旅客洞察-省内来源(StationCityOrigin)表服务接口
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
public interface StationCityOriginService extends ICommonService<StationCityOrigin> {

}