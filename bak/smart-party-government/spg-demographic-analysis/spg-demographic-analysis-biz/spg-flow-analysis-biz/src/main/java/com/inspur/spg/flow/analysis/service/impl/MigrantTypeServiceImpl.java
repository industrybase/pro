package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.MigrantType;
import com.inspur.spg.flow.analysis.mapper.MigrantTypeMapper;
import com.inspur.spg.flow.analysis.service.MigrantTypeService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

/**
 * 人口迁徙-类别(MigrantType)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class MigrantTypeServiceImpl extends CommonServiceImpl<MigrantTypeMapper, MigrantType> implements MigrantTypeService {

}