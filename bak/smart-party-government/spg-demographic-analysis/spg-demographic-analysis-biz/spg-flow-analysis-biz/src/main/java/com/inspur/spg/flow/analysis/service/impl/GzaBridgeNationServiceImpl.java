package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgeNation;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeNationMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeNationService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流洞察-归属国家分布(GzaBridgeNation)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Service
public class GzaBridgeNationServiceImpl extends BasePositionServiceImpl<GzaBridgeNationMapper, GzaBridgeNation> implements GzaBridgeNationService {


    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeNation> list) {
        for (GzaBridgeNation entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}