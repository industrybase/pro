package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.mapper.MigrantFromChannelMapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromChannel;
import com.inspur.spg.flow.analysis.service.MigrantFromChannelService;
import org.springframework.stereotype.Service;

/**
 * 迁徙来源省份(MigrantFromChannel)表服务实现类
 *
 * @author CJ
 * @since 2022-02-10 11:27:36
 */
@Service
public class MigrantFromChannelServiceImpl extends CommonServiceImpl<MigrantFromChannelMapper, MigrantFromChannel> implements MigrantFromChannelService {

}