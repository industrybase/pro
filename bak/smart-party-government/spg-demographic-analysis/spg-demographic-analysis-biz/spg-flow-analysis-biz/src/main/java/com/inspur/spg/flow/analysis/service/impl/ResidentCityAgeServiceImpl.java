package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.ResidentCityAge;
import com.inspur.spg.flow.analysis.mapper.ResidentCityAgeMapper;
import com.inspur.spg.flow.analysis.service.ResidentCityAgeService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

/**
 * 人口洞察-地市常驻人口年龄(ResidentCityAge)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class ResidentCityAgeServiceImpl extends CommonServiceImpl<ResidentCityAgeMapper, ResidentCityAge> implements ResidentCityAgeService {


}