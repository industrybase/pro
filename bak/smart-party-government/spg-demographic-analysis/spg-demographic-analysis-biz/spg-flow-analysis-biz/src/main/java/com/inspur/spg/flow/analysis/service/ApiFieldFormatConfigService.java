package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.ApiFieldFormatConfig;

import java.util.List;
import java.util.Map;

/**
 * API接口字段转换配置(ApiFieldFormatConfig)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 16:15:47
 */
public interface ApiFieldFormatConfigService extends IService<ApiFieldFormatConfig> {

    List<ApiFieldFormatConfig> getListByApiId(Integer apiId);

    void saveByConfigField(Map<String, Object> map) throws ClassNotFoundException;
}