package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.StationSex;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 场站客流画像-性别(StationSex)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface StationSexService extends ICommonService<StationSex> {

}