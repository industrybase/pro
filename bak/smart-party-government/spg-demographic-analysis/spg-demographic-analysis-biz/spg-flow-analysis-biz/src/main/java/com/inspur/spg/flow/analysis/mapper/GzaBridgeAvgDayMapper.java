package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeAvgDay;

/**
 * 港珠澳大桥客流趋势-每日平均通关时长(GzaBridgeAvgDay)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 09:52:21
 */
@Mapper
public interface GzaBridgeAvgDayMapper extends CommonMapper<GzaBridgeAvgDay> {

}