package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.TollDayFlow;
import org.apache.ibatis.annotations.Mapper;

/**
 * 收费站每日客流(TollDayFlow)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:52:42
 */
@Mapper
public interface TollDayFlowMapper extends CommonMapper<TollDayFlow> {

}