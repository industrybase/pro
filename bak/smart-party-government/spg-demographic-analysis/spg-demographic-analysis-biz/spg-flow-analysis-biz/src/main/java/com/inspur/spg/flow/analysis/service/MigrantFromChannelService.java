package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromChannel;

/**
 * 迁徙来源省份(MigrantFromChannel)表服务接口
 *
 * @author CJ
 * @since 2022-02-10 11:27:36
 */
public interface MigrantFromChannelService extends ICommonService<MigrantFromChannel> {

}