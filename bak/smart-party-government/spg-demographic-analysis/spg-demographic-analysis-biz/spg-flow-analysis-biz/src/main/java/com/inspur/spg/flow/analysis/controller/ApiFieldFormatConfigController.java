package com.inspur.spg.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.flow.analysis.service.ApiFieldFormatConfigService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.Map;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/7 10:38
 **/
@RestController
@RequestMapping("/apiFieldFormatConfig")
public class ApiFieldFormatConfigController {

    @Resource
    private ApiFieldFormatConfigService apiFieldFormatConfigService;

    @PostMapping("/saveByApiResult")
    @ApiIgnore
    @Inner
    public R saveByApi(@RequestBody Map<String,Object> map) throws ClassNotFoundException {
        apiFieldFormatConfigService.saveByConfigField(map);
        return R.ok();
    }

}
