package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.flow.analysis.api.entity.ApiConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * API接口配置(ApiDbTableConfig)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 16:15:44
 */
@Mapper
public interface ApiConfigMapper extends BaseMapper<ApiConfig> {


}