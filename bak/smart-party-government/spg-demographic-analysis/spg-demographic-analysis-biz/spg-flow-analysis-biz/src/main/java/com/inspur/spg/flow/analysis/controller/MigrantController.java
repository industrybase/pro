package com.inspur.spg.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.vo.PopulationInsightVO;
import com.inspur.spg.flow.analysis.service.MigrantFromService;
import com.inspur.spg.flow.analysis.service.MigrantOutService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/*
 * @Author CJ
 * @Description 迁徙来源
 * @Date 2022/2/10 10:41
 **/
@RestController
@RequestMapping("/migrant")
@Api(value = "migrant", tags = "迁徙模块")
public class MigrantController {
    
    @Resource
    private MigrantFromService migrantFromService;
    @Resource
    private MigrantOutService migrantOutService;

    /**
     * @Author CJ
     * @Description 人口洞察
     * @Date 2022/2/15 13:10
     * @param migrantDto
     * @return com.inspur.spg.common.core.util.R<com.inspur.spg.flow.analysis.api.vo.PopulationInsightVo>
     **/
    @GetMapping("/getPopulationStats")
    @ApiOperation(value = "获取人口统计信息", notes = "获取人口统计信息")
    public R<PopulationInsightVO> getPopulationStats(@Valid BaseMigrantDTO migrantDto){
        if(migrantDto.getStatDate()  == null){
            return R.failed("查询日期不能为空");
        }
        //数据统计
        return R.ok(PopulationInsightVO.builder()
                .leave(migrantOutService.getPopulationStats(migrantDto))
                .origin(migrantFromService.getPopulationStats(migrantDto))
                .build());
    }

}
