package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromSource;
import com.inspur.spg.flow.analysis.mapper.MigrantFromSourceMapper;
import com.inspur.spg.flow.analysis.service.MigrantFromSourceService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

/**
 * 人口迁徙-迁入来源类型(MigrantFromSource)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class MigrantFromSourceServiceImpl extends CommonServiceImpl<MigrantFromSourceMapper, MigrantFromSource> implements MigrantFromSourceService {

}