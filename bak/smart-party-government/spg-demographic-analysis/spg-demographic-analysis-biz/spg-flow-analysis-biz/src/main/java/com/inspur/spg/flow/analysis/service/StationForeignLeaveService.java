package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationForeignLeave;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 场站旅客洞察-国际去向(StationForeignLeave)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface StationForeignLeaveService extends ICommonService<StationForeignLeave> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/11 16:17
     * @param positionDto
     * @return int
     **/
    int getTotalNum(BasePositionDTO positionDto);

}