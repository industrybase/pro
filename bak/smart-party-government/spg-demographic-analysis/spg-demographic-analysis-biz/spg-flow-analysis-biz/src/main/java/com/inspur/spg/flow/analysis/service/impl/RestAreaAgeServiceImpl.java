package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.StationAge;
import com.inspur.spg.flow.analysis.mapper.RestAreaAgeMapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaAge;
import com.inspur.spg.flow.analysis.service.RestAreaAgeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务区客流画像-年龄(RestAreaAge)表服务实现类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Service
public class RestAreaAgeServiceImpl extends BasePositionServiceImpl<RestAreaAgeMapper, RestAreaAge> implements RestAreaAgeService {

    @Override
    public boolean fastSaveIgnoreBatch(List<RestAreaAge> list) {
        for (RestAreaAge entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}