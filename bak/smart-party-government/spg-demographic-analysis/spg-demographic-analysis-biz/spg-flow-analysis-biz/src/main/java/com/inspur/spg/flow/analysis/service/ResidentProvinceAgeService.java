package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.ResidentProvinceAge;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 人口洞察-地市常驻人口(ResidentProvinceAge)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface ResidentProvinceAgeService extends ICommonService<ResidentProvinceAge> {

}