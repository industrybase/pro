package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.QzStraitAge;
import com.inspur.spg.flow.analysis.mapper.QzStraitAgeMapper;
import com.inspur.spg.flow.analysis.service.QzStraitAgeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * (QzStraitAge)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class QzStraitAgeServiceImpl extends BasePositionServiceImpl<QzStraitAgeMapper, QzStraitAge> implements QzStraitAgeService {

    @Override
    public boolean fastSaveIgnoreBatch(List<QzStraitAge> list) {
        for (QzStraitAge entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}