package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.PositionConfig;

import java.util.List;

/**
 * 枢纽阀值列表(PositionWarning)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface PositionConfigService extends IService<PositionConfig> {

    /**
     * @Author CJ
     * @Description 根据场站名称查询
     * @Date 2022/1/6 17:40
     * @param name
     * @return com.inspur.spg.flow.analysis.api.entity.PositionWarning
     **/
    PositionConfig getByName(String name);

    /**
     * @Author CJ
     * @Description 查询所有
     * @Date 2022/2/22 10:00
     * @param
     **/
    List<PositionConfig> getAll();

    /**
     * @Author CJ
     * @Description 根据类型id查询
     * @Date 2022/2/22 10:07
     * @param typeId
     **/
    List<PositionConfig> getAllByTypeId(Integer typeId);
}