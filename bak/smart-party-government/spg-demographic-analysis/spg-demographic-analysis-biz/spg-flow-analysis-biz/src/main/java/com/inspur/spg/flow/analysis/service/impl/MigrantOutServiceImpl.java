package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.vo.PopulationProportionStatsVO;
import com.inspur.spg.flow.analysis.service.MigrantOutForeignChannelService;
import com.inspur.spg.flow.analysis.service.MigrantOutProvinceChannelService;
import com.inspur.spg.flow.analysis.service.MigrantOutService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/15 12:53
 **/
@Service
public class MigrantOutServiceImpl implements MigrantOutService {

    @Resource
    private MigrantOutProvinceChannelService migrantOutProvinceChannelService;
    @Resource
    private MigrantOutForeignChannelService migrantOutForeignChannelService;

    @Override
    public PopulationProportionStatsVO getPopulationStats(BaseMigrantDTO migrantDto) {
        int foreignTotalNum = migrantOutProvinceChannelService.getTotalNum(migrantDto);
        //省外总数
        int provinceTotalNum = migrantOutForeignChannelService.getTotalNum(migrantDto);
        int totalNum = foreignTotalNum + provinceTotalNum;
        //计算占比
        BigDecimal provinceProportion = totalNum == 0 ? BigDecimal.valueOf(0) : BigDecimal.valueOf((double) provinceTotalNum/totalNum * 100).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal foreignProportion = BigDecimal.valueOf(100d - provinceProportion.doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
        return PopulationProportionStatsVO.builder()
                .chartsInfos(migrantOutProvinceChannelService.getChartsInfo(migrantDto))
                .foreignProportion(foreignProportion)
                .provinceProportion(provinceProportion).build();
    }
}
