package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.QzStraitTrend;
import com.inspur.spg.flow.analysis.mapper.QzStraitTrendMapper;
import com.inspur.spg.flow.analysis.service.QzStraitTrendService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 琼州海峡客流趋势(QzStraitTrend)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class QzStraitTrendServiceImpl extends BasePositionServiceImpl<QzStraitTrendMapper, QzStraitTrend> implements QzStraitTrendService {


    @Override
    public boolean fastSaveIgnoreBatch(List<QzStraitTrend> list) {
        for (QzStraitTrend entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}