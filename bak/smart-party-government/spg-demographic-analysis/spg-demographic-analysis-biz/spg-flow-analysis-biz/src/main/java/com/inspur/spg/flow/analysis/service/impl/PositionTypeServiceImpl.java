/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.PositionType;
import com.inspur.spg.flow.analysis.api.vo.PositionTypeVO;
import com.inspur.spg.flow.analysis.mapper.PositionTypeMapper;
import com.inspur.spg.flow.analysis.service.PositionConfigService;
import com.inspur.spg.flow.analysis.service.PositionTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 枢纽类别
 *
 * @author chenshun
 * @date 2022-02-21 20:25:21
 */
@Service
public class PositionTypeServiceImpl extends CommonServiceImpl<PositionTypeMapper, PositionType> implements PositionTypeService {

    @Resource
    private PositionConfigService positionConfigService;

    @Override
    public List<PositionTypeVO> getAll() {
        List<PositionType> list = list();
        List<PositionTypeVO> voList = list.stream().map(item -> {
            PositionTypeVO positionTypeVo = new PositionTypeVO();
            positionTypeVo.setPositionConfigs(positionConfigService.getAllByTypeId(item.getId()));
            positionTypeVo.setId(item.getId());
            positionTypeVo.setName(item.getName());
            return positionTypeVo;
        }).collect(Collectors.toList());
        return voList;
    }


}
