package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.QzStraitAge;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * (QzStraitAge)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface QzStraitAgeService extends ICommonService<QzStraitAge> {

}