package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.RestAreaSex;
import com.inspur.spg.flow.analysis.mapper.RestAreaStayTimeMapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaStayTime;
import com.inspur.spg.flow.analysis.service.RestAreaStayTimeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务区客流画像驻留时长(RestAreaStayTime)表服务实现类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Service
public class RestAreaStayTimeServiceImpl extends BasePositionServiceImpl<RestAreaStayTimeMapper, RestAreaStayTime> implements RestAreaStayTimeService {

    @Override
    public boolean fastSaveIgnoreBatch(List<RestAreaStayTime> list) {
        for (RestAreaStayTime entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}