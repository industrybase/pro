package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.StationCityLeave;

/**
 * 场站旅客洞察-省内去向(StationCityLeave)表服务接口
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
public interface StationCityLeaveService extends ICommonService<StationCityLeave> {

}