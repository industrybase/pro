package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromChannel;

/**
 * 迁徙来源省份(MigrantFromChannel)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-10 11:27:36
 */
@Mapper
public interface MigrantFromChannelMapper extends CommonMapper<MigrantFromChannel> {



}