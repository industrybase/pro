package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.TollAge;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TollAgeMapper extends CommonMapper<TollAge> {
}
