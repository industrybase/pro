package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.mapper.GzaBridgeAvgDayMapper;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeAvgDay;
import com.inspur.spg.flow.analysis.service.GzaBridgeAvgDayService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流趋势-每日平均通关时长(GzaBridgeAvgDay)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:23
 */
@Service
public class GzaBridgeAvgDayServiceImpl extends BasePositionServiceImpl<GzaBridgeAvgDayMapper, GzaBridgeAvgDay> implements GzaBridgeAvgDayService {


    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeAvgDay> list) {
        for (GzaBridgeAvgDay entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}