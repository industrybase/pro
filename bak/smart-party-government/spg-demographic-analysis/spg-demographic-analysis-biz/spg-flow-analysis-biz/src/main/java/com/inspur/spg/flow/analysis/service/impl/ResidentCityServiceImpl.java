package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.constant.enums.GenderEnum;
import com.inspur.spg.flow.analysis.api.entity.ResidentCity;
import com.inspur.spg.flow.analysis.mapper.ResidentCityMapper;
import com.inspur.spg.flow.analysis.service.ResidentCityService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 人口洞察-地市常驻人口(ResidentCity)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class ResidentCityServiceImpl extends CommonServiceImpl<ResidentCityMapper, ResidentCity> implements ResidentCityService {

    @Override
    public boolean fastSaveIgnoreBatch(List<ResidentCity> list) {
        List<ResidentCity> saveList = new ArrayList<>();
        for(ResidentCity item : list){
            //男性人口
            ResidentCity male = new ResidentCity();
            BeanUtils.copyProperties(item,male);
            male.setGender(GenderEnum.male.getCode());
            Integer maleNum = (int)Math.round(item.getNum().doubleValue() * item.getProportion().doubleValue());
            male.setNum(maleNum);
            saveList.add(male);

            //女性人口
            ResidentCity female = new ResidentCity();
            BeanUtils.copyProperties(item,female);
            female.setGender(GenderEnum.female.getCode());
            Double proportion = 1d - male.getProportion().doubleValue();
            female.setProportion(new BigDecimal(proportion));
            female.setNum(item.getNum().intValue() - maleNum.intValue());
            saveList.add(female);
            //总人口
            item.setProportion(BigDecimal.valueOf(1));
            saveList.add(item);
        }
        return super.fastSaveIgnoreBatch(saveList);
    }

}