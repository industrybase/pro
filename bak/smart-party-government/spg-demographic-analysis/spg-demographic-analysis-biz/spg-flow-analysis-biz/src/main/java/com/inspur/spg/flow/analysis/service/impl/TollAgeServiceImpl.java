package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.TollAge;
import com.inspur.spg.flow.analysis.mapper.TollAgeMapper;
import com.inspur.spg.flow.analysis.service.TollAgeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/31 9:12
 **/
@Service
public class TollAgeServiceImpl extends BasePositionServiceImpl<TollAgeMapper, TollAge> implements TollAgeService {

    @Override
    public boolean fastSaveIgnoreBatch(List<TollAge> list) {
        for (TollAge entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }


}
