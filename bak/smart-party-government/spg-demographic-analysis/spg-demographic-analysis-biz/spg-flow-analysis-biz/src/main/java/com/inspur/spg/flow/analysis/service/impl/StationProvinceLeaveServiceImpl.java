package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.mapper.StationProvinceLeaveMapper;
import com.inspur.spg.flow.analysis.api.entity.StationProvinceLeave;
import com.inspur.spg.flow.analysis.service.StationProvinceLeaveService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站旅客洞察-省外去向地(StationProvinceLeave)表服务实现类
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
@Service
public class StationProvinceLeaveServiceImpl extends BasePositionServiceImpl<StationProvinceLeaveMapper, StationProvinceLeave> implements StationProvinceLeaveService {

    @Override
    public boolean fastSaveIgnoreBatch(List<StationProvinceLeave> list) {
        for (StationProvinceLeave entity : list) {
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }


    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfo(BasePositionDTO positionDto) {
        return baseMapper.getChartsInfoGroupByAreaName(positionDto);
    }

    @Override
    public int getTotalNum(BasePositionDTO positionDto) {
        return baseMapper.getTotalNum(positionDto);
    }

}