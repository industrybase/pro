package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationForeignLeave;
import org.apache.ibatis.annotations.Mapper;

/**
 * 场站旅客洞察-国际去向(StationForeignLeave)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface StationForeignLeaveMapper extends CommonMapper<StationForeignLeave> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/11 16:17
     * @return int
     **/
    int getTotalNum(BasePositionDTO positionDto);

}