package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantRefChannelForeignProvince;

/**
 * 关联表迁入迁出(包含境外,省外)(MigrantRefChannelForeignProvince)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-10 11:30:13
 */
@Mapper
public interface MigrantRefChannelForeignProvinceMapper extends CommonMapper<MigrantRefChannelForeignProvince> {

}