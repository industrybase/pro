package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutChannel;

/**
 * 迁出渠道分布(MigrantOutChannel)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-10 11:28:01
 */
@Mapper
public interface MigrantOutChannelMapper extends CommonMapper<MigrantOutChannel> {

}