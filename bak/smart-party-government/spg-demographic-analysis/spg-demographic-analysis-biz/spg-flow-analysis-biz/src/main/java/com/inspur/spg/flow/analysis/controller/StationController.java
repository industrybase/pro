package com.inspur.spg.flow.analysis.controller;

import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.vo.PopulationInsightVO;
import com.inspur.spg.flow.analysis.api.vo.StationPassengerVO;
import com.inspur.spg.flow.analysis.service.StationLeaveService;
import com.inspur.spg.flow.analysis.service.StationOriginService;
import com.inspur.spg.flow.analysis.service.StationPassengerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/*
 * @Author CJ
 * @Description 场站ctl
 * @Date 2022/2/11 16:36
 **/
@RestController
@RequestMapping("/station")
@Api(value = "station", tags = "场站模块")
public class StationController {

    @Resource
    private StationPassengerService stationPassengerService;
    @Resource
    private StationLeaveService stationLeaveService;
    @Resource
    private StationOriginService stationOriginService;

    /**
     * @Author CJ
     * @Description 获取旅客量
     * @Date 2022/2/11 16:39
     * @param
     * @return com.inspur.spg.common.core.util.R<java.util.Map<java.lang.String,java.lang.Integer>>
     **/
    @GetMapping("/getPassengerNum")
    @ApiOperation(value = "获取旅客量", notes = "获取旅客量")
    public R<StationPassengerVO> getPassengerNum(@Valid BasePositionDTO positionDto){
        if(positionDto.getStatDate()  == null){
            return R.failed("查询日期不能为空");
        }
        return R.ok(stationPassengerService.getPassengerNum(positionDto));
    }

    /**
     * @Author CJ
     * @Description 人口洞察
     * @Date 2022/2/15 13:10
     * @param positionDto
     * @return com.inspur.spg.common.core.util.R<com.inspur.spg.flow.analysis.api.vo.PopulationInsightVo>
     **/
    @GetMapping("/getPopulationStats")
    @ApiOperation(value = "获取人口统计信息", notes = "获取人口统计信息")
    public R<PopulationInsightVO> getPopulationStats(@Valid BasePositionDTO positionDto){
        if(positionDto.getStatDate()  == null){
            return R.failed("查询日期不能为空");
        }
        //数据统计
        return R.ok(PopulationInsightVO.builder()
                .leave(stationLeaveService.getPopulationStats(positionDto))
                .origin(stationOriginService.getPopulationStats(positionDto))
                .build());
    }
}
