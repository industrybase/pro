package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutForeignChannel;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人口迁徙-境外迁出去向及渠道(MigrantOutForeignChannel)表数据库访问层
 *
 * @author cj
 * @since 2022-02-04 15:07:09
 */
@Mapper
public interface MigrantOutForeignChannelMapper extends CommonMapper<MigrantOutForeignChannel> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/10 12:46
     * @param migrantDto
     * @return java.lang.Integer
     **/
    int getTotalNum(BaseMigrantDTO migrantDto);

}