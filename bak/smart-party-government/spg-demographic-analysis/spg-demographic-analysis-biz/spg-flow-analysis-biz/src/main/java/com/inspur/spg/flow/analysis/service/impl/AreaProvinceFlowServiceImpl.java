package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.api.dto.AreaProvinceFlowDTO;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.AreaProvinceFlow;
import com.inspur.spg.flow.analysis.mapper.AreaProvinceFlowMapper;
import com.inspur.spg.flow.analysis.service.AreaProvinceFlowService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 省客流实时数据（5分钟）(AreaProvinceFlow)表服务实现类
 *
 * @author CJ
 * @since 2022-02-07 11:33:38
 */
@Service
public class AreaProvinceFlowServiceImpl extends CommonServiceImpl<AreaProvinceFlowMapper, AreaProvinceFlow> implements AreaProvinceFlowService {

    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfo(AreaProvinceFlowDTO areaProvinceFlowDTO) {
        return baseMapper.getChartsInfo(areaProvinceFlowDTO.getStatDate(),areaProvinceFlowDTO.getArea(), DateUtil.getHourlyList(areaProvinceFlowDTO.getStatDate()));
    }

}