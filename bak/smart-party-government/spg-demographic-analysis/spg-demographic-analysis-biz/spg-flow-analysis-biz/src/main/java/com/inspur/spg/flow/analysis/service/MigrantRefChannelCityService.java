package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.MigrantRefChannelCity;

/**
 * 关联表省内(MigrantRefChannelCity)表服务接口
 *
 * @author CJ
 * @since 2022-02-10 11:30:00
 */
public interface MigrantRefChannelCityService extends ICommonService<MigrantRefChannelCity> {

}