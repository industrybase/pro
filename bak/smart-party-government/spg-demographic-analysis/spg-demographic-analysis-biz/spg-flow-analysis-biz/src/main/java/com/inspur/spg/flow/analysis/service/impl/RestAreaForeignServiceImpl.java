package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.RestAreaCategory;
import com.inspur.spg.flow.analysis.mapper.RestAreaForeignMapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaForeign;
import com.inspur.spg.flow.analysis.service.RestAreaForeignService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务区客流画像归属-国际(RestAreaForeign)表服务实现类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Service
public class RestAreaForeignServiceImpl extends BasePositionServiceImpl<RestAreaForeignMapper, RestAreaForeign> implements RestAreaForeignService {

    @Override
    public boolean fastSaveIgnoreBatch(List<RestAreaForeign> list) {
        for (RestAreaForeign entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}