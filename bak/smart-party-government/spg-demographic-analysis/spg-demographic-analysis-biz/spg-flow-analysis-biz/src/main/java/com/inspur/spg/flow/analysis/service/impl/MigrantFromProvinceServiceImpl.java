package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromProvince;
import com.inspur.spg.flow.analysis.mapper.MigrantFromProvinceMapper;
import com.inspur.spg.flow.analysis.service.MigrantFromProvinceService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * 人口迁徙-省外迁入来源及渠道(MigrantFromProvince)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class MigrantFromProvinceServiceImpl extends CommonServiceImpl<MigrantFromProvinceMapper, MigrantFromProvince> implements MigrantFromProvinceService {

    @Override
    public int getTotalNum(BaseMigrantDTO migrantDto) {
        return baseMapper.getTotalNum(migrantDto);
    }

    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfo(BaseMigrantDTO migrantDto){
        return baseMapper.getChartsInfoGroupByInArea(migrantDto);
    }
}