package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.RestAreaProvince;
import com.inspur.spg.flow.analysis.mapper.RestAreaSexMapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaSex;
import com.inspur.spg.flow.analysis.service.RestAreaSexService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 服务区客流画像-性别(RestAreaSex)表服务实现类
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Service
public class RestAreaSexServiceImpl extends BasePositionServiceImpl<RestAreaSexMapper, RestAreaSex> implements RestAreaSexService {

    @Override
    public boolean fastSaveIgnoreBatch(List<RestAreaSex> list) {
        for (RestAreaSex entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}