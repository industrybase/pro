package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.dto.AreaProvinceFlowDTO;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.AreaProvinceFlow;

import java.math.BigDecimal;
import java.util.List;

/**
 * 省客流实时数据（5分钟）(AreaProvinceFlow)表服务接口
 *
 * @author CJ
 * @since 2022-02-07 11:33:38
 */
public interface AreaProvinceFlowService extends ICommonService<AreaProvinceFlow> {

    /**
     * 查询统计数据
     * @param areaProvinceFlowDTO
     * @return
     */
    List<ChartsInfo<BigDecimal>> getChartsInfo(AreaProvinceFlowDTO areaProvinceFlowDTO);


}