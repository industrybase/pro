package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.StationAge;
import com.inspur.spg.flow.analysis.api.entity.StationForeignLeave;
import com.inspur.spg.flow.analysis.mapper.StationAgeMapper;
import com.inspur.spg.flow.analysis.service.StationAgeService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站客流画像-年龄(StationAge)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class StationAgeServiceImpl extends BasePositionServiceImpl<StationAgeMapper, StationAge> implements StationAgeService {

    @Override
    public boolean fastSaveIgnoreBatch(List<StationAge> list) {
        for (StationAge entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}