package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeAvgTime;

/**
 * 港珠澳大桥客流洞察-时点通关时长(GzaBridgeAvgTime)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 09:52:25
 */
public interface GzaBridgeAvgTimeService extends ICommonService<GzaBridgeAvgTime> {

}