package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.ResidentCity;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 人口洞察-地市常驻人口(ResidentCity)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface ResidentCityService extends ICommonService<ResidentCity> {

}