package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.ResidentProvince;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人口洞察-省常驻人口年龄(ResidentProvince)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface ResidentProvinceMapper extends CommonMapper<ResidentProvince> {

}