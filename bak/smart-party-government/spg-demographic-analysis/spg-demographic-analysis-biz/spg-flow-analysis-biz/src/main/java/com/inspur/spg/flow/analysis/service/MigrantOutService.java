package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.vo.PopulationProportionStatsVO;

public interface MigrantOutService {

    /**
     * @Author CJ
     * @Description 获取人口信息
     * @Date 2022/2/11 18:18
     * @param migrantDto
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    PopulationProportionStatsVO getPopulationStats(BaseMigrantDTO migrantDto);

}
