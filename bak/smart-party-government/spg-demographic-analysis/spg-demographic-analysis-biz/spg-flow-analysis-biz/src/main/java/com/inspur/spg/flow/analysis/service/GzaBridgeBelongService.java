package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeBelong;

/**
 * 港珠澳大桥客流洞察-归属地类型(GzaBridgeBelong)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 09:59:24
 */
public interface GzaBridgeBelongService extends ICommonService<GzaBridgeBelong> {

}