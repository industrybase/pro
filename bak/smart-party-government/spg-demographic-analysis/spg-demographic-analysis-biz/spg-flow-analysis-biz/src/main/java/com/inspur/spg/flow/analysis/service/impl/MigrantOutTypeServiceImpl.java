package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutType;
import com.inspur.spg.flow.analysis.mapper.MigrantOutTypeMapper;
import com.inspur.spg.flow.analysis.service.MigrantOutTypeService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

/**
 * 人口迁徙-迁出类别分布(MigrantOutType)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class MigrantOutTypeServiceImpl extends CommonServiceImpl<MigrantOutTypeMapper, MigrantOutType> implements MigrantOutTypeService {

}