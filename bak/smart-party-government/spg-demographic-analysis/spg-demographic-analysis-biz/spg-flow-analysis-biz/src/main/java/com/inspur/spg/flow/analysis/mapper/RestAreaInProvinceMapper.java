package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.RestAreaInProvince;

/**
 * 服务区客流画像归属-省内(RestAreaInProvince)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
@Mapper
public interface RestAreaInProvinceMapper extends CommonMapper<RestAreaInProvince> {

}