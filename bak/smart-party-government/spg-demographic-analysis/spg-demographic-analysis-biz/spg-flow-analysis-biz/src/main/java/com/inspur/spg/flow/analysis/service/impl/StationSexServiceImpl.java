package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.StationSex;
import com.inspur.spg.flow.analysis.mapper.StationSexMapper;
import com.inspur.spg.flow.analysis.service.PositionConfigService;
import com.inspur.spg.flow.analysis.service.StationSexService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站客流画像-性别(StationSex)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class StationSexServiceImpl extends BasePositionServiceImpl<StationSexMapper, StationSex> implements StationSexService {

    @Resource
    private PositionConfigService positionConfigService;

    @Override
    public boolean fastSaveIgnoreBatch(List<StationSex> list) {
        for (StationSex entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}