package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantFromSource;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人口迁徙-迁入来源类型(MigrantFromSource)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface MigrantFromSourceMapper extends CommonMapper<MigrantFromSource> {

}