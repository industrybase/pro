package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutProvinceChannel;
import org.apache.ibatis.annotations.Mapper;

import java.math.BigDecimal;
import java.util.List;

/**
 * 人口迁徙-省际迁出去向及渠道(MigrantProvinceChannel)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface MigrantOutProvinceChannelMapper extends CommonMapper<MigrantOutProvinceChannel> {

    /**
     * @Author CJ
     * @Description 统计总数
     * @Date 2022/2/10 12:46
     * @param
     * @return java.lang.Integer
     **/
    int getTotalNum(BaseMigrantDTO migrantDto);

    /**
     *
     * @param migrantDto
     * @return
     */
    List<ChartsInfo<BigDecimal>> getChartsInfoGroupByOutArea(BaseMigrantDTO migrantDto);

}