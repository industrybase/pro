package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import org.apache.ibatis.annotations.Mapper;
import com.inspur.spg.flow.analysis.api.entity.StationCityOrigin;

/**
 * 场站旅客洞察-省内来源(StationCityOrigin)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
@Mapper
public interface StationCityOriginMapper extends CommonMapper<StationCityOrigin> {

}