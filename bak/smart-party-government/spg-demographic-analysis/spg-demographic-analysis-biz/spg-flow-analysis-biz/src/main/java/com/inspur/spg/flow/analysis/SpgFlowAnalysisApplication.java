package com.inspur.spg.flow.analysis;

import cn.hutool.extra.spring.SpringUtil;
import com.inspur.spg.common.core.util.SpringContextHolder;
import com.inspur.spg.common.feign.annotation.EnableSpgFeignClients;
import com.inspur.spg.common.security.annotation.EnableSpgResourceServer;
import com.inspur.spg.common.swagger.annotation.EnableSpgSwagger2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
import java.util.Arrays;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/6 11:00
 **/
@EnableSpgSwagger2
@EnableSpgFeignClients
@EnableSpgResourceServer
@EnableDiscoveryClient
@SpringBootApplication
public class SpgFlowAnalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpgFlowAnalysisApplication.class, args);
    }

}
