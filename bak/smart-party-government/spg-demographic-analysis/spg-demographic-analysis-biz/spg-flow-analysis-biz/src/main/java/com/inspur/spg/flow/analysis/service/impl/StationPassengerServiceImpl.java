package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.entity.StationPassenger;
import com.inspur.spg.flow.analysis.api.vo.StationPassengerVO;
import com.inspur.spg.flow.analysis.mapper.StationPassengerMapper;
import com.inspur.spg.flow.analysis.service.StationPassengerService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站旅客洞察-旅客量(StationPassenger)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class StationPassengerServiceImpl extends BasePositionServiceImpl<StationPassengerMapper, StationPassenger> implements StationPassengerService {

    @Override
    public boolean fastSaveIgnoreBatch(List<StationPassenger> list) {
        for (StationPassenger entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

    @Override
    public StationPassengerVO getPassengerNum(BasePositionDTO positionDto) {
        return baseMapper.getTotalNum(positionDto);
    }

    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfoGroupByStatDate(BasePositionDTO positionDTO) {
        return baseMapper.getChartsInfoGroupByStatDate(positionDTO);
    }


}