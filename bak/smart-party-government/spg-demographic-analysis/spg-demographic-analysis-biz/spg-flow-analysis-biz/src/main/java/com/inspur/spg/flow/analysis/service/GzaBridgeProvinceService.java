package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeProvince;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 港珠澳大桥客流洞察-归属省份分布(GzaBridgeProvince)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
public interface GzaBridgeProvinceService extends ICommonService<GzaBridgeProvince> {

}