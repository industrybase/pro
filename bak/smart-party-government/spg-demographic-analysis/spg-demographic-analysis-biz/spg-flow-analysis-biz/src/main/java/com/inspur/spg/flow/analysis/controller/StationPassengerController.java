package com.inspur.spg.flow.analysis.controller;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.dto.MigrantHistoricalTrendDTO;
import com.inspur.spg.flow.analysis.api.vo.StationPassengerVO;
import com.inspur.spg.flow.analysis.service.StationPassengerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/*
 * @Author CJ
 * @Description 场站旅客Ctl
 * @Date 2022/3/10 16:36
 **/
@RestController
@RequestMapping("/stationPassenger")
@Api(value = "stationPassenger", tags = "场站旅客模块")
public class StationPassengerController {

    @Resource
    private StationPassengerService stationPassengerService;

    /**
     * @Author CJ
     * @Description 获取旅客量
     * @Date 2022/2/11 16:39
     * @param
     * @return com.inspur.spg.common.core.util.R<java.util.Map<java.lang.String,java.lang.Integer>>
     **/
    @GetMapping("/getNum")
    @ApiOperation(value = "获取旅客量", notes = "获取旅客量")
    public R<StationPassengerVO> getPassengerNum(@Valid BasePositionDTO positionDto){
        if(positionDto.getStatDate()  == null){
            return R.failed("查询日期不能为空");
        }
        return R.ok(stationPassengerService.getPassengerNum(positionDto));
    }

    /**
     * 获取统计趋势
     * @param positionDTO
     * @return
     */
    @GetMapping("/getChartInfo")
    @ApiOperation(value = "获取统计趋势", notes = "获取统计趋势")
    public R<List<ChartsInfo<BigDecimal>>> getChartInfo(@Valid BasePositionDTO positionDTO){
        if(positionDTO.getStartDate() == null || positionDTO.getEndDate() == null){
            return R.failed("查询开始日期和结束日期不能为空");
        }
        return R.ok(stationPassengerService.getChartsInfoGroupByStatDate(positionDTO));
    }

}
