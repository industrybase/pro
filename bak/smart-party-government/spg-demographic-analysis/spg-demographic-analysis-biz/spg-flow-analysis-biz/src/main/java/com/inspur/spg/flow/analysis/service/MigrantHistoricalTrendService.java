package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.MigrantHistoricalTrendDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantHistoricalTrend;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.vo.MigrantHistoricalTrendVO;

import java.math.BigDecimal;
import java.util.List;

/**
 * 人口迁徙-历史迁徙趋势(MigrantHistoricalTrend)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
public interface MigrantHistoricalTrendService extends ICommonService<MigrantHistoricalTrend> {

    /**
     * 获取迁徙情况
     * @param baseMigrantDto
     * @return
     */
    MigrantHistoricalTrendVO getMigrantInfo(MigrantHistoricalTrendDTO baseMigrantDto);

    /**
     * 查询单个
     * @param baseMigrantDto
     * @return
     */
    MigrantHistoricalTrend getOneByDto(MigrantHistoricalTrendDTO baseMigrantDto);

    /**
     * 查询统计数据
     * @param baseMigrantDto
     * @return
     */
    List<ChartsInfo<BigDecimal>> getChartsInfo(MigrantHistoricalTrendDTO baseMigrantDto);
}