package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.flow.analysis.api.entity.TollSex;
import com.inspur.spg.common.mybatis.service.ICommonService;

/**
 * 收费站热力客流画像-性别(TollSex)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:52:43
 */
public interface TollSexService extends ICommonService<TollSex> {

}