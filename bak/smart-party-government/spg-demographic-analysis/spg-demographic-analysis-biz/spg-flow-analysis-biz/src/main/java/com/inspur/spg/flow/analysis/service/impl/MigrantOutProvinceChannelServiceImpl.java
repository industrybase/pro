package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutProvinceChannel;
import com.inspur.spg.flow.analysis.mapper.MigrantOutProvinceChannelMapper;
import com.inspur.spg.flow.analysis.service.MigrantOutProvinceChannelService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * 人口迁徙-省际迁出去向及渠道(MigrantProvinceChannel)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class MigrantOutProvinceChannelServiceImpl extends CommonServiceImpl<MigrantOutProvinceChannelMapper, MigrantOutProvinceChannel> implements MigrantOutProvinceChannelService {

    @Override
    public int getTotalNum(BaseMigrantDTO migrantDto) {
        return baseMapper.getTotalNum(migrantDto);
    }

    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfo(BaseMigrantDTO migrantDto){
        return baseMapper.getChartsInfoGroupByOutArea(migrantDto);
    }
}