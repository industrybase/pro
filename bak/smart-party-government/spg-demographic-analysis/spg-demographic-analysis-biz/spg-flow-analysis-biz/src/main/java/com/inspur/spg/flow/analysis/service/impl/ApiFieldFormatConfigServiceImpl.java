package com.inspur.spg.flow.analysis.service.impl;

import cn.hutool.core.bean.DynaBean;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.core.util.SpringContextHolder;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.ApiConfig;
import com.inspur.spg.flow.analysis.api.entity.ApiFieldFormatConfig;
import com.inspur.spg.flow.analysis.mapper.ApiFieldFormatConfigMapper;
import com.inspur.spg.flow.analysis.service.ApiConfigService;
import com.inspur.spg.flow.analysis.service.ApiFieldFormatConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 * API接口字段转换配置(ApiFieldFormatConfig)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 16:15:47
 */
@Service
@Slf4j
public class ApiFieldFormatConfigServiceImpl extends ServiceImpl<ApiFieldFormatConfigMapper, ApiFieldFormatConfig> implements ApiFieldFormatConfigService {

    @Resource
    private ApiConfigService apiConfigService;

    @Override
    public List<ApiFieldFormatConfig> getListByApiId(Integer apiId) {
        return this.baseMapper.selectList(Wrappers.<ApiFieldFormatConfig>lambdaQuery().eq(ApiFieldFormatConfig::getApiId,apiId));
    }

    @Override
    @Transactional
    public void saveByConfigField(Map<String, Object> map) throws ClassNotFoundException {
        String apiName = (String) map.get("apiName");
        ApiConfig apiConfig = apiConfigService.getByApiName(apiName);
        List<Map<String,Object>> dataList = (List<Map<String, Object>>) map.get("data");
        List<ApiFieldFormatConfig> apiFieldFormatConfigList = getListByApiId(apiConfig.getId());
        //通过反射机制读取类，根据数据库读取进行新增
        Class apiClass = Class.forName(apiConfig.getPackagePath());
        String beanName = apiConfig.getBeanName();
        ICommonService service = SpringContextHolder.getBean(beanName);
        List<?> saveList = new ArrayList<>();
        for(Map<String,Object> dataMap : dataList){
            //读取类对象
            DynaBean dynaBean = DynaBean.create(apiClass);
            for(ApiFieldFormatConfig apiFieldFormatConfig : apiFieldFormatConfigList){
                try{
                    Object value = dataMap.get(apiFieldFormatConfig.getApiFieldName());
                    if(apiFieldFormatConfig.getFieldType().equals("date") && apiFieldFormatConfig.getApiFieldType().equals("string")){
                        Date dateValue = DateUtil.parse(value.toString(),apiFieldFormatConfig.getApiFieldFormat());
                        if(dateValue != null) {
                            value = DateUtil.format(dateValue, apiFieldFormatConfig.getFieldFormat());
                        }
                    }
                    dynaBean.set(apiFieldFormatConfig.getFieldName(),value);
                }catch (Exception e){
                    log.error("接口数据转换异常：{}",e.getLocalizedMessage());
                }
            }
            dynaBean.set("createTime",new Date());
            saveList.add(dynaBean.getBean());
        }
        //批量新增
        service.fastSaveIgnoreBatch(saveList);
    }

}