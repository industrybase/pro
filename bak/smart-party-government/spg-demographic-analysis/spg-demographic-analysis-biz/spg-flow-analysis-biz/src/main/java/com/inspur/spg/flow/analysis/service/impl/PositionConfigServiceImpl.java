package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.flow.analysis.api.entity.PositionConfig;
import com.inspur.spg.flow.analysis.mapper.PositionConfigMapper;
import com.inspur.spg.flow.analysis.service.PositionConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 枢纽阀值列表(PostionWarning)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class PositionConfigServiceImpl extends ServiceImpl<PositionConfigMapper, PositionConfig> implements PositionConfigService {

    @Override
    public PositionConfig getByName(String name) {
        return this.getOne(Wrappers.<PositionConfig>lambdaQuery().eq(PositionConfig::getName,name).eq(PositionConfig::getIsEnable, CommonConstants.ENABLE));
    }

    @Override
    public List<PositionConfig> getAll() {
        return list(Wrappers.<PositionConfig>lambdaQuery().eq(PositionConfig::getIsEnable,CommonConstants.ENABLE));
    }

    @Override
    public List<PositionConfig> getAllByTypeId(Integer typeId) {
        return list(Wrappers.<PositionConfig>lambdaQuery().eq(PositionConfig::getIsEnable,CommonConstants.ENABLE).eq(PositionConfig::getTypeId,typeId));
    }

}