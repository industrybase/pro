package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.mapper.StationProvinceOriginMapper;
import com.inspur.spg.flow.analysis.api.entity.StationProvinceOrigin;
import com.inspur.spg.flow.analysis.service.StationProvinceOriginService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 场站旅客洞察-省内来源地(StationProvinceOrigin)表服务实现类
 *
 * @author CJ
 * @since 2022-02-11 12:38:22
 */
@Service
public class StationProvinceOriginServiceImpl extends BasePositionServiceImpl<StationProvinceOriginMapper, StationProvinceOrigin> implements StationProvinceOriginService {

    @Override
    public boolean fastSaveIgnoreBatch(List<StationProvinceOrigin> list) {
        for (StationProvinceOrigin entity : list) {
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }


    @Override
    public List<ChartsInfo<BigDecimal>> getChartsInfo(BasePositionDTO positionDto) {
        return baseMapper.getChartsInfoGroupByAreaName(positionDto);
    }

    @Override
    public int getTotalNum(BasePositionDTO positionDto) {
        return baseMapper.getTotalNum(positionDto);
    }

}