package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.mapper.MigrantRefChannelCityMapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantRefChannelCity;
import com.inspur.spg.flow.analysis.service.MigrantRefChannelCityService;
import org.springframework.stereotype.Service;

/**
 * 关联表省内(MigrantRefChannelCity)表服务实现类
 *
 * @author CJ
 * @since 2022-02-10 11:30:00
 */
@Service
public class MigrantRefChannelCityServiceImpl extends CommonServiceImpl<MigrantRefChannelCityMapper, MigrantRefChannelCity> implements MigrantRefChannelCityService {

}