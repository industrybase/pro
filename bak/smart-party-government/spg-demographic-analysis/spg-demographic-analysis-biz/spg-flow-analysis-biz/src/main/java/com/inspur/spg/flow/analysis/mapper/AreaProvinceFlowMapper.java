package com.inspur.spg.flow.analysis.mapper;

import com.inspur.spg.common.core.entity.ChartsInfo;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.AreaProvinceFlow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 省客流实时数据（5分钟）(AreaProvinceFlow)表数据库访问层
 *
 * @author CJ
 * @since 2022-02-07 11:33:37
 */
@Mapper
public interface AreaProvinceFlowMapper extends CommonMapper<AreaProvinceFlow> {

    /**
     * 查询统计数据
     * @param statDate 统计日期
     * @param area 城市名称
     * @param statTimes 当天的所有时间整点
     * @return
     */
    List<ChartsInfo<BigDecimal>> getChartsInfo(@Param("statDate") Date statDate,@Param("area") String area,@Param("statTimes") List<Date> statTimes);



}