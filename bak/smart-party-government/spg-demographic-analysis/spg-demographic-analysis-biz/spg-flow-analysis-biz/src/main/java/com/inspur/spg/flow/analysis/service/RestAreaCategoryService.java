package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.RestAreaCategory;

/**
 * 服务区客流画像-归属类别(RestAreaCategory)表服务接口
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
public interface RestAreaCategoryService extends ICommonService<RestAreaCategory> {

}