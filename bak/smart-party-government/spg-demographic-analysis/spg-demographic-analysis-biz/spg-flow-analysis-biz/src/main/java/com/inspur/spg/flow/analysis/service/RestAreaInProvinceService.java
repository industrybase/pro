package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.RestAreaInProvince;

/**
 * 服务区客流画像归属-省内(RestAreaInProvince)表服务接口
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
public interface RestAreaInProvinceService extends ICommonService<RestAreaInProvince> {

}