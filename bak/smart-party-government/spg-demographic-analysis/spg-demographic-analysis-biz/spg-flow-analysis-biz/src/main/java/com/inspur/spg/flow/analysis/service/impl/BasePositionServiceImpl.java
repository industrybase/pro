package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.BasePosition;
import com.inspur.spg.flow.analysis.api.entity.PositionConfig;
import com.inspur.spg.flow.analysis.service.PositionConfigService;

import javax.annotation.Resource;

public class BasePositionServiceImpl <M extends CommonMapper<T>, T> extends CommonServiceImpl<M,T>{

    @Resource
    private PositionConfigService positionConfigService;

    protected void getPositionByName(BasePosition basePosition){
        PositionConfig positionConfig = positionConfigService.getByName(basePosition.getPositionName());
        if(positionConfig == null){
            return;
        }
        basePosition.setPositionId(positionConfig.getCode());
        //basePosition.setPositionCityName(positionConfig.getCityName());
        basePosition.setPositionConfigId(positionConfig.getId());
        //basePosition.setPositionType(positionConfig.getTypeId().toString());
    }

}
