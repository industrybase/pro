package com.inspur.spg.flow.analysis.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.entity.ApiFieldFormatConfig;
import com.inspur.spg.flow.analysis.api.entity.PositionConfig;
import com.inspur.spg.flow.analysis.service.PositionConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/13 14:32
 **/
@RestController
@RequestMapping("/positionConfig")
@Api(value = "positionConfig", tags = "枢纽配置信息模块")
public class PositionConfigController {

    @Resource
    private PositionConfigService positionConfigService;

    /**
     * @Author CJ
     * @Description 查询所有
     * @Date 2022/1/13 15:01
     * @param
     * @return com.inspur.spg.common.core.util.R<java.util.List<com.inspur.spg.flow.analysis.api.entity.PositionWarning>>
     **/
    @GetMapping("/getAll")
    @ApiOperation(value = "查询所有数据", notes = "查询所有数据")
    public R<List<PositionConfig>> getAll(){
        return R.ok(positionConfigService.list(Wrappers.<PositionConfig>lambdaQuery().eq(PositionConfig::getIsEnable, CommonConstants.ENABLE)));
    }



}
