package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.GzaBridgeAvgTime;
import com.inspur.spg.flow.analysis.mapper.GzaBridgeAvgTimeMapper;
import com.inspur.spg.flow.analysis.service.GzaBridgeAvgTimeService;
import com.inspur.spg.flow.analysis.service.PositionConfigService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 港珠澳大桥客流洞察-时点通关时长(GzaBridgeAvgTime)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 09:52:25
 */
@Service
public class GzaBridgeAvgTimeServiceImpl extends BasePositionServiceImpl<GzaBridgeAvgTimeMapper, GzaBridgeAvgTime> implements GzaBridgeAvgTimeService {

    @Resource
    private PositionConfigService positionConfigService;

    @Override
    public boolean fastSaveIgnoreBatch(List<GzaBridgeAvgTime> list) {
        for (GzaBridgeAvgTime entity : list) {
            getPositionByName(entity);
        }
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}