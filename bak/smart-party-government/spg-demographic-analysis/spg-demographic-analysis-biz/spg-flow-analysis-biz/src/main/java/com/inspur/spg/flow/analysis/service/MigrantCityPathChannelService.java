package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.flow.analysis.api.dto.BaseMigrantDTO;
import com.inspur.spg.flow.analysis.api.entity.MigrantCityPathChannel;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.vo.MigrantCityPathAllChannelVO;
import com.inspur.spg.flow.analysis.api.vo.MigrantCityRankVO;

import java.util.List;

/**
 * 人口迁徙-省内路线及渠道迁徙(MigrantCityPathChannel)表服务接口
 *
 * @author CJ
 * @since 2022-01-06 10:08:42
 */
public interface MigrantCityPathChannelService extends ICommonService<MigrantCityPathChannel> {

    /**
     * 获取省内地市迁徙排名数据
     * @param migrantDto
     * @return
     */
    MigrantCityRankVO getRank(BaseMigrantDTO migrantDto);

    /**
     * 获取所有渠道数
     * @param migrantDto
     * @return
     */
    List<MigrantCityPathAllChannelVO> getAllChannelList(BaseMigrantDTO migrantDto);

}