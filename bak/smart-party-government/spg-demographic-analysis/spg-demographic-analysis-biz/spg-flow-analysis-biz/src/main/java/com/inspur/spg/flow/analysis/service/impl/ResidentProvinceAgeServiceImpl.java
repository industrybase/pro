package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.ResidentProvinceAge;
import com.inspur.spg.flow.analysis.mapper.ResidentProvinceAgeMapper;
import com.inspur.spg.flow.analysis.service.ResidentProvinceAgeService;
import org.springframework.stereotype.Service;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;
import com.inspur.spg.common.mybatis.service.CommonServiceImpl;

/**
 * 人口洞察-地市常驻人口(ResidentProvinceAge)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class ResidentProvinceAgeServiceImpl extends CommonServiceImpl<ResidentProvinceAgeMapper, ResidentProvinceAge> implements ResidentProvinceAgeService {

}