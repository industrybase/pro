package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.MigrantType;
import org.apache.ibatis.annotations.Mapper;

/**
 * 人口迁徙-类别(MigrantType)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Mapper
public interface MigrantTypeMapper extends CommonMapper<MigrantType> {

}