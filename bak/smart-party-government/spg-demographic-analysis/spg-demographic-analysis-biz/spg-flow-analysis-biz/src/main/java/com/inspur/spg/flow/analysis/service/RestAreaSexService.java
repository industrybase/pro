package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.RestAreaSex;

/**
 * 服务区客流画像-性别(RestAreaSex)表服务接口
 *
 * @author CJ
 * @since 2022-01-31 09:36:37
 */
public interface RestAreaSexService extends ICommonService<RestAreaSex> {

}