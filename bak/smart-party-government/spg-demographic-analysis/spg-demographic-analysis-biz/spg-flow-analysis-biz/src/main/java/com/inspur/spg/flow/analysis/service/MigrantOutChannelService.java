package com.inspur.spg.flow.analysis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.common.mybatis.service.ICommonService;
import com.inspur.spg.flow.analysis.api.entity.MigrantOutChannel;

/**
 * 迁出渠道分布(MigrantOutChannel)表服务接口
 *
 * @author CJ
 * @since 2022-02-10 11:28:01
 */
public interface MigrantOutChannelService extends ICommonService<MigrantOutChannel> {

}