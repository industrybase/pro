package com.inspur.spg.flow.analysis.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.flow.analysis.api.entity.ApiConfig;
import com.inspur.spg.flow.analysis.mapper.ApiConfigMapper;
import com.inspur.spg.flow.analysis.service.ApiConfigService;
import org.springframework.stereotype.Service;

/**
 * API接口配置(ApiDbTableConfig)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 16:15:45
 */
@Service
public class ApiConfigServiceImpl extends ServiceImpl<ApiConfigMapper, ApiConfig> implements ApiConfigService {

    @Override
    public ApiConfig getByApiName(String apiName) {
        return this.baseMapper.selectOne(Wrappers.<ApiConfig>lambdaQuery().eq(ApiConfig::getApiName, apiName));
    }

}