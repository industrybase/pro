package com.inspur.spg.flow.analysis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.common.mybatis.mapper.CommonMapper;
import com.inspur.spg.flow.analysis.api.entity.GzaBridgeNation;
import org.apache.ibatis.annotations.Mapper;

/**
 * 港珠澳大桥客流洞察-归属国家分布(GzaBridgeNation)表数据库访问层
 *
 * @author CJ
 * @since 2022-01-06 09:52:27
 */
@Mapper
public interface GzaBridgeNationMapper extends CommonMapper<GzaBridgeNation> {

}