package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.QzStraitSex;
import com.inspur.spg.flow.analysis.mapper.QzStraitSexMapper;
import com.inspur.spg.flow.analysis.service.QzStraitSexService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 琼州海峡客流性别(QzStraitSex)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class QzStraitSexServiceImpl extends BasePositionServiceImpl<QzStraitSexMapper, QzStraitSex> implements QzStraitSexService {


    @Override
    public boolean fastSaveIgnoreBatch(List<QzStraitSex> list) {
        for (QzStraitSex entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}