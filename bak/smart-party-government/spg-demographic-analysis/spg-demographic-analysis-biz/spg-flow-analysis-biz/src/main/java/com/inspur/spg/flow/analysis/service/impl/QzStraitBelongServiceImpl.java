package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.QzStraitBelong;
import com.inspur.spg.flow.analysis.mapper.QzStraitBelongMapper;
import com.inspur.spg.flow.analysis.service.QzStraitBelongService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 琼州海峡客流归属地(QzStraitBelong)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class QzStraitBelongServiceImpl extends BasePositionServiceImpl<QzStraitBelongMapper, QzStraitBelong> implements QzStraitBelongService {


    @Override
    public boolean fastSaveIgnoreBatch(List<QzStraitBelong> list) {
        for (QzStraitBelong entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}