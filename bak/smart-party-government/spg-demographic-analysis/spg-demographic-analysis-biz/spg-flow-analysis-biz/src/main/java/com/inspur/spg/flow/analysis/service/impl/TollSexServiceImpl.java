package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.TollSex;
import com.inspur.spg.flow.analysis.mapper.TollSexMapper;
import com.inspur.spg.flow.analysis.service.TollSexService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 收费站热力客流画像-性别(TollSex)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:52:43
 */
@Service
public class TollSexServiceImpl extends BasePositionServiceImpl<TollSexMapper, TollSex> implements TollSexService {


    @Override
    public boolean fastSaveIgnoreBatch(List<TollSex> list) {
        for (TollSex entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}