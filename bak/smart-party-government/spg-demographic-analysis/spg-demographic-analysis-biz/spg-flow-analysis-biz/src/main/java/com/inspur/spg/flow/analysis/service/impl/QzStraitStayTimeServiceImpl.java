package com.inspur.spg.flow.analysis.service.impl;

import com.inspur.spg.flow.analysis.api.entity.QzStraitStayTime;
import com.inspur.spg.flow.analysis.mapper.QzStraitStayTimeMapper;
import com.inspur.spg.flow.analysis.service.QzStraitStayTimeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 琼州海峡客流驻留时长(QzStraitStayTime)表服务实现类
 *
 * @author CJ
 * @since 2022-01-06 10:08:46
 */
@Service
public class QzStraitStayTimeServiceImpl extends BasePositionServiceImpl<QzStraitStayTimeMapper, QzStraitStayTime> implements QzStraitStayTimeService {

    @Override
    public boolean fastSaveIgnoreBatch(List<QzStraitStayTime> list) {
        for (QzStraitStayTime entity : list) {
            getPositionByName(entity);
		}
        list = list.stream().filter(item -> item.getPositionId() != null).collect(Collectors.toList());
        return super.fastSaveIgnoreBatch(list);
    }

}