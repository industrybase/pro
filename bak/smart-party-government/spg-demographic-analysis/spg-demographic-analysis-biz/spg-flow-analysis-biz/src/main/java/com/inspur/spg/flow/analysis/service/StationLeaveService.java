package com.inspur.spg.flow.analysis.service;

import com.inspur.spg.flow.analysis.api.dto.BasePositionDTO;
import com.inspur.spg.flow.analysis.api.vo.PopulationProportionStatsVO;

/**
 * @Author CJ
 * @Description 场站去向
 * @Date 2022/2/11 18:17
 **/
public interface StationLeaveService {

    /**
     * @Author CJ
     * @Description 获取人口信息
     * @Date 2022/2/11 18:18
     * @param positionDto
     * @return java.util.Map<java.lang.String,java.lang.Object>
     **/
    PopulationProportionStatsVO getPopulationStats(BasePositionDTO positionDto);

}
