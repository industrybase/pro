package com.inspur.spg.gateway;

import com.inspur.spg.common.swagger.annotation.EnableSpgSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/28 17:03
 **/
@EnableDiscoveryClient
@SpringBootApplication
@EnableSpgSwagger2
public class SpgGateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpgGateWayApplication.class, args);
    }


}
