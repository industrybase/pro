/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.inspur.spg.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.inspur.spg.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 行政区域表
 *
 * @author CJ
 * @date 2022-02-28 14:37:27
 */
@Data
@TableName("sys_area")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "行政区域表")
public class SysArea extends BaseEntity {

    /**
     * 区域ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="区域ID")
    private Long areaId;

    /**
     * 区域名称
     */
    @ApiModelProperty(value="区域名称")
    private String areaName;

    /**
     * 父级区域ID
     */
    @ApiModelProperty(value="父级区域ID")
    private Long parentId;

    /**
     * 区域等级
     */
    @ApiModelProperty(value="区域等级")
    private Integer level;


}
