package com.inspur.spg.admin.api.feign;

import com.inspur.spg.admin.api.dto.BizUserInfo;
import com.inspur.spg.admin.api.entity.BizUser;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.constant.ServiceNameConstants;
import com.inspur.spg.common.core.util.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @Author CJ
 * @Date 2022/2/28 20:32
 * @Description:
 */
@FeignClient(contextId = "remoteBizUserService", value = ServiceNameConstants.UMPS_SERVICE)
public interface RemoteBizUserService {

    @GetMapping("/biz/user/info/{username}")
    R<BizUserInfo> info(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM) String from);

}
