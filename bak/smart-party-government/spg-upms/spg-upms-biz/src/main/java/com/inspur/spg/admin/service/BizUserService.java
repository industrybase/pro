package com.inspur.spg.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.inspur.spg.admin.api.dto.BizUserDTO;
import com.inspur.spg.admin.api.dto.BizUserInfo;
import com.inspur.spg.admin.api.dto.UserDTO;
import com.inspur.spg.admin.api.dto.UserInfo;
import com.inspur.spg.admin.api.entity.BizUser;
import com.inspur.spg.admin.api.entity.SysUser;
import com.inspur.spg.admin.api.vo.BizUserExcelVO;
import com.inspur.spg.admin.api.vo.BizUserVO;
import com.inspur.spg.admin.api.vo.UserExcelVO;
import com.inspur.spg.admin.api.vo.UserVO;
import com.inspur.spg.common.core.util.R;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Set;

/**
 * @Author CJ
 * @Date 2022/2/28 20:29
 * @Description: 业务用户
 */
public interface BizUserService extends IService<BizUser> {

    /**
     * 查询用户信息
     * @param user 用户
     * @return userInfo
     */
    BizUserInfo getUserInfo(BizUser user);

    /**
     * 分页查询用户信息（含有角色信息）
     * @param page 分页对象
     * @param userDTO 参数列表
     * @return
     */
    IPage<List<BizUserVO>> getUserWithRolePage(Page page, BizUserDTO userDTO);

    /**
     * 删除用户
     * @param user 用户
     * @return boolean
     */
    Boolean removeUserById(BizUser user);

    /**
     * 更新当前用户基本信息
     * @param userDto 用户信息
     * @return Boolean 操作成功返回true,操作失败返回false
     */
    Boolean updateUserInfo(BizUserDTO userDto);

    /**
     * 更新指定用户信息
     * @param userDto 用户信息
     * @return
     */
    Boolean updateUser(BizUserDTO userDto);

    /**
     * 通过ID查询用户信息
     * @param id 用户ID
     * @return 用户信息
     */
    BizUserVO getUserVoById(Long id);

    /**
     * 查询上级部门的用户信息
     * @param username 用户名
     * @return R
     */
    List<BizUser> listAncestorUsersByUsername(String username);

    /**
     * 保存用户信息
     * @param userDto DTO 对象
     * @return success/fail
     */
    Boolean saveUser(BizUserDTO userDto);

    /**
     * 查询全部的用户
     * @param userDTO 查询条件
     * @return list
     */
    List<BizUserExcelVO> listUser(BizUserDTO userDTO);

    /**
     * excel 导入用户
     * @param excelVOList excel 列表数据
     * @param bindingResult 错误数据
     * @return ok fail
     */
    R importUser(List<BizUserExcelVO> excelVOList, BindingResult bindingResult);

    /**
     * 根据部门 id 列表查询对应的用户 id 集合
     * @param deptIds 部门 id 列表
     * @return userIdList
     */
    List<Long> listUserIdByDeptIds(Set<Long> deptIds);

}
