/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.inspur.spg.admin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.inspur.spg.admin.api.entity.SysArea;
import com.inspur.spg.admin.service.SysAreaService;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.log.annotation.SysLog;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 行政区域表
 *
 * @author CJ
 * @date 2022-02-28 14:37:27
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/area" )
@Api(value = "area", tags = "行政区域管理")
public class AreaController {

    private final SysAreaService sysAreaService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sysArea 行政区域表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('sys_area_get')" )
    public R getSysAreaPage(Page page, SysArea sysArea) {
        return R.ok(sysAreaService.page(page, Wrappers.query(sysArea)));
    }


    /**
     * 通过id查询行政区域表
     * @param areaId id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{areaId}" )
    @PreAuthorize("@pms.hasPermission('sys_area_get')" )
    public R getById(@PathVariable("areaId" ) Long areaId) {
        return R.ok(sysAreaService.getById(areaId));
    }

    /**
     * 新增行政区域表
     * @param sysArea 行政区域表
     * @return R
     */
    @ApiOperation(value = "新增行政区域表", notes = "新增行政区域表")
    @SysLog("新增行政区域表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('sys_area_add')" )
    public R save(@RequestBody SysArea sysArea) {
        return R.ok(sysAreaService.save(sysArea));
    }

    /**
     * 修改行政区域表
     * @param sysArea 行政区域表
     * @return R
     */
    @ApiOperation(value = "修改行政区域表", notes = "修改行政区域表")
    @SysLog("修改行政区域表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('sys_area_edit')" )
    public R updateById(@RequestBody SysArea sysArea) {
        return R.ok(sysAreaService.updateById(sysArea));
    }

    /**
     * 通过id删除行政区域表
     * @param areaId id
     * @return R
     */
    @ApiOperation(value = "通过id删除行政区域表", notes = "通过id删除行政区域表")
    @SysLog("通过id删除行政区域表" )
    @DeleteMapping("/{areaId}" )
    @PreAuthorize("@pms.hasPermission('sys_area_del')" )
    public R removeById(@PathVariable Long areaId) {
        return R.ok(sysAreaService.removeById(areaId));
    }

}
