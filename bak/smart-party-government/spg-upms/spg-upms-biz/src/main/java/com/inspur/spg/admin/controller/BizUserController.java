package com.inspur.spg.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.inspur.spg.admin.api.dto.BizUserDTO;
import com.inspur.spg.admin.api.dto.BizUserInfo;
import com.inspur.spg.admin.api.entity.BizUser;
import com.inspur.spg.admin.api.vo.BizUserExcelVO;
import com.inspur.spg.admin.api.vo.BizUserInfoVO;
import com.inspur.spg.admin.api.vo.BizUserVO;
import com.inspur.spg.admin.service.BizUserService;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.log.annotation.SysLog;
import com.inspur.spg.common.security.annotation.Inner;
import com.inspur.spg.common.security.util.SecurityUtils;
import com.pig4cloud.plugin.excel.annotation.RequestExcel;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * @Author CJ
 * @Date 2022/2/28 20:32 （可以根据需要修改）
 * @Description:
 */

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/28 20:32
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping("/biz/user")
@Api(value = "bizUser", tags = "业务用户管理模块")
public class BizUserController {

    private final BizUserService userService;

    /**
     * 获取当前用户全部信息
     * @return 用户信息
     */
    @GetMapping(value = { "/info" })
    public R<BizUserInfoVO> info() {
        String username = SecurityUtils.getUser().getUsername();
        BizUser user = userService.getOne(Wrappers.<BizUser>query().lambda().eq(BizUser::getUsername, username));
        if (user == null) {
            return R.failed("获取当前用户信息失败");
        }
        BizUserInfo userInfo = userService.getUserInfo(user);
        BizUserInfoVO vo = new BizUserInfoVO();
        vo.setBizUser(userInfo.getBizUser());
        vo.setRoles(userInfo.getRoles());
        vo.setPermissions(userInfo.getPermissions());
        return R.ok(vo);
    }

    /**
     * @Author CJ
     * @Description 根据用户名查询业务用户
     * @Date 2022/2/28 20:34
     * @param username
     **/
    @Inner
    @GetMapping("/info/{username}")
    public R<BizUserInfo> info(@PathVariable String username) {
        BizUser user = userService.getOne(Wrappers.<BizUser>lambdaQuery()
                .eq(BizUser::getUsername, username));
        if (user == null) {
            return R.failed(String.format("用户信息为空 %s", username));
        }
        return R.ok(userService.getUserInfo(user));
    }


    /**
     * 根据部门id，查询对应的用户 id 集合
     * @param deptIds 部门id 集合
     * @return 用户 id 集合
     */
    @Inner
    @GetMapping("/ids")
    public R<List<Long>> listUserIdByDeptIds(@RequestParam("deptIds") Set<Long> deptIds) {
        return R.ok(userService.listUserIdByDeptIds(deptIds));
    }

    /**
     * 通过ID查询用户信息
     * @param id ID
     * @return 用户信息
     */
    @GetMapping("/{id:\\d+}")
    public R<BizUserVO> user(@PathVariable Long id) {
        return R.ok(userService.getUserVoById(id));
    }

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return
     */
    @GetMapping("/details/{username}")
    public R<BizUser> user(@PathVariable String username) {
        BizUser condition = new BizUser();
        condition.setUsername(username);
        return R.ok(userService.getOne(new QueryWrapper<>(condition)));
    }

    /**
     * 删除用户信息
     * @param id ID
     * @return R
     */
    @SysLog("删除用户信息")
    @DeleteMapping("/{id:\\d+}")
    @PreAuthorize("@pms.hasPermission('biz_user_del')")
    public R<Boolean> userDel(@PathVariable Long id) {
        BizUser user = userService.getById(id);
        return R.ok(userService.removeUserById(user));
    }

    /**
     * 添加用户
     * @param userDto 用户信息
     * @return success/false
     */
    @SysLog("添加用户")
    @PostMapping
    @PreAuthorize("@pms.hasPermission('biz_user_add')")
    public R<Boolean> user(@RequestBody BizUserDTO userDto) {
        return R.ok(userService.saveUser(userDto));
    }

    /**
     * 更新用户信息
     * @param userDto 用户信息
     * @return R
     */
    @SysLog("更新用户信息")
    @PutMapping
    @PreAuthorize("@pms.hasPermission('biz_user_edit')")
    public R<Boolean> updateUser(@Valid @RequestBody BizUserDTO userDto) {
        return R.ok(userService.updateUser(userDto));
    }

    /**
     * 分页查询用户
     * @param page 参数集
     * @param userDTO 查询参数列表
     * @return 用户集合
     */
    @GetMapping("/page")
    public R<IPage<List<BizUserVO>>> getUserPage(Page page, BizUserDTO userDTO) {
        return R.ok(userService.getUserWithRolePage(page, userDTO));
    }

    /**
     * 修改个人信息
     * @param userDto userDto
     * @return success/false
     */
    @SysLog("修改个人信息")
    @PutMapping("/edit")
    public R<Boolean> updateUserInfo(@Valid @RequestBody BizUserDTO userDto) {
        return R.ok(userService.updateUserInfo(userDto));
    }

    /**
     * @param username 用户名称
     * @return 上级部门用户列表
     */
    @GetMapping("/ancestor/{username}")
    public R<List<BizUser>> listAncestorUsers(@PathVariable String username) {
        return R.ok(userService.listAncestorUsersByUsername(username));
    }

    /**
     * 导出excel 表格
     * @param userDTO 查询条件
     * @return
     */
    @ResponseExcel
    @GetMapping("/export")
    @PreAuthorize("@pms.hasPermission('biz_user_import_export')")
    public List<BizUserExcelVO> export(BizUserDTO userDTO) {
        return userService.listUser(userDTO);
    }

    /**
     * 导入用户
     * @param excelVOList 用户列表
     * @param bindingResult 错误信息列表
     * @return R
     */
    @PostMapping("/import")
    @PreAuthorize("@pms.hasPermission('biz_user_import_export')")
    public R importUser(@RequestExcel List<BizUserExcelVO> excelVOList, BindingResult bindingResult) {
        return userService.importUser(excelVOList, bindingResult);
    }

}
