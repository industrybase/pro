/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.spg.admin.controller;

import cn.hutool.core.lang.tree.Tree;
import com.inspur.spg.admin.api.entity.BizMenu;
import com.inspur.spg.admin.api.entity.SysMenu;
import com.inspur.spg.admin.service.BizMenuService;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.log.annotation.SysLog;
import com.inspur.spg.common.security.util.SecurityUtils;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author lengleng
 * @date 2017/10/31
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/biz/menu")
@Api(value = "bizMenu", tags = "业务菜单管理模块")
public class BizMenuController {

	private final BizMenuService bizMenuService;

	/**
	 * 返回当前用户的树形菜单集合
	 * @param parentId 父节点ID
	 * @return 当前用户的树形菜单
	 */
	@GetMapping
	public R<List<Tree<Long>>> getUserMenu(Long parentId) {
		// 获取符合条件的菜单
		Set<BizMenu> menuSet = SecurityUtils.getRoles(SecurityConstants.ROLE_BIZ).stream().map(bizMenuService::findMenuByRoleId)
				.flatMap(Collection::stream).collect(Collectors.toSet());
		return R.ok(bizMenuService.filterMenu(menuSet, parentId));
	}

	/**
	 * 返回树形菜单集合
	 * @param lazy 是否是懒加载
	 * @param parentId 父节点ID
	 * @return 树形菜单
	 */
	@GetMapping(value = "/tree")
	public R<List<Tree<Long>>> getTree(boolean lazy, Long parentId) {
		return R.ok(bizMenuService.treeMenu(lazy, parentId));
	}

	/**
	 * 返回角色的菜单集合
	 * @param roleId 角色ID
	 * @return 属性集合
	 */
	@GetMapping("/tree/{roleId}")
	public R<List<Long>> getRoleTree(@PathVariable Long roleId) {
		return R.ok(
				bizMenuService.findMenuByRoleId(roleId).stream().map(BizMenu::getMenuId).collect(Collectors.toList()));
	}

	/**
	 * 通过ID查询菜单的详细信息
	 * @param id 菜单ID
	 * @return 菜单详细信息
	 */
	@GetMapping("/{id:\\d+}")
	public R<BizMenu> getById(@PathVariable Long id) {
		return R.ok(bizMenuService.getById(id));
	}

	/**
	 * 新增菜单
	 * @param bizMenu 菜单信息
	 * @return 含ID 菜单信息
	 */
	@SysLog("新增菜单")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('biz_menu_add')")
	public R<BizMenu> save(@Valid @RequestBody BizMenu bizMenu) {
		bizMenuService.save(bizMenu);
		return R.ok(bizMenu);
	}

	/**
	 * 删除菜单
	 * @param id 菜单ID
	 * @return success/false
	 */
	@SysLog("删除菜单")
	@DeleteMapping("/{id:\\d+}")
	@PreAuthorize("@pms.hasPermission('biz_menu_del')")
	public R<Boolean> removeById(@PathVariable Long id) {
		return R.ok(bizMenuService.removeMenuById(id));
	}

	/**
	 * 更新菜单
	 * @param bizMenu
	 * @return
	 */
	@SysLog("更新菜单")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('biz_menu_edit')")
	public R<Boolean> update(@Valid @RequestBody BizMenu bizMenu) {
		return R.ok(bizMenuService.updateMenuById(bizMenu));
	}

	/**
	 * 清除菜单缓存
	 */
	@SysLog("清除菜单缓存")
	@DeleteMapping("/cache")
	@PreAuthorize("@pms.hasPermission('biz_menu_del')")
	public R clearMenuCache() {
		bizMenuService.clearMenuCache();
		return R.ok();
	}

}
