/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.spg.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.admin.api.entity.BizDept;
import com.inspur.spg.admin.api.entity.BizDeptRelation;
import com.inspur.spg.admin.api.entity.SysDept;
import com.inspur.spg.admin.api.entity.SysDeptRelation;
import com.inspur.spg.admin.mapper.BizDeptMapper;
import com.inspur.spg.admin.mapper.SysDeptMapper;
import com.inspur.spg.admin.service.BizDeptRelationService;
import com.inspur.spg.admin.service.BizDeptService;
import com.inspur.spg.admin.service.SysDeptRelationService;
import com.inspur.spg.admin.service.SysDeptService;
import com.inspur.spg.common.security.util.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 部门管理 服务实现类
 * </p>
 *
 * @author lengleng
 * @since 2019/2/1
 */
@Service
@RequiredArgsConstructor
public class BizDeptServiceImpl extends ServiceImpl<BizDeptMapper, BizDept> implements BizDeptService {

	private final BizDeptRelationService bizDeptRelationService;

	/**
	 * 添加信息部门
	 * @param dept 部门
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean saveDept(BizDept dept) {
		BizDept bizDept = new BizDept();
		BeanUtils.copyProperties(dept, bizDept);
		this.save(bizDept);
		bizDeptRelationService.saveDeptRelation(bizDept);
		return Boolean.TRUE;
	}

	/**
	 * 删除部门
	 * @param id 部门 ID
	 * @return 成功、失败
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean removeDeptById(Long id) {
		// 级联删除部门
		List<Long> idList = bizDeptRelationService
				.list(Wrappers.<BizDeptRelation>query().lambda().eq(BizDeptRelation::getAncestor, id)).stream()
				.map(BizDeptRelation::getDescendant).collect(Collectors.toList());

		if (CollUtil.isNotEmpty(idList)) {
			this.removeByIds(idList);
		}

		// 删除部门级联关系
		bizDeptRelationService.removeDeptRelationById(id);
		return Boolean.TRUE;
	}

	/**
	 * 更新部门
	 * @param bizDept 部门信息
	 * @return 成功、失败
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean updateDeptById(BizDept bizDept) {
		// 更新部门状态
		this.updateById(bizDept);
		// 更新部门关系
		BizDeptRelation relation = new BizDeptRelation();
		relation.setAncestor(bizDept.getParentId());
		relation.setDescendant(bizDept.getDeptId());
		bizDeptRelationService.updateDeptRelation(relation);
		return Boolean.TRUE;
	}

	@Override
	public List<Long> listChildDeptId(Long deptId) {
		List<BizDeptRelation> deptRelations = bizDeptRelationService.list(Wrappers.<BizDeptRelation>lambdaQuery()
				.eq(BizDeptRelation::getAncestor, deptId).ne(BizDeptRelation::getDescendant, deptId));
		if (CollUtil.isNotEmpty(deptRelations)) {
			return deptRelations.stream().map(BizDeptRelation::getDescendant).collect(Collectors.toList());
		}
		return new ArrayList<>();
	}

	/**
	 * 查询全部部门树
	 * @return 树
	 */
	@Override
	public List<Tree<Long>> listDeptTrees() {
		return getDeptTree(this.list(Wrappers.emptyWrapper()));
	}

	/**
	 * 查询用户部门树
	 * @return
	 */
	@Override
	public List<Tree<Long>> listCurrentUserDeptTrees() {
		Long deptId = SecurityUtils.getUser().getDeptId();
		List<Long> descendantIdList = bizDeptRelationService
				.list(Wrappers.<BizDeptRelation>query().lambda().eq(BizDeptRelation::getAncestor, deptId)).stream()
				.map(BizDeptRelation::getDescendant).collect(Collectors.toList());

		List<BizDept> deptList = baseMapper.selectBatchIds(descendantIdList);
		return getDeptTree(deptList);
	}

	/**
	 * 构建部门树
	 * @param depts 部门
	 * @return
	 */
	private List<Tree<Long>> getDeptTree(List<BizDept> depts) {
		List<TreeNode<Long>> collect = depts.stream().filter(dept -> dept.getDeptId().intValue() != dept.getParentId())
				.sorted(Comparator.comparingInt(BizDept::getSortOrder)).map(dept -> {
					TreeNode<Long> treeNode = new TreeNode();
					treeNode.setId(dept.getDeptId());
					treeNode.setParentId(dept.getParentId());
					treeNode.setName(dept.getName());
					treeNode.setWeight(dept.getSortOrder());
					// 扩展属性
					Map<String, Object> extra = new HashMap<>(4);
					extra.put("createTime", dept.getCreateTime());
					treeNode.setExtra(extra);
					return treeNode;
				}).collect(Collectors.toList());

		return TreeUtil.build(collect, 0L);
	}

}
