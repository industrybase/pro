package com.inspur.spg.admin.service.impl;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.inspur.spg.admin.api.dto.BizUserDTO;
import com.inspur.spg.admin.api.dto.BizUserInfo;
import com.inspur.spg.admin.api.dto.UserDTO;
import com.inspur.spg.admin.api.entity.*;
import com.inspur.spg.admin.api.vo.BizUserExcelVO;
import com.inspur.spg.admin.api.vo.BizUserVO;
import com.inspur.spg.admin.api.vo.UserExcelVO;
import com.inspur.spg.admin.api.vo.UserVO;
import com.inspur.spg.admin.mapper.*;
import com.inspur.spg.admin.service.BizMenuService;
import com.inspur.spg.admin.service.BizUserService;
import com.inspur.spg.common.core.constant.CacheConstants;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.constant.enums.MenuTypeEnum;
import com.inspur.spg.common.core.util.R;
import com.pig4cloud.plugin.excel.vo.ErrorMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/*
 * @Author CJ
 * @Description
 * @Date 2022/2/28 20:30
 **/
@Service
@RequiredArgsConstructor
public class BizUserServiceImpl  extends ServiceImpl<BizUserMapper, BizUser> implements BizUserService {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    private final BizRoleMapper bizRoleMapper;

    private final SysDeptMapper sysDeptMapper;

    private final BizMenuService bizMenuService;

    private final BizUserRoleMapper bizUserRoleMapper;

    /**
     * 保存用户信息
     * @param userDto DTO 对象
     * @return success/fail
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveUser(BizUserDTO userDto) {
        BizUser user = new BizUser();
        BeanUtils.copyProperties(userDto, user);
        user.setDelFlag(CommonConstants.STATUS_NORMAL);
        user.setPassword(ENCODER.encode(userDto.getPassword()));
        baseMapper.insert(user);
        userDto.getRole().stream().map(roleId -> {
            BizUserRole userRole = new BizUserRole();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(roleId);
            return userRole;
        }).forEach(bizUserRoleMapper::insert);
        return Boolean.TRUE;
    }

    @Override
    public BizUserInfo getUserInfo(BizUser user) {
        BizUserInfo userInfo = new BizUserInfo();
        userInfo.setBizUser(user);
        // 设置角色列表
        List<BizRole> roleList = bizRoleMapper.listRolesByUserId(user.getUserId());
        userInfo.setRoleList(roleList);
        // 设置角色列表 （ID）
        List<Long> roleIds = roleList.stream().map(BizRole::getRoleId).collect(Collectors.toList());
        userInfo.setRoles(ArrayUtil.toArray(roleIds, Long.class));
        // 设置权限列表（menu.permission）
        Set<String> permissions = roleIds.stream().map(bizMenuService::findMenuByRoleId).flatMap(Collection::stream)
                .filter(m -> MenuTypeEnum.BUTTON.getType().equals(m.getType())).map(BizMenu::getPermission)
                .filter(StrUtil::isNotBlank).collect(Collectors.toSet());
        userInfo.setPermissions(ArrayUtil.toArray(permissions, String.class));

        return userInfo;
    }

    /**
     * 分页查询用户信息（含有角色信息）
     * @param page 分页对象
     * @param userDTO 参数列表
     * @return
     */
    @Override
    public IPage<List<BizUserVO>> getUserWithRolePage(Page page, BizUserDTO userDTO) {
        return baseMapper.getUserVosPage(page, userDTO);
    }

    /**
     * 通过ID查询用户信息
     * @param id 用户ID
     * @return 用户信息
     */
    @Override
    public BizUserVO getUserVoById(Long id) {
        return baseMapper.getUserVoById(id);
    }

    /**
     * 删除用户
     * @param bizUser 用户
     * @return Boolean
     */
    @Override
    @CacheEvict(value = CacheConstants.BIZ_USER_DETAILS, key = "#bizUser.username")
    public Boolean removeUserById(BizUser bizUser) {
        bizUserRoleMapper.deleteByUserId(bizUser.getUserId());
        this.removeById(bizUser.getUserId());
        return Boolean.TRUE;
    }

    @Override
    @CacheEvict(value = CacheConstants.BIZ_USER_DETAILS, key = "#userDto.username")
    public Boolean updateUserInfo(BizUserDTO userDto) {
        BizUserVO userVO = baseMapper.getUserVoByUsername(userDto.getUsername());

        Assert.isTrue(ENCODER.matches(userDto.getPassword(), userVO.getPassword()), "原密码错误，修改失败");

        BizUser bizUser = new BizUser();
        if (StrUtil.isNotBlank(userDto.getNewpassword1())) {
            bizUser.setPassword(ENCODER.encode(userDto.getNewpassword1()));
        }
        bizUser.setPhone(userDto.getPhone());
        bizUser.setUserId(userVO.getUserId());
        bizUser.setAvatar(userDto.getAvatar());
        return this.updateById(bizUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = CacheConstants.BIZ_USER_DETAILS, key = "#userDto.username")
    public Boolean updateUser(BizUserDTO userDto) {
        BizUser bizUser = new BizUser();
        BeanUtils.copyProperties(userDto, bizUser);
        bizUser.setUpdateTime(LocalDateTime.now());

        if (StrUtil.isNotBlank(userDto.getPassword())) {
            bizUser.setPassword(ENCODER.encode(userDto.getPassword()));
        }
        this.updateById(bizUser);

        bizUserRoleMapper
                .delete(Wrappers.<BizUserRole>update().lambda().eq(BizUserRole::getUserId, userDto.getUserId()));
        userDto.getRole().forEach(roleId -> {
            BizUserRole userRole = new BizUserRole();
            userRole.setUserId(bizUser.getUserId());
            userRole.setRoleId(roleId);
            userRole.insert();
        });
        return Boolean.TRUE;
    }

    /**
     * 查询上级部门的用户信息
     * @param username 用户名
     * @return R
     */
    @Override
    public List<BizUser> listAncestorUsersByUsername(String username) {
        BizUser sysUser = this.getOne(Wrappers.<BizUser>query().lambda().eq(BizUser::getUsername, username));

        SysDept sysDept = sysDeptMapper.selectById(sysUser.getDeptId());
        if (sysDept == null) {
            return null;
        }

        Long parentId = sysDept.getParentId();
        return this.list(Wrappers.<BizUser>query().lambda().eq(BizUser::getDeptId, parentId));
    }

    /**
     * 查询全部的用户
     * @param userDTO 查询条件
     * @return list
     */
    @Override
    public List<BizUserExcelVO> listUser(BizUserDTO userDTO) {
        List<BizUserVO> voList = baseMapper.selectVoList(userDTO);
        // 转换成execl 对象输出
        List<BizUserExcelVO> userExcelVOList = voList.stream().map(userVO -> {
            BizUserExcelVO excelVO = new BizUserExcelVO();
            BeanUtils.copyProperties(userVO, excelVO);
            String roleNameList = userVO.getRoleList().stream().map(SysRole::getRoleName)
                    .collect(Collectors.joining(StrUtil.COMMA));
            excelVO.setRoleNameList(roleNameList);
            return excelVO;
        }).collect(Collectors.toList());
        return userExcelVOList;
    }

    /**
     * excel 导入用户, 插入正确的 错误的提示行号
     * @param excelVOList excel 列表数据
     * @param bindingResult 错误数据
     * @return ok fail
     */
    @Override
    public R importUser(List<BizUserExcelVO> excelVOList, BindingResult bindingResult) {
        // 通用校验获取失败的数据
        List<ErrorMessage> errorMessageList = (List<ErrorMessage>) bindingResult.getTarget();

        // 个性化校验逻辑
        List<BizUser> userList = this.list();
        List<SysDept> deptList = sysDeptMapper.selectList(Wrappers.emptyWrapper());
        List<BizRole> roleList = bizRoleMapper.selectList(Wrappers.emptyWrapper());

        // 执行数据插入操作 组装 UserDto
        for (int i = 0; i < excelVOList.size(); i++) {
            BizUserExcelVO excel = excelVOList.get(i);
            Set<String> errorMsg = new HashSet<>();
            // 校验用户名是否存在
            boolean exsitUserName = userList.stream()
                    .anyMatch(sysUser -> excel.getUsername().equals(sysUser.getUsername()));

            if (exsitUserName) {
                errorMsg.add(String.format("%s 用户名已存在", excel.getUsername()));
            }

            // 判断输入的部门名称列表是否合法
            Optional<SysDept> deptOptional = deptList.stream()
                    .filter(dept -> excel.getDeptName().equals(dept.getName())).findFirst();
            if (!deptOptional.isPresent()) {
                errorMsg.add(String.format("%s 部门名称不存在", excel.getDeptName()));
            }

            // 判断输入的角色名称列表是否合法
            List<String> roleNameList = StrUtil.split(excel.getRoleNameList(), StrUtil.COMMA);
            List<BizRole> roleCollList = roleList.stream()
                    .filter(role -> roleNameList.stream().anyMatch(name -> role.getRoleName().equals(name)))
                    .collect(Collectors.toList());

            if (roleCollList.size() != roleNameList.size()) {
                errorMsg.add(String.format("%s 角色名称不存在", excel.getRoleNameList()));
            }

            // 数据合法情况
            if (CollUtil.isEmpty(errorMsg)) {
                insertExcelUser(excel, deptOptional, roleCollList);
            }
            else {
                // 数据不合法情况
                errorMessageList.add(new ErrorMessage((long) (i + 2), errorMsg));
            }

        }

        if (CollUtil.isNotEmpty(errorMessageList)) {
            return R.failed(errorMessageList);
        }
        return R.ok();
    }

    @Override
    public List<Long> listUserIdByDeptIds(Set<Long> deptIds) {
        return this.listObjs(
                Wrappers.lambdaQuery(BizUser.class).select(BizUser::getUserId).in(BizUser::getDeptId, deptIds),
                Long.class::cast);
    }

    /**
     * 插入excel User
     */
    private void insertExcelUser(BizUserExcelVO excel, Optional<SysDept> deptOptional, List<BizRole> roleCollList) {
        BizUserDTO userDTO = new BizUserDTO();
        userDTO.setUsername(excel.getUsername());
        userDTO.setPhone(excel.getPhone());
        // 批量导入初始密码为手机号
        userDTO.setPassword(userDTO.getPhone());
        // 根据部门名称查询部门ID
        userDTO.setDeptId(deptOptional.get().getDeptId());
        // 根据角色名称查询角色ID
        List<Long> roleIdList = roleCollList.stream().map(BizRole::getRoleId).collect(Collectors.toList());
        userDTO.setRole(roleIdList);
        // 插入用户
        this.saveUser(userDTO);
    }

}
