/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.spg.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.inspur.spg.admin.api.entity.BizUserRole;
import com.inspur.spg.admin.api.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 业务用户角色表 Mapper 接口
 * </p>
 *
 * @author CJ
 * @since 2022/02/28
 */
@Mapper
public interface BizUserRoleMapper extends BaseMapper<BizUserRole> {

	/**
	 * @Author CJ
	 * @Description 根据用户Id删除该用户的角色关系
	 * @Date 2022/2/28 20:54
	 * @param userId
	 **/
	Boolean deleteByUserId(@Param("userId") Long userId);

}
