/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.spg.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.inspur.spg.admin.api.entity.BizRole;
import com.inspur.spg.admin.api.entity.SysRole;
import com.inspur.spg.admin.api.vo.RoleVo;
import com.inspur.spg.admin.service.BizRoleMenuService;
import com.inspur.spg.admin.service.BizRoleService;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.common.log.annotation.SysLog;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author lengleng
 * @date 2019/2/1
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/biz/role")
@Api(value = "bizRole", tags = "业务角色管理模块")
public class BizRoleController {

	private final BizRoleService bizRoleService;

	private final BizRoleMenuService bizRoleMenuService;

	/**
	 * 通过ID查询角色信息
	 * @param id ID
	 * @return 角色信息
	 */
	@GetMapping("/{id:\\d+}")
	public R<BizRole> getById(@PathVariable Long id) {
		return R.ok(bizRoleService.getById(id));
	}

	/**
	 * 添加角色
	 * @param bizRole 角色信息
	 * @return success、false
	 */
	@SysLog("添加角色")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('biz_role_add')")
	public R<Boolean> save(@Valid @RequestBody BizRole bizRole) {
		return R.ok(bizRoleService.save(bizRole));
	}

	/**
	 * 修改角色
	 * @param bizRole 角色信息
	 * @return success/false
	 */
	@SysLog("修改角色")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('biz_role_edit')")
	public R<Boolean> update(@Valid @RequestBody BizRole bizRole) {
		return R.ok(bizRoleService.updateById(bizRole));
	}

	/**
	 * 删除角色
	 * @param id
	 * @return
	 */
	@SysLog("删除角色")
	@DeleteMapping("/{id:\\d+}")
	@PreAuthorize("@pms.hasPermission('biz_role_del')")
	public R<Boolean> removeById(@PathVariable Long id) {
		return R.ok(bizRoleService.removeRoleById(id));
	}

	/**
	 * 获取角色列表
	 * @return 角色列表
	 */
	@GetMapping("/list")
	public R<List<BizRole>> listRoles() {
		return R.ok(bizRoleService.list(Wrappers.emptyWrapper()));
	}

	/**
	 * 分页查询角色信息
	 * @param page 分页对象
	 * @return 分页对象
	 */
	@GetMapping("/page")
	public R<IPage<SysRole>> getRolePage(Page page) {
		return R.ok(bizRoleService.page(page, Wrappers.emptyWrapper()));
	}

	/**
	 * 更新角色菜单
	 * @param roleVo 角色对象
	 * @return success、false
	 */
	@SysLog("更新角色菜单")
	@PutMapping("/menu")
	@PreAuthorize("@pms.hasPermission('biz_role_perm')")
	public R<Boolean> saveRoleMenus(@RequestBody RoleVo roleVo) {
		BizRole bizRole = bizRoleService.getById(roleVo.getRoleId());
		return R.ok(bizRoleMenuService.saveRoleMenus(bizRole.getRoleCode(), roleVo.getRoleId(), roleVo.getMenuIds()));
	}

}
