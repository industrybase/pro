package com.xxl.job.executor.core.jobhandler;

import com.alibaba.fastjson.JSONObject;
import com.inspur.spg.common.core.exception.BusinessException;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.executor.core.exception.XxlJobExecutorException;
import org.apache.commons.lang.StringUtils;


import java.util.Map;

public class BaseJobHandler {

    protected Map<String,Object> checkJobParamNull(String jobParam) throws BusinessException{
        if (StringUtils.isBlank(jobParam)) {
            throw new XxlJobExecutorException("没有传入参数");
        }
        return JSONObject.parseObject(jobParam);
    }

}
