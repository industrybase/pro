package com.xxl.job.executor.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description 高德商用配置
 * @Date 2022/2/9 11:00
 **/
@Data
@Component
@ConfigurationProperties("external.gmap.commercial")
public class GmapCommercialConfig {

    private String clientSecret;

    private String clientKey;

    private String url;

}
