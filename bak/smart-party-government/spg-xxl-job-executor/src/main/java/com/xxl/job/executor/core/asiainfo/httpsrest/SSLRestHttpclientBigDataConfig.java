package com.xxl.job.executor.core.asiainfo.httpsrest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description 能开大数据参数
 * @Date 2021/4/13 19:16
 **/
@Data
@Component
@RefreshScope
@ConfigurationProperties("external.cmcc.nk.bigdata")
public class SSLRestHttpclientBigDataConfig{

	//https端口
	private int httpsPort;
	//https地址
	private String httpsUrl;
	//加密私钥
	private String signPrivateKey;
	//appId
	private String appId;
	//账号
	private String account;
	//密码
	private String password;

}
