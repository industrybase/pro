package com.xxl.job.executor.core.jobhandler;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.epidemic.prevention.api.feign.RemoteCovidService;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.executor.core.config.CovidConfig;
import com.xxl.job.executor.core.exception.XxlJobExecutorException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/*
 * @Author CJ
 * @Description 疫情任务定时同步
 * @Date 2022/1/3 15:58
 **/
@Component
public class CovidJobHandler {

    @Resource
    private CovidConfig covidConfig;
    @Resource
    private RemoteCovidService remoteCovidService;


    @XxlJob("covidJobHandler")
    public void execute() throws Exception{
        String result = HttpUtil.get(covidConfig.getUrl());
        Map<String,Object> resultMap = JSONObject.parseObject(result);
        String data = (String) resultMap.get("data");
        Map<String,Object> dataMap = JSONObject.parseObject(data);
        R returnR = remoteCovidService.saveBatch(dataMap, SecurityConstants.FROM_IN);
        if(returnR.getCode() ==  CommonConstants.FAIL){
            throw new XxlJobExecutorException("保存失败");
        }
    }

}
