package com.xxl.job.executor.core.asiainfo.httpsrest;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSONObject;
import com.inspur.spg.common.core.util.DateUtil;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.executor.core.asiainfo.bean.AsiainfoHeader;
import com.xxl.job.executor.core.asiainfo.sign.RSASignature;
import com.xxl.job.executor.core.asiainfo.util.AsiainfoHashMap;
import lombok.RequiredArgsConstructor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

public class SSLRestHttpclient {

    private static final Logger log = LoggerFactory.getLogger(SSLRestHttpclient.class);

    public static HttpClient getsslhttpClient() throws Exception {
        HttpClient httpClient = new DefaultHttpClient();
        return httpClient;
    }

	public static String post( String appId,String body, String privateKey, String url) throws Exception {
		//随机字符流水
		String nonce = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		//报文流水号
		String busiSerial = UUID.randomUUID().toString().replace("-", "");
		//传入请求头参数
		AsiainfoHeader asiainfoHeader = new AsiainfoHeader();
		asiainfoHeader.setAppId(appId);
		asiainfoHeader.setBusiSerial(busiSerial);
		asiainfoHeader.setTimestamp(DateUtil.format(new Date(), "yyyyMMddHHmmssSSS"));
		asiainfoHeader.setNonce(nonce);
		HttpClient httpclient = getsslhttpClient();
		try {
			AsiainfoHashMap head = AsiainfoHashMap.toAsiainfoHashMap(asiainfoHeader);
			HttpPost postMethod = new HttpPost(url);
			String content = RSASignature.getSignContent(RSASignature.getSortedMap(head)) + body;
			String signstr = RSASignature.sign(content, privateKey);

			Set keys = head.keySet();
			Iterator it = keys.iterator();
			while (it.hasNext()) {
				String a = (String) it.next();
				postMethod.setHeader(a, head.get(a));
			}
			postMethod.setHeader("sign", signstr);

			//测试可以用这个跳过白名单
			String ip = "14.23.164.13";
			postMethod.setHeader("X-Forwarded-For", ip);
			postMethod.setHeader("HTTP_X_FORWARDED_FOR", ip);
			postMethod.setHeader("HTTP_CLIENT_IP", ip);
			postMethod.setHeader("REMOTE_ADDR", ip);
			postMethod.setHeader("Host", ip);

			//postMethod.setHeader("Content-type","application/json");
			//log.info("报文请求头---------------------->>>>>>>>>{}", JSONObject.toJSONString(postMethod.getAllHeaders()));
			//log.info("报文请求参数-------------------->>>>>>>>>" + body);
			log.info("请求接口：{}，请求头参数：{}，请求报文参数{}",url,JSONObject.toJSONString(postMethod.getAllHeaders()),body);
			StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
			postMethod.setEntity(entity);
			HttpResponse response = httpclient.execute(postMethod);
			StatusLine respHttpStatus = response.getStatusLine();
			int staus = respHttpStatus.getStatusCode();
			if (staus == 200) {
				HttpEntity responseBody = response.getEntity();
				String result = EntityUtils.toString(responseBody, "UTF-8");
				log.info("请求接口：{}，返回报文参数：{}",url,result);
				return result;
			} else {
				return "请求失败" + staus;
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
		return null;
	}

}
