package com.xxl.job.executor.core.jobhandler;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.thread.ExecutorBuilder;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.flow.analysis.api.feign.RemoteApiFieldFormatConfigService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.executor.core.asiainfo.httpsrest.SSLRestHttpclientBigDataConfig;
import com.xxl.job.executor.core.exception.XxlJobExecutorException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

/*
 * @Author CJ
 * @Description 能开交通数据
 * @Date 2022/1/7 11:07
 **/
@Component
@Slf4j
public class CmccNkTrafficJobHandler extends NkBigDataBaseJobHandler{

    @Resource
    private RemoteApiFieldFormatConfigService remoteApiFieldFormatConfigService;
    @Resource
    private SSLRestHttpclientBigDataConfig sslRestHttpclientConfig;

    //初始化一个线程池，用于补数据用
    private final ExecutorService executor = ExecutorBuilder.create().setCorePoolSize(5).setMaxPoolSize(10).setWorkQueue(new LinkedBlockingQueue<>(10)).build();

    @XxlJob("honeycombTrafficJobHandler")
    public void execute() throws Exception{
        //任务参数
        Map<String, Object> jobParamMap = checkJobParamNull(XxlJobHelper.getJobParam());
        //接口标识
        String apiName = (String) jobParamMap.get("apiName");
        if (StringUtils.isBlank(apiName)) {
            throw new XxlJobExecutorException("apiName必传");
        }
        //地址
        String url = (String) jobParamMap.get("url");
        if (StringUtils.isBlank(url)) {
            throw new XxlJobExecutorException("url必传");
        }
        //
        Integer timeDifferent = (Integer) jobParamMap.get("timeDifferent");
        if (timeDifferent == null) {
            throw new XxlJobExecutorException("timeDifferent必传");
        }
        //
        String timeFormat = (String) jobParamMap.get("timeFormat");
        if (timeFormat == null) {
            throw new XxlJobExecutorException("timeFormat必传");
        }
        String timeType = (String) jobParamMap.get("timeType");
        //开始时间
        String startTime = (String) jobParamMap.get("startTime");
        //结束时间
        String endTime = (String) jobParamMap.get("endTime");
        //请求日期字段名
        String apiDateFieldName = (String) jobParamMap.get("apiDateFieldName");
        if(StringUtils.isBlank(apiDateFieldName)){
            apiDateFieldName = "statDate";
        }
        //TODO 补数异步执行，避免执行时间过长，一直占用任务队列线程
        if(StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)){
            List<String> times = getTimeDifferent(timeType,startTime,endTime,timeFormat,timeDifferent);
            String finalApiDateFieldName = apiDateFieldName;
            //异步调用
            executor.execute(() -> {
                for(String requestTime : times){
                    try {
                        List<Map<String,Object>> dataList = doPost(finalApiDateFieldName,url,requestTime,XxlJobHelper.getJobId(),true);
                        doSave(apiName,requestTime,dataList);
                    } catch (Exception e) {
                        log.error("任务ID：{}，调用异常：{}" ,XxlJobHelper.getJobId(), e.getLocalizedMessage());
                    }
                }
            });
        }else{
            //请求日期
            String requestTime = (String) jobParamMap.get("requestTime");
            if(StringUtils.isBlank(requestTime)) {
                Date nowTime = new Date();
                if("month".equals(timeType)){
                    requestTime = DateUtil.format(DateUtil.addMonths(nowTime,timeDifferent),timeFormat);
                }else if("day".equals(timeType)){
                    requestTime = DateUtil.format(DateUtil.addDays(nowTime,timeDifferent),timeFormat);
                }else if("hour".equals(timeType)){
                    requestTime = DateUtil.format(DateUtil.addHours(nowTime,timeDifferent),timeFormat);
                }else if("minute".equals(timeType)){
                    requestTime = DateUtil.format(DateUtil.addSecond(DateUtil.addMinutes(nowTime,timeDifferent),-nowTime.getSeconds()),timeFormat);
                }
            }
            List<Map<String,Object>> dataList = doPost(apiDateFieldName,url,requestTime,XxlJobHelper.getJobId(),false);
            doSave(apiName,requestTime,dataList);
        }
    }



    /**
     * @Author CJ
     * @Description 获取时间差日期
     * @Date 2022/3/4 16:44
     * @param timeType
     * @param startTime
     * @param endTime
     * @param format
     **/
    private List<String> getTimeDifferent(String timeType,String startTime,String endTime,String format,Integer timeDifferent){
        Date requestStartTime = DateUtil.parseDateTime(startTime);
        Date requestEndTime = DateUtil.parseDateTime(endTime);
        if((requestEndTime.getTime() - requestEndTime.getTime()) < 0){
            throw new XxlJobExecutorException("结束时间不能小于开始时间");
        }
        List<String> times = new ArrayList<>();
        long start = DateUtil.getMonth(requestStartTime);
        long end = DateUtil.getMonth(requestEndTime);
        int different = (int)(end - start);
        if("month".equals(timeType)){
            for(int i = 0; i <= different; i++){
                times.add(DateUtil.format(DateUtil.addMonths(requestStartTime,i),format));
            }
        }else if("day".equals(timeType)){
            for(int i = 0; i <= different; i++){
                times.add(DateUtil.format(DateUtil.addDays(requestStartTime,i),format));
            }
        }else if("hour".equals(timeType)){
            start = requestStartTime.getTime();
            end = requestEndTime.getTime();
            different = (int) ((end - start) /(1000 * 60 * 60));
            for(int i = 0; i <= different; i++){
                times.add(DateUtil.format(DateUtil.addHours(requestStartTime,i),format));
            }
        }else if("minute".equals(timeType)){
            start = requestStartTime.getTime();
            end = requestEndTime.getTime();
            int minute = (int) ((end - start) / (1000 * 60));
            different = minute/timeDifferent + 1;
            for(int i = 0; i < different; i++){
                times.add(DateUtil.format(DateUtil.addMinutes(requestStartTime,i*timeDifferent),format));
            }
        }
        return times;
    }

    /**
     * 请求接口
     * @param apiDateFieldName 请求api标识名称
     * @param url 请求地址
     * @param requestTime 请求时间
     * @param jobId 任务ID
     * @param isAsyn 是否为异步
     * @return
     * @throws Exception
     */
    private List<Map<String, Object>> doPost(String apiDateFieldName,String url,String requestTime,Long jobId,boolean isAsyn) throws Exception {
        url = sslRestHttpclientConfig.getHttpsUrl() + url;
        //请求参数
        Map<String, Object> requestParamsMap = new HashMap<>();
        requestParamsMap.put(apiDateFieldName, requestTime);
        Map<String, Object> responseMap;
        if(isAsyn){
            responseMap = doPostExecuteAsyn(jobId,url, requestParamsMap, sslRestHttpclientConfig.getAppId(), sslRestHttpclientConfig.getSignPrivateKey());
        }else {
            responseMap = doPostExecute(url, requestParamsMap, sslRestHttpclientConfig.getAppId(), sslRestHttpclientConfig.getSignPrivateKey());
        }
        Map<String, Object> resultMap = (Map<String, Object>) responseMap.get("result");
        List<Map<String, Object>> data = (List<Map<String, Object>>) resultMap.get("data");
        return data;
    }

    /**
     * 保存数据
     * @param apiName
     * @param requestTime
     * @param dataList
     */
    private void doSave(String apiName,String requestTime,List<Map<String, Object>> dataList){
        if(CollectionUtil.isEmpty(dataList)){
            throw new XxlJobExecutorException("无数据");
        }
        R returnR = remoteApiFieldFormatConfigService.saveByApiResult(Dict.create().set("data",dataList).set("apiName",apiName).set("delValue",requestTime), SecurityConstants.FROM_IN);
        if(returnR.getCode() ==  CommonConstants.FAIL){
            throw new XxlJobExecutorException("保存失败");
        }
    }

}
