package com.xxl.job.executor.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description 疫情配置
 * @Date 2022/1/3 16:07
 **/

@Data
@Component
@ConfigurationProperties("external.covid")
public class CovidConfig {

    private String url;

}
