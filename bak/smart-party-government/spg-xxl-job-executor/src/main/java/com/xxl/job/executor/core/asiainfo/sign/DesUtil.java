package com.xxl.job.executor.core.asiainfo.sign;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.SecureRandom;

@Slf4j
public class DesUtil {
	
    private final static String DES = "DES";

    /**
     * 加密
     * @param src 数据源
     * @param key 密钥，长度必须是8的倍数
     * @return 返回加密后的数据
     * @throws Exception
     */
    public static byte[] encrypt(byte[] src, byte[] key) throws Exception {

        // DES算法要求有一个可信任的随机数源
        SecureRandom sr = new SecureRandom();
        // 从原始密匙数据创建DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密匙工厂，然后用它把DESKeySpec转换成
        // 一个SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        // Cipher对象实际完成加密操作
        Cipher cipher = Cipher.getInstance(DES);
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
        // 正式执行加密操作
        return cipher.doFinal(src);

    }

    /**
     * 解密
     * @param src 数据源
     * @param key 密钥，长度必须是8的倍数
     * @return 返回解密后的原始数据
     * @throws Exception
     */
    public static byte[] decrypt(byte[] src, byte[] key) throws Exception {

        // DES算法要求有一个可信任的随机数源
        SecureRandom sr = new SecureRandom();
        // 从原始密匙数据创建一个DESKeySpec对象
        DESKeySpec dks = new DESKeySpec(key);
        // 创建一个密匙工厂，然后用它把DESKeySpec对象转换成
        // 一个SecretKey对象
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
        SecretKey securekey = keyFactory.generateSecret(dks);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance(DES);
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
        // 正式执行解密操作
        return cipher.doFinal(src);

    }

    /**
     * 密码解密
     * @param data
     * @return
     * @throws Exception
     */
    public final static String decrypt(String data,String key) {
        try {
            return new String(decrypt(hex2byte(data.getBytes()), getKey(key).getBytes()));
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error("数据:"+data + " 解密失败");
        }
        return null;
    }

    /**
     * 密码加密
     * @param password
     * @return
     * @throws Exception
     */
    public final static String encrypt(String password,String key) {
        try {
            return byte2hex(encrypt(password.getBytes(), getKey(key).getBytes()));
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error("数据:"+password + " 加密失败");
        }
        return null;
    }

    /**
     * 二行制转字符串
     * @param b
     * @return
     */
    public static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = (Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1)
                hs = hs + "0" + stmp;
            else
                hs = hs + stmp;
        }
        return hs.toUpperCase();
    }

    public static byte[] hex2byte(byte[] b) {
        if ((b.length % 2) != 0)
            throw new IllegalArgumentException("长度不是偶数");
        byte[] b2 = new byte[b.length / 2];
        for (int n = 0; n < b.length; n += 2) {
            String item = new String(b, n, 2);
            b2[n / 2] = (byte) Integer.parseInt(item, 16);
        }
        return b2;
    }

    private static String getKey(String key){
        int paratopeLen = 8-key.length()%8;
        StringBuilder sb = new StringBuilder(key);
        while(paratopeLen > 0 && paratopeLen!=8){
            sb.append("0");
            paratopeLen--;
        }
        return sb.toString();
    }
    public static String getToken(String account,String timestamp,String password){
        String token = account+"|" + timestamp;
        String key = "";
        key = getKey(password);
        /*log.info("key={}",key);
        log.info("tokenstr={}",tokenstr);*/
        String str1 = encrypt(token,key);
        return str1;
    }

    public static String getPassword(String password,String timestr){
        String passwordstr = password+"|" +timestr;
        String str1 = encrypt(passwordstr,password);
        return str1;
    }

    // 测试用例，不需要传递任何参数，直接执行即可。
    public static void main(String[] args) {
        String tokenstr = "zhys|" + System.currentTimeMillis();
        String key = "zhys";
        key = getKey(key);
        String str1 = encrypt(tokenstr,key);

        System.out.println("原始值: " + tokenstr);
        System.out.println("加密后: " + str1);
        System.out.println("解密后: " + decrypt(str1,key));
//        zhys  zhys#569
//        System.out.println(DateUtil.getTimestampByFormat("yyyyMMddHHmmssSSS",120L));
    }

}

