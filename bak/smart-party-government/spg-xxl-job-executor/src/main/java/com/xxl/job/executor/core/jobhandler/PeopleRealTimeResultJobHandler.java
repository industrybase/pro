package com.xxl.job.executor.core.jobhandler;

import com.alibaba.fastjson.JSONObject;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeResultService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/*
 * @Author CJ
 * @Description 统计前一天实时数据总量
 * @Date 2022/2/13 12:57
 **/
@Component
public class PeopleRealTimeResultJobHandler extends BaseJobHandler{

    @Resource
    private RemotePeopleRealTimeResultService remotePeopleRealTimeResultService;

    @XxlJob("peopleRealTimeResultJobHandler")
    public void execute() throws Exception{
        //任务参数
        Map<String, Object> jobParamMap = checkJobParamNull(XxlJobHelper.getJobParam());
        String statDate = (String) jobParamMap.get("statDate");
        if(StringUtils.isBlank(statDate)){
            statDate = DateUtil.formatDate(DateUtil.addDays(new Date(),-1));
            jobParamMap.put("statDate",statDate);
        }
        R returnR = remotePeopleRealTimeResultService.executeSave(jobParamMap, SecurityConstants.FROM_IN);
        if(returnR.getCode() != CommonConstants.SUCCESS){
            XxlJobHelper.log("保存失败：{}",returnR.getMsg());
        }
    }

}
