package com.xxl.job.executor.core.asiainfo.httpsrest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description
 * @Date 2021/4/2 10:47
 **/
@Data
public class SSLRestHttpclientBaseConfig {

	//https端口
	private int httpsPort;
	//https地址
	private String httpsUrl;
	//加密私钥
	private String signPrivateKey;
	//appId
	private String appId;

}
