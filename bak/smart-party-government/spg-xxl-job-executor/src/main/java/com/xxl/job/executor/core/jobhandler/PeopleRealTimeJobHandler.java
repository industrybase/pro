package com.xxl.job.executor.core.jobhandler;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSONObject;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.util.DateUtil;
import com.inspur.spg.common.core.util.ListUtils;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeFlowInService;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeFlowOutService;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleRealTimeNumService;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemotePeopleStayTimeService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.executor.core.asiainfo.httpsrest.SSLRestHttpclientBigDataConfig;
import com.xxl.job.executor.core.asiainfo.sign.DesUtil;
import com.xxl.job.executor.core.exception.XxlJobExecutorException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/29 14:51
 **/
@Component
public class PeopleRealTimeJobHandler extends NkBigDataBaseJobHandler {

    @Resource
    private SSLRestHttpclientBigDataConfig sslRestHttpclientBigDataConfig;
    @Resource
    private RemotePeopleRealTimeNumService remotePeopleRealTimeNumService;
    @Resource
    private RemotePeopleRealTimeFlowInService remotePeopleRealTimeFlowInService;
    @Resource
    private RemotePeopleRealTimeFlowOutService remotePeopleRealTimeFlowOutService;
    @Resource
    private RemotePeopleStayTimeService remotePeopleStayTimeService;

    @XxlJob("peopleRealTimeJobHandler")
    public void execute() throws Exception {
        //任务参数
        Map<String, Object> jobParamMap = checkJobParamNull(XxlJobHelper.getJobParam());
        //活动类型
        Integer actionType = (Integer) jobParamMap.get("actionType");
        if (actionType == null) {
            throw new XxlJobExecutorException("actionType 不能为空");
        }
        //任务
        String jobCode = (String) jobParamMap.get("jobCode");
        if (StringUtils.isBlank(jobCode)) {
            throw new XxlJobExecutorException("jobCode 不能为空");
        }
        //开始时间时间差
        Integer startTimeTimeDifference = (Integer) jobParamMap.get("startTimeTimeDifference");
        //结束时间时间差
        Integer endTimeTimeDifference = (Integer) jobParamMap.get("endTimeTimeDifference");
        String startTime = (String) jobParamMap.get("startTime");
        String endTime = (String) jobParamMap.get("endTime");
        //时间
        if (StringUtils.isBlank(startTime)) {
            startTime = DateUtil.format(DateUtil.addMinutes(new Date(), startTimeTimeDifference), "yyyyMMddHHmmss");
        }else{
            startTime = DateUtil.format(DateUtil.parseDateTime(startTime),"yyyyMMddHHmmss");
        }
        if (StringUtils.isBlank(endTime)) {
            endTime = DateUtil.format(DateUtil.addMinutes(new Date(), endTimeTimeDifference), "yyyyMMddHHmmss");
        }else{
            endTime = DateUtil.format(DateUtil.parseDateTime(endTime),"yyyyMMddHHmmss");
        }
        Map<String,List<Map<String,Object>>> saveMap = new HashMap<>();
        AtomicInteger currentPage = new AtomicInteger(0);
        int pageSize = 1000;
        int totalPage = 0;
        saveMap = getData(startTime,endTime,jobCode,pageSize,totalPage,currentPage,saveMap);
        if(MapUtil.isEmpty(saveMap)){
            throw new XxlJobExecutorException("接口无数据");
        }
        saveListByActionType(actionType, saveMap);
      /*  Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("token", DesUtil.getToken(sslRestHttpclientBigDataConfig.getAccount(), DateUtil.format(new Date(), "yyyyMMddHHmmss"), sslRestHttpclientBigDataConfig.getPassword()));
        requestMap.put("systemCode", sslRestHttpclientBigDataConfig.getAccount());
        requestMap.put("applicationId", sslRestHttpclientBigDataConfig.getAppId());
        requestMap.put("jobCode", jobCode);
        requestMap.put("startTime", startTime);
        requestMap.put("endTime", endTime);
        requestMap.put("startNum", 1);
        requestMap.put("endNum", "1000");
        requestMap.put("pageLen", "1000000");
        Map<String, Object> responseMap = doPostExecute(sslRestHttpclientBigDataConfig.getHttpsUrl() + "/eaop/rest/BIGDATA/services/realrealcount/v1.1.1", requestMap, sslRestHttpclientBigDataConfig.getAppId(), sslRestHttpclientBigDataConfig.getSignPrivateKey());
        Map<String, Object> resultMap = (Map<String, Object>) responseMap.get("result");
        Map<String, Object> responseDataMap = (Map<String, Object>) resultMap.get("respdata");
        if (!responseDataMap.get("code").equals(200)) {
            throw new XxlJobExecutorException("接口返回失败：" + new JSONObject(responseDataMap).toJSONString());
        }
        Map<String, Object> contentMap = (Map<String, Object>) responseDataMap.get("content");
        List<Map<String, Object>> list = (List<Map<String, Object>>) contentMap.get("list");
        if(CollectionUtil.isEmpty(list)){
            throw new XxlJobExecutorException("接口无数据");
        }
        saveListByActionType(actionType, list);*/
    }

    /**
     * @param startTime
     * @param endTime
     * @param jobCode
     * @param pageSize
     * @param totalPage
     * @param currentPage
     * @param saveMap
     * @return void
     * @Author CJ
     * @Description 获取接口数据
     * @Date 2022/1/29 16:13
     **/
    private Map<String, List<Map<String, Object>>> getData(String startTime, String endTime, String jobCode, int pageSize, int totalPage, AtomicInteger currentPage, Map<String, List<Map<String, Object>>> saveMap) throws Exception {
        Map<String, Object> requestMap = new HashMap<>();
        requestMap.put("token", DesUtil.getToken(sslRestHttpclientBigDataConfig.getAccount(), DateUtil.format(new Date(), "yyyyMMddHHmmss"), sslRestHttpclientBigDataConfig.getPassword()));
        requestMap.put("systemCode", sslRestHttpclientBigDataConfig.getAccount());
        requestMap.put("applicationId", sslRestHttpclientBigDataConfig.getAppId());
        requestMap.put("jobCode", jobCode);
        requestMap.put("startTime", startTime);
        requestMap.put("endTime", endTime);
        requestMap.put("startNum", pageSize * currentPage.get() + 1);
        requestMap.put("endNum", "1000");
        requestMap.put("pageLen", pageSize);
        Map<String, Object> responseMap = doPostExecute(sslRestHttpclientBigDataConfig.getHttpsUrl() + "/eaop/rest/BIGDATA/services/realrealcount/v1.1.1", requestMap, sslRestHttpclientBigDataConfig.getAppId(), sslRestHttpclientBigDataConfig.getSignPrivateKey());
        Map<String, Object> resultMap = (Map<String, Object>) responseMap.get("result");
        Map<String, Object> responseDataMap = (Map<String, Object>) resultMap.get("respdata");
        if (!responseDataMap.get("code").equals(200)) {
            XxlJobHelper.log("接口返回失败：" + new JSONObject(responseDataMap).toJSONString());
            return null;
        }
        Map<String, Object> contentMap = (Map<String, Object>) responseDataMap.get("content");
        Integer total = (Integer) contentMap.get("total");
        if (totalPage == 0) {
            totalPage = total / pageSize + 1;
        }
        List<Map<String, Object>> list = (List<Map<String, Object>>) contentMap.get("list");
        saveMap.put("page_" + currentPage, list);
        //递归页数
        while (currentPage.incrementAndGet() < totalPage){
            saveMap.putAll(getData(startTime, endTime, jobCode, pageSize, totalPage, currentPage, saveMap));
        }
        return saveMap;
    }

    /**
     * @param actionType
     * @param saveMap
     * @return void
     * @Author CJ
     * @Description 保存数据
     * @Date 2022/1/29 16:13
     **/
    private void saveListByActionType(Integer actionType, Map<String, List<Map<String, Object>>> saveMap) {
        for (Map.Entry<String, List<Map<String, Object>>> entry : saveMap.entrySet()) {
            List<Map<String, Object>> list = entry.getValue();
            if (CollectionUtil.isNotEmpty(list)) {
                R result = new R();
                if (actionType == 1) {
                    result = remotePeopleRealTimeNumService.saveBatch(list, SecurityConstants.FROM_IN);
                } else if (actionType == 3) {
                    result = remotePeopleRealTimeFlowInService.saveBatch(list, SecurityConstants.FROM_IN);
                } else if (actionType == 4) {
                    result = remotePeopleRealTimeFlowOutService.saveBatch(list, SecurityConstants.FROM_IN);
                } else if (actionType == 5) {
                    result = remotePeopleStayTimeService.saveBatch(list, SecurityConstants.FROM_IN);
                }
                if (result.getCode() != CommonConstants.SUCCESS) {
                    XxlJobHelper.log("数据保存失败，{}，{}",result.getMsg(),JSONObject.toJSONString(list));
                }
            }
        }
    }

    /**
     * @param actionType
     * @param list
     * @return void
     * @Author CJ
     * @Description 保存数据
     * @Date 2022/1/29 16:13
     **/
    private void saveListByActionType(Integer actionType, List<Map<String, Object>> list) {
        List<List> groupList = ListUtils.splitList(list,1000);
        for(List<Map<String,Object>> saveList : groupList){
            R result = new R();
            if (actionType == 1) {
                result = remotePeopleRealTimeNumService.saveBatch(list, SecurityConstants.FROM_IN);
            } else if (actionType == 3) {
                result = remotePeopleRealTimeFlowInService.saveBatch(list, SecurityConstants.FROM_IN);
            } else if (actionType == 4) {
                result = remotePeopleRealTimeFlowOutService.saveBatch(list, SecurityConstants.FROM_IN);
            } else if (actionType == 5) {
                result = remotePeopleStayTimeService.saveBatch(list, SecurityConstants.FROM_IN);
            }
            if (result.getCode() != CommonConstants.SUCCESS) {
                XxlJobHelper.log("数据保存失败，{}，{}",result.getMsg(),JSONObject.toJSONString(saveList));
            }
        }
    }

}
