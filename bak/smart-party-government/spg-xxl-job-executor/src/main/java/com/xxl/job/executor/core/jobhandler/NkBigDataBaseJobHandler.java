package com.xxl.job.executor.core.jobhandler;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSONObject;
import com.inspur.spg.common.core.exception.BusinessException;
import com.inspur.spg.common.core.util.DateUtil;
import com.xxl.job.core.context.XxlJobHelper;

import com.xxl.job.executor.core.asiainfo.bean.AsiainfoHeader;
import com.xxl.job.executor.core.asiainfo.httpsrest.SSLRestHttpclient;
import com.xxl.job.executor.core.exception.XxlJobExecutorException;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/29 15:22
 **/
@Slf4j
public class NkBigDataBaseJobHandler extends BaseJobHandler{

    /**
     * 执行调用
     * @param url
     * @param requestParamsMap
     * @param appId
     * @param signPrivateKey
     * @return
     * @throws Exception
     */
    protected Map<String, Object> doPostExecute(String url, Map<String, Object> requestParamsMap, String appId, String signPrivateKey) throws Exception {
        XxlJobHelper.log("请求报文参数：" + JSONObject.toJSONString(requestParamsMap));
        String response = SSLRestHttpclient.post(appId, new JSONObject(requestParamsMap).toJSONString(), signPrivateKey, url);
        XxlJobHelper.log("返回报文参数：" + response);
        return checkResponse(response);
    }

    /**
     * 异步执行调用日志打印
     * @param url
     * @param requestParamsMap
     * @param appId
     * @param signPrivateKey
     * @return
     * @throws Exception
     */
    protected Map<String, Object> doPostExecuteAsyn(Long jobId,String url, Map<String, Object> requestParamsMap, String appId, String signPrivateKey) throws Exception {
        log.info("任务ID：{}，请求报文参数：{}" ,jobId, JSONObject.toJSONString(requestParamsMap));
        String response = SSLRestHttpclient.post(appId, new JSONObject(requestParamsMap).toJSONString(), signPrivateKey, url);
        log.info("任务ID：{}，返回报文参数：{}" ,jobId,response);
        return checkResponse(response);
    }

    /**
     * 判断返回报文
     * @param response
     * @return
     */
    private Map<String,Object> checkResponse(String response){
        Map<String, Object> responseMap = JSONObject.parseObject(response);
        if(MapUtil.isEmpty(responseMap)){
            throw new XxlJobExecutorException("无报文返回");
        }
        String code = (String) responseMap.get("respcode");
        if(!code.equals("0")){
            throw new XxlJobExecutorException("请求接口异常报文：" + responseMap.get("msg"));
        }
        return responseMap;
    }


}
