package com.xxl.job.executor;

import com.inspur.spg.common.core.util.SpringContextHolder;
import com.inspur.spg.common.feign.annotation.EnableSpgFeignClients;
import com.inspur.spg.common.job.annotation.EnableXxlJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/*
 * @Author CJ
 * @Description
 * @Date 2022/1/3 14:55
 **/
@EnableXxlJob
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableSpgFeignClients
public class XxlJobExecutorApplication {

    public static void main(String[] args) {
        SpringApplication.run(XxlJobExecutorApplication.class, args);
    }


}
