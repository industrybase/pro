package com.xxl.job.executor.core.jobhandler;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.inspur.spg.common.core.constant.CommonConstants;
import com.inspur.spg.common.core.constant.SecurityConstants;
import com.inspur.spg.common.core.util.R;
import com.inspur.spg.real.time.flow.analysis.api.feign.RemoteTrafficEventsRealTimeService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.executor.core.config.GmapCommercialConfig;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * @Author CJ
 * @Description 交通事件实时(高德接口)
 * @Date 2022/2/7 19:35
 **/
@Component
public class TrafficEventsRealTimeJobHandler extends BaseJobHandler{

    @Resource
    private GmapCommercialConfig commercialConfig;
    @Resource
    private RemoteTrafficEventsRealTimeService remoteTrafficEventsRealTimeService;

    //成功结果
    private final int SUCCESS = 0;

    @XxlJob("trafficEventsRealTimeJobHandler")
    public void execute() throws Exception {
        //任务参数
        Map<String, Object> jobParamMap = checkJobParamNull(XxlJobHelper.getJobParam());
        //高德事件类型
        String eventTypes = (String) jobParamMap.get("eventTypes");
        //String eventTypes = "101,102,103,201,202,203,301,302";
        String[] cityCodes = {"440100", "440200", "440300", "440400", "440500", "440600", "440700", "440800", "440900", "441200", "441300", "441400", "441500", "441600", "441700", "441800", "441900", "442000", "445100", "445200", "445300"};
        List<Map<String, Object>> saveList = new ArrayList<>();
        for (String cityCode : cityCodes) {
            long timestamp = System.currentTimeMillis() * 1000;
            Map<String, Object> requestMap = new HashMap<>();
            requestMap.put("adcode", cityCode);
            requestMap.put("eventType", eventTypes);
            requestMap.put("isExpressway", "all");
            requestMap.put("clientKey", commercialConfig.getClientKey());
            requestMap.put("timestamp", timestamp);
            String digest = buildSign(requestMap, commercialConfig.getClientSecret());
            requestMap.put("digest", digest);
            String url = commercialConfig.getUrl() + "/event/queryByAdcode";
            String response = HttpUtil.get(url, requestMap);
            Map<String, Object> responseMap = JSONObject.parseObject(response);
            if(responseMap.containsKey("errcode") || (responseMap.get("code") != null && (Integer)responseMap.get("code") != SUCCESS)){
                XxlJobHelper.log("请求报文参数：{}", JSONObject.toJSONString(requestMap));
                XxlJobHelper.log("报文返回异常：{}", response);
                continue;
            }
            List<Map<String, Object>> data = (List<Map<String, Object>>) responseMap.get("data");
            R returnR = remoteTrafficEventsRealTimeService.saveBatch(data, SecurityConstants.FROM_IN);
            if (returnR.getCode() != CommonConstants.SUCCESS) {
                XxlJobHelper.log("保存失败：{}", JSONObject.toJSONString(returnR));
            }
        }
    }

    /**
     * @param requestMap
     * @param clientSecret
     * @return java.lang.String
     * @Author CJ
     * @Description 构建签名
     * @Date 2022/2/9 10:48
     **/
    private String buildSign(Map<String, Object> requestMap, String clientSecret) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        requestMap = MapUtil.sort(requestMap);
        String signBody = "";
        for (Map.Entry<String, Object> entry : requestMap.entrySet()) {
            signBody += entry.getValue();
        }
        Mac mac = Mac.getInstance("HmacSHA256");
        byte[] secretByte = clientSecret.getBytes("UTF-8");
        byte[] dataBytes = signBody.getBytes("UTF-8");
        SecretKey secretKey = new SecretKeySpec(secretByte, "HMACSHA256");
        mac.init(secretKey);
        byte[] doFinal = mac.doFinal(dataBytes);
        byte[] hexB = new Hex().encode(doFinal);
        String digest = new String(hexB, "utf-8");
        return digest;
    }


}
