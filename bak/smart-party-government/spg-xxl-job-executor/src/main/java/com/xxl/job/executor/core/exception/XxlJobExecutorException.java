package com.xxl.job.executor.core.exception;

public class XxlJobExecutorException extends RuntimeException {

    public XxlJobExecutorException() {
        super();
    }

    public XxlJobExecutorException(String message, Throwable cause, boolean enableSuppression,
                             boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public XxlJobExecutorException(String message, Throwable cause) {
        super(message, cause);
    }

    public XxlJobExecutorException(String message) {
        super(message);
    }

    public XxlJobExecutorException(Throwable cause) {
        super(cause);
    }

}
