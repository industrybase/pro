package com.xxl.job.executor.core.asiainfo.httpsrest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/*
 * @Author CJ
 * @Description 能开短信参数
 * @Date 2021/4/13 19:16
 **/
@Data
@Component
@RefreshScope
@ConfigurationProperties("external.cmcc.nk.sms")
public class SSLRestHttpclientSmsConfig {

	//https端口
	private int httpsPort;
	//https地址
	private String httpsUrl;
	//加密私钥
	private String signPrivateKey;
	//appId
	private String appId;

	private String keyStoreType;

	private String schemeHttps;

	private String keyStoreClientPath;

	private String keyStorePassword;

	private String requestId;

}
