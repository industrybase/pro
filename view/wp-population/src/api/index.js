import * as API from "../utils/request";

export default {

    //疫情专区——人口总量与疫情趋势（当日新增、本土新增、境外新增）
    getPeopleChart(params) {
        return API.GET('/epidemic-prevention-control/covid/getStats', params);
    },
    //疫情专区——实时交通事件
    getNowTraffic(params) {
        return API.GET('/real-time-personnel-mobility/trafficEventsRealTime/getRealTimeList', params)
    },
    //疫情专区——根据地市获取疫情数据
    getPeopleByCity(params) {
        return API.GET('/epidemic-prevention-control/covid/getData', params)
    },
    //疫情专区——人口洞察图表信息
    getPeopleInfo(params) {
        return API.GET('/flow-analysis/migrant/getPopulationStats', params)
    }


}
