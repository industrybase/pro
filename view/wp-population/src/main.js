import Vue from 'vue'
import App from './App.vue'
import less from 'less'
import 'element-ui/lib/theme-chalk/index.css';
import VueRouter from "vue-router";
import { Button, Select,Table,TableColumn } from 'element-ui';


Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(less);
Vue.use(Button);
Vue.use(Select);
Vue.use(Table);
Vue.use(TableColumn,);
new Vue({
    render: h => h(App),
}).$mount('#app');
