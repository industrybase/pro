import axios from 'axios'
import {loginUrl} from "../assets/datas/MenuDatas";

const service = axios.create({
    baseURL: '',
    timeout: 50000,    //访问超时的时间限制
});

service.interceptors.request.use(config => {
    const token = window.localStorage.getItem("token");
    if (token !== '') {
        config.headers.Authorization = 'Bearer ' + localStorage.getItem('token');
    } else {
        window.location.href = loginUrl
    }
    return config
}, error => {
    return error
});

service.interceptors.response.use(config => {
    // console.log('进入响应拦截器');
    // console.log(config);

    return config
}, error => {
    return error
});

export const GET = (url, params = {}) => {
    return service.get(`${url}`, {
        params: params
    })
};
export const POST = (url, params) => {
    return service.post(`${url}`, params)
};



