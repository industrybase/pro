import * as API from "../utils/request";

export default {
    //热力监控——人口洞察图表信息
    getPeopleInfo(params) {
        return API.GET('/flow-analysis/migrant/getPopulationStats', params)
    },
    //热力监控——获取实时人口信息
    getNowPeopleInfo(params){
        return API.GET('/real-time-personnel-mobility/peopleRealTimeNum/getRealTimeList', params)
    },
    //热力监控——实时客流分析
    getNowPeopleLineChart(params){
        return API.GET('/real-time-personnel-mobility/peopleRealTimeResult/getChartsInfo', params)
    },
    //热力监控——获取旅客量
    getTraveler(params){
        return API.GET('/flow-analysis/station/getPassengerNum', params)
    },
    //获取服务区、港口、机场、客运站等场站的经纬度数据
    getPointArea(params){
        return API.GET('/flow-analysis/positionType/getAll', params)
    }
}
