import * as echarts from 'echarts'

const lineColor = '#9FD7FF';
//实时资讯图表配置
const lineChart_nowData = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#fecc00'
            }
        },
        formatter: '{b}: {c}%'
    },
    legend: {
        icon: 'roundRect',
        data: ['总客流量', '到港人数', '离港人数'],
        textStyle: {
            color: lineColor
        },
        right:30
    },


    xAxis: {
        name: '日期',
        nameTextStyle: {
            fontSize: 14,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        triggerEvent: true,
        nameGap: 5,
        type: 'category',
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //刻度
        axisTick: {show: false},
        axisLabel: {
            margin: 10,
            color: lineColor,
            textStyle: {
                fontSize: 14
            },
            itemStyle: {
                backgroundColor: 'red'
            }
        },
        // splitLine: {
        //     lineStyle: {
        //         type: 'solid',
        //         color: lineColor,
        //         width: 1
        //     },
        //     show: true
        // },
        data: [],
    },
    yAxis: {
        type: 'value',
        name: '(人数/万)',
        triggerEvent: true,
        nameTextStyle: {
            fontSize: 14,
            color: lineColor
        },
        splitNumber: 5,
        axisLabel: {
            formatter: '{value}',
            color: lineColor
        },
        axisTick: {show: false},
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //图表坐标背景设置
        splitArea: {show: false},
        splitLine: {
            lineStyle: {
                color: lineColor
            }
        }
    },
    series: [
        {
            name: '总客流量',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#00F0FF'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '到港人数',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#FECC00'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '离港人数',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#FF6296'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        }
    ]
};
//来源|去向Top10图表配置
const barChart_top10 = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#fecc00'
            }
        },
        formatter: '{b}: {c}%'
    },
    grid: {
        left: 10,
        top: 40,
        bottom: 10,
        right: 35,
        containLabel: true
    },
    xAxis: {
        nameTextStyle: {
            fontSize: 14,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        nameGap: 5,
        type: 'category',
        data: [],
        axisLine: {
            lineStyle: {
                color: lineColor
            }
        },
        axisTick: {
            show: false
        },
        //竖坐标轴线
        // splitLine: {
        //     lineStyle: {
        //         type: 'solid',
        //         color: lineColor,
        //         width: 1
        //     },
        //     show: true
        // },
        axisLabel: {
            color: lineColor,
            interval: 0,
            textStyle: {
                fontSize: 14
            }
        }
    },
    yAxis: {
        type: 'value',
        name: '(人数/%)',
        nameTextStyle: {
            fontSize: 14,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        axisTick: {
            show: false
        },
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        axisLabel: {
            color: lineColor
        },
        splitLine: {
            show: true,
            lineStyle: {
                color: lineColor
            }

        }
    },
    series: [{
        data: [],
        type: 'bar',
        barWidth: 12,
        itemStyle: {
            normal: {
                barBorderRadius: [30, 30, 0, 0],
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 1,
                    color: '#fecc00'
                }, {
                    offset: 0,
                    color: '#fecc00'
                }]),
            }
        },
    }]
};
//实时客流
const lineChart_nowDataPeople = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#fecc00'
            }
        },
        formatter: '{a0}: {c0}%<br/>{a1}: {c1}%<br/>{a2}: {c2}%<br/>'
    },
    legend: {
        icon: 'roundRect',
        data: ['实时流入', '实时流出','实时人数'],
        textStyle: {
            color: lineColor
        },
        right:30
    },
    grid: {
        left: '0%',
        right: '6%',
        top: '50px',
        bottom: '10%',
        containLabel: true
    },

    xAxis: {
        name: '日期',
        nameTextStyle: {
            fontSize: 14,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        triggerEvent: true,
        nameGap: 5,
        type: 'category',
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //刻度
        axisTick: {show: false},
        axisLabel: {
            margin: 10,
            color: lineColor,
            textStyle: {
                fontSize: 14
            },
            itemStyle: {
                backgroundColor: 'red'
            }
        },
        data: [],
    },
    yAxis: {
        type: 'value',
        name: '(人数/万)',
        triggerEvent: true,
        nameTextStyle: {
            fontSize: 14,
            color: lineColor
        },
        splitNumber: 5,
        axisLabel: {
            formatter: '{value}',
            color: lineColor
        },
        axisTick: {show: false},
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //图表坐标背景设置
        splitArea: {show: false},
        splitLine: {
            lineStyle: {
                color: lineColor
            }
        }
    },
    series: [

        {
            name: '实时流入',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#00F0FF'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '实时流出',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#FECC00'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '实时人数',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#fe8ef4'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        }
    ]
};
export {lineChart_nowData, barChart_top10,lineChart_nowDataPeople}
