// 配置编译环境和线上环境之间的切换

const env = process.env
const baseUrl = 'http://47.113.204.215:8888';
const codeUrl = `http://47.113.204.215:8888/code`;

export {
    baseUrl,
    codeUrl,
    env
}
