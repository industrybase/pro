import service from '../utils/request'
import qs from 'qs'
import {encryption} from "../utils/util";


const scope = 'server';
export default {

    loginByUsername(userInfo) {
        const user = encryption({
            data: userInfo,
            key: 'inspur@#spg5363!',
            param: ['password']
        });
        const grant_type = 'password';
        let dataObj = qs.stringify({'username': user.username, 'password': user.password});
        // let dataObj = {'username': user.username, 'password': user.password};
        let randomStr = user.randomStr;
        let code = user.code;
        return service({
            url: '/auth/oauth/token',
            headers: {
                isToken: false,
                Authorization: 'Basic aW5zcHVyOmluc3B1cg=='
            },
            method: 'post',
            params: {randomStr, code, grant_type, scope},
            data: dataObj
        })
    },
    loginByMobile(mobile, code) {
        const grant_type = 'app'
        return service({
            url: '/auth/oauth/token',
            headers: {
                isToken: false,
                'Authorization': 'Basic YXBwOmFwcA=='
            },
            method: 'post',
            params: {mobile: mobile, code: code, grant_type}
        })
    },


}
