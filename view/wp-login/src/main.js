import Vue from 'vue'
import App from './App.vue'
import less from 'less'
import VueRouter from "vue-router";
import 'element-ui/lib/theme-chalk/index.css';
import {Icon} from 'element-ui';


Vue.config.productionTip = false;
Vue.use(Icon);
Vue.use(VueRouter);
Vue.use(less);

new Vue({
    render: h => h(App),
}).$mount('#app');
