import axios from 'axios'
import qs from 'qs'
import {serialize} from "../utils/util";
import {Message, MessageBox} from "element-ui";
import errorCode from "../const/errorCode";
//进度条
// import NProgress from 'nprogress'
// import "nprogress/nprogress.css";
// 跨域请求，允许保存cookie
axios.defaults.withCredentials = true;
// 返回其他状态吗
axios.defaults.validateStatus = function(status) {
    return status >= 200 && status <= 500; // 默认的
};
// NProgress Configuration
// NProgress.configure({
//     showSpinner: false
// });
const service = axios.create({
    baseURL: '',
    timeout: 50000,    //访问超时的时间限制
});

service.interceptors.request.use(config => {

    // const isToken = (config.headers || {}).isToken === false;
    // let token = store.getters.access_token;
    // if (token && !isToken) {
    //     config.headers["Authorization"] = "Bearer " + token; // token
    // }
    // headers中配置serialize为true开启序列化
    if (config.method == "post" && config.headers.serialize) {
        config.data = serialize(config.data);
        delete config.data.serialize;
    }

    // 处理get 请求的数组 springmvc 可以处理
    if (config.method === "get") {
        config.paramsSerializer = function (params) {
            return qs.stringify(params, {arrayFormat: "repeat"});
        };
    }

    return config;
}, error => {
    return error
});

service.interceptors.response.use(res => {
    // console.log('response 拦截');
    //     // debugger
    console.log(res);
    const status = Number(res.status) || 200;
    const message = res.data.msg || errorCode[status] || errorCode["default"];
    // 后台定义 424 针对令牌过去的特殊响应码
    if (status === 424) {
        MessageBox.confirm("令牌状态已过期，请点击重新登录", "系统提示", {
            confirmButtonText: "重新登录",
            cancelButtonText: "取消",
            type: "warning"
        })
            .then(() => {
                // store.dispatch("LogOut").then(() => {
                //     // 刷新登录页面，避免多次弹框
                //     window.location.reload();
                // });
            })
            .catch(() => {
            });
        return;
    }

    if (status !== 200 || res.data.code === 1) {
        Message({
            message: message,
            type: "error"
        });
        return Promise.reject(new Error(message));
    }

    return res.data
}, error => {
    return error
});

export default service;




