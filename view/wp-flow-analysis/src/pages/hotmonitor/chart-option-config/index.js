import * as echarts from 'echarts'

const lineColor = '#9FD7FF';
const footSize = '16px';
//实时资讯图表配置
const lineChart_nowData = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#fecc00'
            }
        },
        formatter: '{b}: {c}%'
    },
    legend: {
        icon: 'roundRect',
        data: ['总客流量', '到港人数', '离港人数'],
        textStyle: {
            color: lineColor
        },
        right: 30
    },


    xAxis: {
        name: '日期',
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        triggerEvent: true,
        nameGap: 5,
        type: 'category',
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //刻度
        axisTick: {show: false},
        axisLabel: {
            margin: 10,
            color: lineColor,
            textStyle: {
                fontSize: footSize
            },
            itemStyle: {
                backgroundColor: 'red'
            }
        },
        // splitLine: {
        //     lineStyle: {
        //         type: 'solid',
        //         color: lineColor,
        //         width: 1
        //     },
        //     show: true
        // },
        data: [],
    },
    yAxis: {
        type: 'value',
        name: '(人数/万)',
        triggerEvent: true,
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor
        },
        splitNumber: 5,
        axisLabel: {
            formatter: '{value}',
            color: lineColor
        },
        axisTick: {show: false},
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //图表坐标背景设置
        splitArea: {show: false},
        splitLine: {
            lineStyle: {
                color: lineColor
            }
        }
    },
    series: [
        {
            name: '总客流量',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#00F0FF'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '到港人数',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#FECC00'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '离港人数',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#FF6296'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        }
    ]
};
//来源|去向Top10图表配置
const barChart_top10 = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#fecc00'
            }
        },
        formatter: '{b}: {c}%'
    },
    grid: {
        left: 10,
        top: 40,
        bottom: 10,
        right: 35,
        containLabel: true
    },
    xAxis: {
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        nameGap: 5,
        type: 'category',
        data: [],
        axisLine: {
            lineStyle: {
                color: lineColor
            }
        },
        axisTick: {
            show: false
        },
        //竖坐标轴线
        // splitLine: {
        //     lineStyle: {
        //         type: 'solid',
        //         color: lineColor,
        //         width: 1
        //     },
        //     show: true
        // },
        axisLabel: {
            color: lineColor,
            interval: 0,
            textStyle: {
                fontSize: footSize
            }
        }
    },
    yAxis: {
        type: 'value',
        name: '(人数/%)',
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        axisTick: {
            show: false
        },
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        axisLabel: {
            color: lineColor
        },
        splitLine: {
            show: true,
            lineStyle: {
                color: lineColor
            }

        }
    },
    series: [{
        data: [],
        type: 'bar',
        barWidth: 12,
        itemStyle: {
            normal: {
                barBorderRadius: [30, 30, 0, 0],
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                    offset: 1,
                    color: '#fecc00'
                }, {
                    offset: 0,
                    color: '#fecc00'
                }]),
            }
        },
    }]
};
//实时客流
const lineChart_nowDataPeople = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#fecc00'
            }
        },
        formatter: '{a0}: {c0}%<br/>{a1}: {c1}%<br/>{a2}: {c2}%<br/>'
    },
    legend: {
        icon: 'roundRect',
        data: ['实时流入', '实时流出', '实时人数'],
        textStyle: {
            color: lineColor
        },
        right: 30
    },
    grid: {
        left: '0%',
        right: '6%',
        top: '50px',
        bottom: '10%',
        containLabel: true
    },

    xAxis: {
        name: '日期',
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        triggerEvent: true,
        nameGap: 5,
        type: 'category',
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //刻度
        axisTick: {show: false},
        axisLabel: {
            margin: 10,
            color: lineColor,
            textStyle: {
                fontSize: footSize
            },
            itemStyle: {
                backgroundColor: 'red'
            }
        },
        data: [],
    },
    yAxis: {
        type: 'value',
        name: '(人数/万)',
        triggerEvent: true,
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor
        },
        splitNumber: 5,
        axisLabel: {
            formatter: '{value}',
            color: lineColor
        },
        axisTick: {show: false},
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //图表坐标背景设置
        splitArea: {show: false},
        splitLine: {
            lineStyle: {
                color: lineColor
            }
        }
    },
    series: [

        {
            name: '实时流入',
            type: 'line',
            data: [],

            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            areaStyle: {
                color: {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [{
                        offset: 0, color: 'rgba(229,255,227,0.6)' // 0% 处的颜色
                    }, {
                        offset: 1, color: 'rgba(255,255,227,0)' // 100% 处的颜色
                    }],
                    global: false // 缺省为 false
                }
            },
            smooth: false,
            itemStyle: {
                normal: {
                    color: '#e5ffe3',
                    lineStyle: {
                        width: 4,
                        type: 'solid',  //'dotted'虚线 'solid'实线
                        color: '#e5ffe3'
                    }
                }
            },
            // smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '实时流出',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#FECC00'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        },
        {
            name: '实时人数',
            type: 'line',
            data: [],
            itemStyle: {
                color: '#fe8ef4'
            },
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            smooth: true,
            lineStyle: {
                width: 2
            }
        }
    ]
};
//实时人口趋势
const lineChart_ssrk = {
    tooltip:  {
        axisPointer: {
            type: 'line',
        },
        trigger: 'axis',
        backgroundColor: 'rgba(10,56,119,0.9)',
        textStyle: {color: lineColor},
        borderColor: '#fecc00',
        borderWidth: 1,

    },
    // legend: {
    //     icon: 'roundRect',
    //     data: ['实时流入'],
    //     textStyle: {
    //         color: lineColor
    //     },
    //     right: 30
    // },
    grid: {
        left: '0%',
        right: '6%',
        top: '50px',
        bottom: '10%',
        containLabel: true
    },

    xAxis: {
        name: '时间',
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        triggerEvent: true,
        nameGap: 5,
        type: 'category',
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //刻度
        axisTick: {show: false},
        axisLabel: {
            margin: 10,
            color: lineColor,
            textStyle: {
                fontSize: footSize
            },
            itemStyle: {
                backgroundColor: 'red'
            }
        },
        data: [],
    },
    yAxis: {
        type: 'value',
        name: '(人数/万)',
        triggerEvent: true,
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor
        },
        splitNumber: 5,
        axisLabel: {
            formatter: '{value}',
            color: lineColor
        },
        axisTick: {show: false},
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //图表坐标背景设置
        splitArea: {show: false},
        splitLine: {
            lineStyle: {
                color: lineColor
            }
        }
    },
    series: [
        {
            name: '实时流入',
            type: 'line',
            data: [],

            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            areaStyle: {
                color: {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [{
                        offset: 0, color: 'rgba(245,198,5,0.8)' // 0% 处的颜色
                    }, {
                        offset: 1, color: 'rgba(255,255,227,0.2)' // 100% 处的颜色
                    }],
                    global: false // 缺省为 false
                }
            },
            smooth: true,
            itemStyle: {
                normal: {
                    color: '#F5C605',
                    lineStyle: {
                        width: 2,
                        type: 'solid',  //'dotted'虚线 'solid'实线
                        color: '#F5C605'
                    }
                }
            },
            lineStyle: {
                width: 2
            }
        },
    ]
};
//迁徙趋势
const lineChart_qxqs = {
    tooltip: {
        axisPointer: {
            type: 'line',
        },
        trigger: 'axis',
        backgroundColor: 'rgba(10,56,119,0.9)',
        textStyle: {color: lineColor},
        borderColor: '#fecc00',
        borderWidth: 1,

    },
    // legend: {
    //     icon: 'roundRect',
    //     textStyle: {
    //         color: lineColor
    //     },
    //     right: 30
    // },
    grid: {
        left: '0%',
        right: '6%',
        top: '50px',
        bottom: '10%',
        containLabel: true
    },

    xAxis: {
        name: '日期',
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor,
            padding: [0, 0, 0, 0]
        },
        triggerEvent: true,
        nameGap: 5,
        type: 'category',
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //刻度
        axisTick: {show: false},
        axisLabel: {
            margin: 10,
            color: lineColor,
            textStyle: {
                fontSize: footSize
            },
            itemStyle: {
                backgroundColor: 'red'
            }
        },
        data: [],
    },
    yAxis: {
        type: 'value',
        name: '(人数/万)',
        triggerEvent: true,
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor
        },
        splitNumber: 5,
        axisLabel: {
            formatter: '{value}',
            color: lineColor
        },
        axisTick: {show: false},
        //坐标线
        axisLine: {
            lineStyle: {
                type: 'solid',
                color: lineColor,
                width: 1
            },
            show: true
        },
        //图表坐标背景设置
        splitArea: {show: false},
        splitLine: {
            lineStyle: {
                color: lineColor
            }
        }
    },
    series: [
        {
            name: '实时流入',
            type: 'line',
            data: [],
            triggerEvent: false,
            symbol: 'circle', //标记的图形为实心圆
            symbolSize: 10,
            areaStyle: {
                color: {
                    type: 'linear',
                    x: 0,
                    y: 0,
                    x2: 0,
                    y2: 1,
                    colorStops: [{
                        offset: 0, color: 'rgba(0,230,248,0.8)' // 0% 处的颜色
                    }, {
                        offset: 1, color: 'rgba(255,255,227,0.2)' // 100% 处的颜色
                    }],
                    global: false // 缺省为 false
                }
            },
            smooth: true,
            itemStyle: {
                normal: {
                    color: '#00E6F8',
                    lineStyle: {
                        width: 2,
                        type: 'solid',  //'dotted'虚线 'solid'实线
                        color: '#00E6F8'
                    }
                }
            },
            lineStyle: {
                width: 2
            }
        },
    ]
};
let colors = ['#20ff92', '#fecc00', '#1cbcf9', '#ff795e'];
const barChart_hotLine = {
    tooltip: {
        axisPointer: {
            type: 'shadow',
            shadowStyle: {
                shadowColor: 'rgb(10,56,119)',
                shadowBlur: 1
            },
        },
        function (pos, params, dom, rect, size) {
            console.log(pos);
            console.log(params);
            console.log(dom);
            console.log(pos);
            console.log(rect);
            console.log(size);
            // 鼠标在左侧时 tooltip 显示到右侧，鼠标在右侧时 tooltip 显示到左侧。
            var obj = {top: 60};
            // obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5;
            return obj;
        },
        trigger: 'axis',
        backgroundColor: 'rgba(10,56,119,0.9)',
        textStyle: {color: lineColor},
        borderColor: '#fecc00',
        borderWidth: 1,
    },
    legend: {
        icon: 'roundRect',
        textStyle: {
            color: lineColor
        },
        bottom: 0
    },
    grid: {
        top: 10,
        right:15,
        bottom:60,
        left: 100,
        itemWidth: 50,
        itemHeight: 10,
        padding: [15, 120],
        textStyle: {
            fontSize: 12,
            color: '#9FD7FF',
            padding: [3, 0, 0, 0]
        },
    },
    xAxis: {
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor
        },

        splitNumber: 5,
        axisLabel: {
            formatter: '{value}',
            color: lineColor, textStyle: {
                fontSize: 16
            }
        },
        axisTick: {show: false},
        axisLine: {
            show: true,
            lineStyle: {
                color: '#9fd7ff'
            }
        },
        splitLine: {
            show: false,
            lineStyle: {
                color: '#9fd7ff'
            }
        },
        nameGap: 5,
        type: 'value',
        triggerEvent: true,
    },
    yAxis: {
        nameTextStyle: {
            fontSize: footSize,
            color: lineColor,
            padding: [-5, 0, 0, -100]
        },
        splitLine: {
            show: false,
            lineStyle: {
                color: 'rgba(255,255,255,0.2)'
            }
        },
        splitArea: {show: false},
        nameGap: 5,
        type: 'category',
        data: [],
        axisLine: {
            show: true,
            lineStyle: {
                color: '#9fd7ff'
            }
        },
        axisTick: {show: false},
        axisLabel: {
            margin: 10,
            color: lineColor,
            textStyle: {
                fontSize: 16
            }
        },

        name: '(人数/万)',
        //坐标轴名称相关配置
        nameLocation: 'start',
        triggerEvent: true,
        splitNumber: 5,
    },
    series: [
        {
            name: '水路',
            type: 'bar',
            stack: 'vistors',
            barWidth: '20px',
            data: [],
            color: colors[0],
            showBackground: true,
            backgroundStyle: {
                color: '#0e3372',
                borderColor: '#0051b5',
                borderWidth: 2,
                borderRadius:2
            },


        },
        {
            name: '民航',
            type: 'bar',
            stack: 'vistors',
            barWidth: '20px',
            color: colors[1],
            data: [],    backgroundStyle: {
                color: '#0e3372',
                borderColor: '#0051b5',
                borderWidth: 2,
                borderRadius:2
            },
        },
        {
            name: '铁路',
            type: 'bar',
            stack: 'vistors',
            barWidth: '20px',
            color: colors[2],
            data: [],    backgroundStyle: {
                color: '#0e3372',
                borderColor: '#0051b5',
                borderWidth: 2,
                borderRadius:2
            },
        },
        {
            name: '公路',
            type: 'bar',
            barWidth: '20px',
            stack: 'vistors',
            color: colors[3],
            data: [],    backgroundStyle: {
                color: '#0e3372',
                borderColor: '#0051b5',
                borderWidth: 2,
                borderRadius:2
            },
        }]
};
export {lineChart_nowData, barChart_top10, lineChart_nowDataPeople, lineChart_ssrk, lineChart_qxqs, barChart_hotLine}
