import * as API from "../utils/request";

export default {
    //流动情况——获取迁徙人口、同比、环比信息（历史迁徙模块——获取人口信息）
    getQXPeopleInfo(params) {
        return API.GET('/flow-analysis/migrantHistoricalTrend/getInfo', params)
    },
    //流动情况——获取实时人口趋势图表数据（省客流实时数据模块——获取数据）
    getRealTimePeople(params) {
        return API.GET('/flow-analysis/areaProvinceFlow/getChartsInfo', params)
    },
    //流动情况——获取迁徙趋势图图表数据（历史迁徙模块——获取统计趋势）
    getMigrationTrend(params) {
        return API.GET('/flow-analysis/migrantHistoricalTrend/getChartInfo', params)
    },
    //流动情况——迁入迁出top10表格数据（省内地市迁徙模块——省内热门迁入迁出）
    getHotTrendTop10(params) {
        return API.GET('/flow-analysis/migrantCity/getRank', params)
    },
    //流动情况——热门迁徙路线图表数据（省内地市迁徙模块——热门地市迁徙路线排行）
    getHotTrendChartTop10(params) {
        return API.GET('/flow-analysis/migrantCity/getPopularRouteRank', params)
    },

}
