let url = 'http://47.113.204.215:8888';
const publicPath= '/flow-analysis/';
module.exports = {
    publicPath: publicPath,
    configureWebpack: {
        externals: {
            AMap: "window.AMap"
        }
    },
    // 配置转发代理
    devServer: {
        port: 8080,
        proxy: {
            '/': {
                target: url,
                ws: true,
                pathRewrite: {
                    '^/': '/'
                }
            }
        }
    }
};
